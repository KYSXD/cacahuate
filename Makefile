.PHONY: help
## help
##  - show available targets
help: Makefile
	@sed -n 's/^##//p' $<

.PHONY: build
## build
build:
	./setup.py sdist && ./setup.py bdist_wheel

.PHONY: test
## test
test: pytest lint xmllint xmlvalidate

.PHONY: xml
## xml
xml: xmllint xmlvalidate

.PHONY: publish
## publish
publish:
	twine upload dist/* && git push && git push --tags

.PHONY: clean
## clean
clean:
	rm -rf dist/

.PHONY: pytest
## pytest
pytest:
	pytest -xvv

.PHONY: lint
## lint
lint:
	flake8

.PHONY: xmllint
## xmllint
xmllint:
	xmllint --noout --relaxng cacahuate/xml/process-spec.rng xml/*.xml

.PHONY: xmlvalidate
## xmlvalidate
xmlvalidate:
	python cacahuate/xml/validate.py xml/*.xml -v
