#!/bin/sh

: ${RABBITMQ_HOST:=localhost}
: ${RABBITMQ_PORT:=5672}

cmd="$@"

until nc -z $RABBITMQ_HOST $RABBITMQ_PORT
do
    echo "Waiting for Rabbit ($RABBITMQ_HOST:$RABBITMQ_PORT) to start..."
    sleep 0.5
done

>&2 echo "Rabbit is up - executing command"
exec $cmd
