import cacahuate.models
from cacahuate.database import db_session


def test_user_field():
    test_data = [
        {
            'identifier': 'user_0',
            'fullname': 'Test User 0',
            'email': 'test0@email.foo',
            'is_superuser': True,
        },
        {
            'identifier': 'user_1',
            'fullname': 'Test User 1',
            'email': 'test1@email.foo',
            'is_superuser': False,
        },
    ]

    for data in test_data:
        item = cacahuate.models.User(**data)
        db_session.add(item)
        db_session.commit()

        for k, v in data.items():
            assert getattr(item, k) == v


def test_user_relation():
    group_data = [
        {'codename': 'group_0', 'name': 'Group 0'},
        {'codename': 'group_1', 'name': 'Group 1'},
        {'codename': 'group_2', 'name': 'Group 2'},
    ]

    for data in group_data:
        item = cacahuate.models.Group(**data)
        db_session.add(item)

    permission_data = [
        {'codename': 'perm_0', 'name': 'Perm 0'},
        {'codename': 'perm_1', 'name': 'Perm 1'},
        {'codename': 'perm_2', 'name': 'Perm 2'},
    ]

    for data in permission_data:
        item = cacahuate.models.Permission(**data)
        db_session.add(item)

    user_data = [
        {
            'identifier': 'user_0',
            'fullname': 'Test User 0',
            'email': 'test0@email.foo',
            'is_superuser': True,
        },
        {
            'identifier': 'user_1',
            'fullname': 'Test User 1',
            'email': 'test1@email.foo',
            'is_superuser': False,
        },
        {
            'identifier': 'user_2',
            'fullname': 'Test User 2',
            'email': 'test2@email.foo',
            'is_superuser': False,
        },
    ]

    for data in user_data:
        item = cacahuate.models.User(**data)
        db_session.add(item)

    db_session.commit()

    test_data = [
        {
            'identifier': user_data[0]['identifier'],
            'groups': [
                group_data[1]['codename'],
                group_data[2]['codename'],
            ],
            'permissions': [
                permission_data[1]['codename'],
                permission_data[2]['codename'],
            ],
        },
        {
            'identifier': user_data[1]['identifier'],
            'groups': [
                group_data[1]['codename'],
                group_data[2]['codename'],
            ],
            'permissions': [
                permission_data[1]['codename'],
                permission_data[2]['codename'],
            ],
        },
    ]

    for current_test in test_data:
        user = db_session.query(cacahuate.models.User).filter(
            cacahuate.models.User.identifier == current_test['identifier'],
        ).first()

        user.groups.extend([
            db_session.query(cacahuate.models.Group).filter(
                cacahuate.models.Group.codename == v,
            ).first()
            for v in current_test['groups']
        ])
        user.permissions.extend([
            db_session.query(cacahuate.models.Permission).filter(
                cacahuate.models.Permission.codename == v,
            ).first()
            for v in current_test['permissions']
        ])

        db_session.commit()

        assert sorted([
            item.codename
            for item in user.groups.all()
        ]) == sorted(current_test['groups'])

        assert sorted([
            item.codename
            for item in user.permissions.all()
        ]) == sorted(current_test['permissions'])
