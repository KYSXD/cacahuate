import cacahuate.models
from cacahuate.database import db_session


def test_process_field() -> None:
    test_data = [
        {
            'identifier': 'prueba',
            'version': '2224-12-01',
            'is_public': False,
            'name': 'prueba name',
            'name_template': 'prueba name template',
            'description': 'prueba description',
            'description_template': 'prueba description template',
        },
        {
            'identifier': 'prueba',
            'version': '2224-12-02',
            'is_public': True,
            'name': 'prueba name',
            'name_template': 'prueba name template',
            'description': 'prueba description',
            'description_template': 'prueba description template',
        },
    ]

    for data in test_data:
        item = cacahuate.models.ProcessDefinition(**data)
        db_session.add(item)
        db_session.commit()

        for k, v in data.items():
            assert getattr(item, k) == v
