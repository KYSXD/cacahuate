from cacahuate.errors import MalformedProcess
from cacahuate.xml.validate import _validate_file

import pytest


def test_xml_validation():
    xml_path = 'xml/simple.2018-02-19.xml'
    assert _validate_file(xml_path) is None


def test_xml_validation_repeated_id():
    xml_path = 'xml/invalid/condition_id_repeat.2018-05-28.xml'
    with pytest.raises(MalformedProcess) as cm:
        _validate_file(xml_path)

    msg = '28 Duplicated id: \'start_node\''
    assert str(cm.value) == f'{xml_path}:{msg}'


def test_xml_validation_unexistent_param():
    xml_path = 'xml/invalid/condition_not_param.2018-05-28.xml'
    with pytest.raises(MalformedProcess) as cm:
        _validate_file(xml_path)

    msg = '41 Referenced param does not exist \'a.b\''
    assert str(cm.value) == f'{xml_path}:{msg}'


def test_xml_validation_unexistent_dependency():
    xml_path = 'xml/invalid/condition_not_dep.2018-05-28.xml'
    with pytest.raises(MalformedProcess) as cm:
        _validate_file(xml_path)

    msg = '43 Referenced dependency does not exist \'a.b\''
    assert str(cm.value) == f'{xml_path}:{msg}'


def test_xml_validation_invalid_condition():
    xml_path = 'xml/invalid/condition_not_valid.2018-05-28.xml'
    with pytest.raises(MalformedProcess) as cm:
        _validate_file(xml_path)

    msg = '26 Lex error in condition'
    assert str(cm.value) == f'{xml_path}:{msg}'


def test_xml_validation_no_hyphen_in_id():
    xml_path = 'xml/invalid/validate_hyphen_id.2018-06-13.xml'
    with pytest.raises(MalformedProcess) as cm:
        _validate_file(xml_path)

    msg = '12 Id must be a valid variable name'
    assert str(cm.value) == f'{xml_path}:{msg}'


def test_xml_validation_no_hyphen_in_field_name():
    xml_path = 'xml/invalid/validate_hyphen_field.2018-06-13.xml'
    with pytest.raises(MalformedProcess) as cm:
        _validate_file(xml_path)

    msg = '23 Field names must match [a-zA-Z0-9_]+'
    assert str(cm.value) == f'{xml_path}:{msg}'


def test_xml_validation_no_hyphen_in_form_id():
    xml_path = 'xml/invalid/validate_hyphen_form.2018-06-13.xml'
    with pytest.raises(MalformedProcess) as cm:
        _validate_file(xml_path)

    msg = '23 Form ids must be valid variable names'
    assert str(cm.value) == f'{xml_path}:{msg}'


def test_xml_validation_undefined_form():
    xml_path = 'xml/invalid/condition_undefined_form.2018-07-10.xml'
    with pytest.raises(MalformedProcess) as cm:
        _validate_file(xml_path)

    msg = '26 variable used in condition is not defined \'misterio.password\''
    assert str(cm.value) == f'{xml_path}:{msg}'
