from cacahuate.database import db_session
from cacahuate.models import Execution

from flask import json

from tests.utils import (
    make_auth_header,
    make_user,
)


def test_interpolated_name(config, client, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')

    name = 'Computes a name based on a Cow'

    res = client.post('/v1/execution', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'process_name': 'interpol',
        'form_array': [{
            'ref': 'form',
            'data': {
                'field': 'Cow',
            },
        }],
    }))

    # request succeeded
    assert res.status_code == 201

    # execution has name
    exc = db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).first()

    assert exc.name == name

    # execution collection has name
    reg2 = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg2['id'] == exc.id
    assert reg2['name'] == name
