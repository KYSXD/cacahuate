'''
Tests the cacahuate.mongo module
'''
from cacahuate.mongo import make_context


def test_make_context(config):
    values = {
        'form1': [
            {
                'input1': 'A',
            },
            {
                'input1': 'B',
            },
        ],
    }
    context = make_context(values, config)

    assert context['form1']['input1'] == 'B'
    assert list(context['form1'].all())[0]['input1'] == 'A'

    assert context['_env']['FOO'] == 'var'
