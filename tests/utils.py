import datetime
from base64 import b64encode
from random import choice
from string import ascii_letters

import cacahuate.database
import cacahuate.models

import flask

import jwt


def random_string(length=6):
    return ''.join(choice(ascii_letters) for _ in range(length))


def make_user(identifier=None, name=None, email=None):
    if identifier is None:
        identifier = random_string()

    if name is None:
        name = f'User {identifier.title()}'

    if email is None:
        email = f'{identifier.lower()}@mailinator.com'

    user = cacahuate.models.User(
        identifier=identifier,
        fullname=name,
        email=email,
    )

    cacahuate.database.db_session.add(user)
    cacahuate.database.db_session.commit()

    return user


def make_auth_header(user=None):
    if user is None:
        user = make_user(
            random_string(),
            random_string(),
        )

    time_now = datetime.datetime.utcnow()
    time_exp = time_now + datetime.timedelta(
        seconds=flask.current_app.config['JWT_ACCESS_TOKEN_EXPIRES'],
    )

    token_str = jwt.encode(
        {
            'sub': user.identifier,
            'exp': time_exp,
            'iat': time_now,
        },
        flask.current_app.config['SECRET_KEY'],
        algorithm='HS256',
    )

    return {
        'Authorization': 'Basic {}'.format(
            b64encode(
                '{}:{}'.format(
                    user.identifier,
                    token_str,
                ).encode(),
            ).decode(),
        ),
    }


def make_pointer(
    process_name,
    node_id,
    node_type='action',
    execution=None,
):
    if execution is None:
        execution = cacahuate.models.Execution(
            process_name=process_name,
            name=process_name,
            description=process_name,
            status='ongoing',
        )

    ptr = cacahuate.models.Pointer(
        name=node_id,
        node_id=node_id,
        node_type=node_type,
        description=node_id,
        execution=execution,
        status='ongoing',
    )

    cacahuate.database.db_session.add(ptr)
    cacahuate.database.db_session.commit()

    return ptr


def make_date(year=2018, month=5, day=4, hour=0, minute=0, second=0):
    return datetime.datetime(year, month, day, hour, minute, second)


def assert_near_date(date, seconds=2):
    reference = datetime.datetime.now()
    assert (date - reference).total_seconds() < seconds
