import os

import cacahuate.database
import cacahuate.database.permissions
import cacahuate.http.app

import itacate

import pymongo

import pytest


def ismybirthday(value):
    if value.month == 5 and value.day == 10:
        return 'Today is my birthday!'
    return 'Is not my birthday'


TESTING_SETTINGS = {
    'CUSTOM_LOGIN_PROVIDERS': {
        'hardcoded': 'tests.hardcoded_login',
    },
    'CUSTOM_HIERARCHY_PROVIDERS': {
        'hardcoded': 'tests.hardcoded_hierarchy',
        'noparam': 'tests.noparam_hierarchy',
    },
    'PROCESS_ENV': {
        'FOO': 'var',
    },
    'PAGINATION_LIMIT': 1_000_000_000,
    'PAGINATION_OFFSET': 0,
    'TEMPLATE_PATH': os.path.join(os.path.dirname(__file__), 'templates'),
    'JINJA_FILTERS': {
        'ismybirthday': ismybirthday,
    },
    'ENABLED_LOGIN_PROVIDERS': [],
    'SOLVERS': {
        'check_truthy': lambda x: (True, '') if x else (False, f'{x} fails'),
    },
}


@pytest.fixture
def config():
    ''' Returns a fully loaded configuration dict '''
    con = itacate.Config(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            '..',
        ),
    )

    con.from_object('cacahuate.settings')
    con.from_mapping(TESTING_SETTINGS)

    return con


@pytest.fixture
def app():
    '''
    Create a Flask app context for the tests.
    '''
    def shutdown_session(exception=None):
        pass

    cacahuate.http.app.shutdown_session = shutdown_session

    app = cacahuate.http.app.create_app()
    app.config.from_mapping(TESTING_SETTINGS)
    with app.app_context():
        yield app


@pytest.fixture
def client(app):
    ''' makes and returns a testclient for the flask application '''
    with app.test_client() as client:
        yield client


@pytest.fixture
def mongo(config):
    uri = config['MONGO_URI']
    client = pymongo.MongoClient(uri)
    database_name = pymongo.uri_parser.parse_uri(uri)['database']
    db = client[database_name]

    yield db

    db[config['POINTER_COLLECTION']].drop()
    db[config['EXECUTION_COLLECTION']].drop()


@pytest.fixture
def engine(config):
    cacahuate.database.init_engine(
        config['SQLALCHEMY_DATABASE_URI'],
    )
    return cacahuate.database.engine


@pytest.fixture(autouse=True)
def tables(engine):
    cacahuate.database.Base.metadata.drop_all(engine)
    cacahuate.database.Base.metadata.create_all(engine)
    cacahuate.database.permissions.create_initial_permissions()

    yield

    cacahuate.database.db_session.remove()
