from datetime import datetime
from itertools import chain

from cacahuate.database import db_session
from cacahuate.errors import (
    MisconfiguredProvider,
)
from cacahuate.handler.legacy import (
    Handler,
    compact_values,
    unpack_pointer_values,
)
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.mongo import (
    execution_entry,
    pointer_entry,
)
from cacahuate.xml import Xml
from cacahuate.xml.node import (
    Form,
)

from pika.adapters.blocking_connection import BlockingChannel

import pytest

import simplejson as json

import tests.utils
from tests.utils import (
    assert_near_date,
    make_pointer,
    make_user,
    random_string,
)


def test_wakeup(config, mongo, mocker):
    ''' the first stage in a node's lifecycle '''
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.basic_publish'
    ))
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.exchange_declare'
    ))
    # setup stuff
    handler = Handler(config)

    pointer = make_pointer('simple.2018-02-19.xml', 'start_node')
    execution = pointer.execution
    juan = tests.utils.make_user('juan', 'Juan')
    manager = tests.utils.make_user(
        'juan_manager',
        'Juan Manager',
        'hardcoded@mailinator.com',
    )

    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        execution,
        Xml.load(config, execution.process_name).get_state(),
    ))

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(pointer),
    )

    # will wakeup the second node
    handler.step({
        'command': 'step',
        'pointer_id': pointer.id,
        'user_identifier': juan.identifier,
        'input': [],
    })

    # test manager is notified
    BlockingChannel.basic_publish.assert_called_once()
    BlockingChannel.exchange_declare.assert_called_once()

    args = BlockingChannel.basic_publish.call_args[1]

    assert args['exchange'] == config['RABBIT_NOTIFY_EXCHANGE']
    assert args['routing_key'] == 'email'
    assert json.loads(args['body']) == {
        'recipient': 'hardcoded@mailinator.com',
        'subject': '[procesos] Tarea asignada',
        'template': 'assigned-task.html',
        'data': {
            'pointer': {
                **db_session.query(Pointer).filter(
                    Pointer.status == 'ongoing',
                ).first().as_json(),
                'execution': db_session.query(Pointer).filter(
                    Pointer.status == 'ongoing',
                ).first().execution.as_json(),
            },
            'cacahuate_url': config['GUI_URL'],
        },
    }

    # pointer collection updated
    reg = next(mongo[config["POINTER_COLLECTION"]].find({'state': 'ongoing'}))

    assert_near_date(reg['started_at'])
    assert reg['finished_at'] is None
    assert reg['node'] == {
        'id': 'mid_node',
        'type': 'action',
        'description': 'añadir información',
        'name': 'Segundo paso',
    }
    assert reg['values'] == []

    assert reg['actors'] == {
        '_type': ':map',
        'items': {},
    }
    assert reg['state'] == 'ongoing'

    # execution collection updated
    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg['state']['items']['mid_node']['state'] == 'ongoing'

    # tasks where asigned
    assert manager.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 1

    task = manager.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).first()

    assert isinstance(task, Pointer)
    assert task.node_id == 'mid_node'
    assert task.execution.id == execution.id


def test_teardown(config, mongo):
    ''' second and last stage of a node's lifecycle '''
    # test setup
    handler = Handler(config)

    process_name = 'simple.2018-02-19.xml'
    xml = Xml.load(config, process_name, direct=True)

    p_0 = make_pointer('simple.2018-02-19.xml', 'mid_node')
    main_ptr_01 = p_0
    execution = p_0.execution
    main_exe = p_0.execution

    tests.utils.make_user('juan', 'Juan')
    manager = tests.utils.make_user('manager', 'Manager')
    manager2 = tests.utils.make_user('manager2', 'Manager2')

    manager.assigned_tasks.extend([p_0])
    manager2.assigned_tasks.extend([p_0])

    db_session.commit()

    state = Xml.load(config, execution.process_name).get_state()
    state['items']['start_node']['state'] = 'valid'

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        state,
    ))

    # will teardown mid_node
    handler.step({
        'command': 'step',
        'pointer_id': p_0.id,
        'user_identifier': manager.identifier,
        'input': [Form.state_json('mid_form', [
            {
                '_type': 'field',
                'state': 'valid',
                'value': 'yes',
                'value_caption': 'yes',
                'name': 'data',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == p_0.id,
        Pointer.status == 'ongoing',
    ).first() is None

    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).count() == 1
    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first().node_id == 'final_node'

    # mongo has a registry
    reg = next(mongo[config["POINTER_COLLECTION"]].find())

    assert_near_date(reg['finished_at'])
    assert reg['node']['id'] == p_0.node_id

    assert reg['actors'] == {
        '_type': ':map',
        'items': {
            'manager': {
                '_type': 'actor',
                'state': 'valid',
                'user': manager.as_json(include=[
                    '_type',
                    'identifier',
                    'fullname',
                    'email',
                ]),
                'forms': [Form.state_json('mid_form', [
                    {
                        '_type': 'field',
                        'state': 'valid',
                        'value': 'yes',
                        'value_caption': 'yes',
                        'name': 'data',
                    },
                ])],
            },
        },
    }

    # tasks where deleted from user
    assert manager.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 0
    assert manager2.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 0

    # state
    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg['state'] == {
        '_type': ':sorted_map',
        'items': {
            'start_node': {
                **xml.get_node('start_node').get_state(),
                'state': 'valid',
            },
            'mid_node': {
                **xml.get_node('mid_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'manager': {
                            '_type': 'actor',
                            'state': 'valid',
                            'user': manager.as_json(include=[
                                '_type',
                                'identifier',
                                'fullname',
                                'email',
                            ]),
                            'forms': [Form.state_json('mid_form', [
                                {
                                    '_type': 'field',
                                    'state': 'valid',
                                    'value': 'yes',
                                    'value_caption': 'yes',
                                    'name': 'data',
                                },
                            ])],
                        },
                    },
                },
                'state': 'valid',
            },
            'final_node': {
                **xml.get_node('final_node').get_state(),
                'state': 'ongoing',
            },
        },
        'item_order': [
            'start_node',
            'mid_node',
            'final_node',
        ],
    }

    expected_values = {
        'mid_form': [{'data': 'yes'}],
    }
    assert {
        'mid_form': reg['values']['mid_form'],
    } == expected_values

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'manager',
                'field_id': 'data',
                'form_id': 'mid_form',
                'form_index': 0,
                'ref': 'mid_node.manager.0:mid_form.data',
                'task_form_index': 0,
                'value': 'yes',
                'value_caption': 'yes',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'mid_form': [
            {
                'data': {
                    'authors': {
                        'manager',
                    },
                    'refs': {
                        'mid_node.manager.0:mid_form.data',
                    },
                    'value': 'yes',
                    'value_caption': 'yes',
                },
            },
        ],
    }


def test_finish_execution(config, mongo):
    handler = Handler(config)

    p_0 = make_pointer('simple.2018-02-19.xml', 'manager')
    execution = p_0.execution
    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'status': 'ongoing',
        'id': execution.id,
    })

    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())
    assert execution.id == reg['id']

    handler.finish_execution(execution)

    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg['status'] == 'finished'
    assert_near_date(reg['finished_at'])


def test_call_handler_delete_process(config, mongo):
    handler = Handler(config)
    pointer = make_pointer('simple.2018-02-19.xml', 'requester')
    user = make_user('juan', 'Juan')
    execution_id = pointer.execution.id
    payload = {
        'command': 'cancel',
        'execution_id': execution_id,
        'pointer_id': pointer.id,
        'user_identifier': user.identifier,
    }

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'status': 'ongoing',
        'id': execution_id,
    })

    ptr_1 = make_pointer(
        'simple.2018-02-19.xml',
        'requester',
        execution=pointer.execution,
    )
    ptr_2 = make_pointer(
        'simple.2018-02-19.xml',
        'requester',
        execution=pointer.execution,
    )
    ptr_3 = make_pointer(
        'simple.2018-02-19.xml',
        'requester',
    )

    mongo[config["POINTER_COLLECTION"]].insert_many([
        {
            'state': 'finished',
            'id': ptr_1.id,
        },
        {
            'state': 'ongoing',
            'id': ptr_2.id,
        },
        {
            'state': 'ongoing',
            'id': ptr_3.id,
        },
    ])

    handler(json.dumps(payload))

    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg['id'] == execution_id
    assert reg['status'] == "cancelled"
    assert_near_date(reg['finished_at'])

    assert db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).one() == ptr_3.execution
    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).one() == ptr_3

    assert mongo[config["POINTER_COLLECTION"]].find_one({
        'id': ptr_1.id,
    })['state'] == 'finished'
    assert mongo[config["POINTER_COLLECTION"]].find_one({
        'id': ptr_2.id,
    })['state'] == 'cancelled'
    assert mongo[config["POINTER_COLLECTION"]].find_one({
        'id': ptr_3.id,
    })['state'] == 'ongoing'


def test_resistance_unexistent_hierarchy_backend(config, mongo):
    handler = Handler(config)

    ptr = make_pointer('wrong.2018-04-11.xml', 'start_node')
    exc = ptr.execution
    user = make_user('juan', 'Juan')

    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        exc,
        Xml.load(config, exc.process_name).get_state(),
    ))

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    with pytest.raises(MisconfiguredProvider):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': ptr.id,
            'user_identifier': user.identifier,
            'input': {},
        }))


def test_resistance_hierarchy_return(config, mongo):
    handler = Handler(config)

    ptr = make_pointer('wrong.2018-04-11.xml', 'start_node')
    exc = ptr.execution
    user = make_user('juan', 'Juan')

    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        exc,
        Xml.load(config, exc.process_name).get_state(),
    ))

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    with pytest.raises(MisconfiguredProvider):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': ptr.id,
            'user_identifier': user.identifier,
            'input': {},
        }))


def test_resistance_hierarchy_item(config, mongo):
    handler = Handler(config)

    ptr = make_pointer('wrong.2018-04-11.xml', 'start_node')
    exc = ptr.execution
    user = make_user('juan', 'Juan')

    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        exc,
        Xml.load(config, exc.process_name).get_state(),
    ))

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    with pytest.raises(MisconfiguredProvider):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': ptr.id,
            'user_identifier': user.identifier,
            'input': {},
        }))


def test_resistance_node_not_found(config, mongo):
    handler = Handler(config)

    ptr = make_pointer('wrong.2018-04-11.xml', 'start_node')
    exc = ptr.execution
    user = make_user('juan', 'Juan')

    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        exc,
        Xml.load(config, exc.process_name).get_state(),
    ))

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    with pytest.raises(MisconfiguredProvider):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': ptr.id,
            'user_identifier': user.identifier,
            'input': {},
        }))


def test_compact_form_values(config):
    handler = Handler(config)
    names = [random_string() for _ in '123']

    values = handler.compact_form_values([
        Form.state_json('form1', [
            {
                'name': 'name',
                'type': 'text',
                'value': names[0],
            },
        ]),
        Form.state_json('form1', [
            {
                'name': 'name',
                'type': 'text',
                'value': names[1],
            },
        ]),
        Form.state_json('form2', [
            {
                'name': 'name',
                'type': 'text',
                'value': names[2],
            },
        ]),
    ])

    assert values == {
        'values.form1': [
            {
                'name': name,
            } for name in names[:2]
        ],
        'values.form2': [
            {
                'name': names[2],
            },
        ],
    }
