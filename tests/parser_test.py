import dataclasses
import os
import xml.dom.pulldom

from cacahuate.xml.parser import (
    parse_dependencies,
    parse_form,
    parse_input,
    parse_options,
    parse_xml,
)
from cacahuate.xml.translator import translate_process

import yaml


def load_yaml(path):
    with open(path, 'r') as stream:
        return yaml.safe_load(stream)


def test_parse_dependencies():
    parser = xml.dom.pulldom.parseString(
        '''
          <dependencies>
            <dep>foo1.bar</dep>
            <dep>foo2.bar</dep>
          </dependencies>
        ''',
    )

    next(parser)
    _event, node = next(parser)
    assert node.tagName == 'dependencies'

    assert [
        'foo1.bar',
        'foo2.bar',
    ] == parse_dependencies(
        node,
        parser,
    )


def test_parse_options():
    parser = xml.dom.pulldom.parseString(
        '''
          <options>
            <option value="opt_1">Option 1</option>
            <option value="opt_2">Option 2</option>
          </options>
        ''',
    )

    next(parser)
    _event, node = next(parser)
    assert node.tagName == 'options'

    assert [
        {
            'label': 'Option 1',
            'value': 'opt_1',
        },
        {
            'label': 'Option 2',
            'value': 'opt_2',
        },
    ] == parse_options(
        node,
        parser,
    )


def test_parse_input():
    parser = xml.dom.pulldom.parseString(
      '''
        <input
          type="text"
          label="Nombre"
          name="name"
          placeholder="Jon Snow"
          default="Jon Snow"
        />
      ''',
    )

    next(parser)
    _event, node = next(parser)
    assert node.tagName == 'input'

    assert {
        'id': 'name',
        'type': 'text',
        'label': 'Nombre',
        'dependencies': [],
        'required': 'FALSE',
    } == parse_input(node, parser)


def test_parse_form(config):
    parser = xml.dom.pulldom.parseString(
      '''
        <form id="auth_form">
          <input
            type="text"
            label="Nombre"
            name="name"
            placeholder="Jon Snow"
            default="Jon Snow"
          />
        </form>
      ''',
    )

    next(parser)
    _event, node = next(parser)
    assert node.tagName == 'form'

    assert {
        'id': 'auth_form',
        'inputs': [
            {
                'id': 'name',
                'type': 'text',
                'label': 'Nombre',
                'dependencies': [],
                'required': 'FALSE',
            },
        ],
        'min_amount': 1,
        'max_amount': 1,
    } == parse_form(node, parser)


def test_parse_xml(config):
    proc_names = [
        'call-render.2020-04-24',
        'complex.2023-03-01',
        'gift-request.2020-04-05',
        'proc-connector.2023-03-01',
        'select-model.2020-11-01',
        'select-ref.2021-04-15',
        'simple.2018-02-19',
        'validation-multiform.2018-05-22',
    ]

    for proc_name in proc_names:
        xml_path = os.path.join(config['XML_PATH'], f'{proc_name}.xml')
        with open(xml_path) as f:
            parsed = parse_xml(xml.dom.pulldom.parse(f))

        filename = f'tests/fixtures/parsed/{proc_name}.yml'
        expected = load_yaml(filename)
        assert expected == parsed


def test_translate_process(config):
    proc_names = [
        'complex.2023-03-01',
        'gift-request.2020-04-05',
        'select-model.2020-11-01',
        'select-ref.2021-04-15',
        'simple.2018-02-19',
        'validation-multiform.2018-05-22',
    ]

    for proc_name in proc_names:
        xml_path = os.path.join(config['XML_PATH'], f'{proc_name}.xml')
        with open(xml_path) as f:
            parsed = parse_xml(xml.dom.pulldom.parse(f))

        filename = f'tests/fixtures/translated/{proc_name}.yml'
        expected = load_yaml(filename)
        identifier, version = proc_name.split('.')
        assert expected == dataclasses.asdict(
            translate_process(parsed, identifier, version),
        )
