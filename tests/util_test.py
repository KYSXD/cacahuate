from cacahuate.models import clear_username
from cacahuate.utils import get_processable_nodes


def test_clear_email():
    assert clear_username('kevin@mailinator.com') == 'kevin'
    assert clear_username('a.wonderful.code@gmail.com') == 'awonderfulcode'
    assert clear_username('foo@var.com.mx') == 'foo'
    assert clear_username('foo.var@var.com.mx') == 'foovar'
    assert clear_username('$foo') == 'foo'


def test_get_processable_nodes():
    assert sorted(get_processable_nodes(
        graph={
            "A": set(),
            "B": {"A"},
            "C": {"A"},
            "D": {"B", "C"},
            "E": "I",
            "F": "E",
            "G": set(),
            "H": {"A", "B", "C"},
            "I": {"A", "B", "G"},
        },
        processed_nodes={
            "A",
            "B",
            "C",
            "G",
            "I",
        },
    )) == sorted(['D', 'E', 'H'])
