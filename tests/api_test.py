import cacahuate.xml


def test_name_with_if(client, mongo, config):
    xml = cacahuate.xml.Xml.load(config, 'pollo')
    assert xml.name == 'pollo.2018-05-20.xml'
