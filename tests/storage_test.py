from unittest.mock import MagicMock

from cacahuate.database import db_session
from cacahuate.handler.legacy import Handler
from cacahuate.models import Pointer
from cacahuate.mongo import pointer_entry
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

import requests

from tests.utils import (
    make_pointer,
    make_user,
    random_string,
)


def test_send_request_multiple(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    ''' Tests that values are stored in such a way that they can be iterated
    in a jinja template. Specifically in this test they'll be used as part of
    a request node, thus also testing that all of the values can be used '''
    # test setup
    class ResponseMock:
        status_code = 200
        text = 'request response'

    mock = MagicMock(return_value=ResponseMock())

    mocker.patch(
        'requests.request',
        new=mock,
    )

    handler = Handler(config)
    user = make_user('juan', 'Juan')
    ptr = make_pointer('request-multiple.2019-11-14.xml', 'start_node')
    execution = ptr.execution
    names = [random_string() for _ in '123']

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, execution.process_name).get_state(),
    })

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [
            Form.state_json('form1', [
                {
                    'name': 'name',
                    'type': 'text',
                    'value': names[0],
                    'value_caption': names[0],
                },
            ]),
            Form.state_json('form1', [
                {
                    'name': 'name',
                    'type': 'text',
                    'value': names[1],
                    'value_caption': names[1],
                },
            ]),
            Form.state_json('form1', [
                {
                    'name': 'name',
                    'type': 'text',
                    'value': names[2],
                    'value_caption': names[2],
                },
            ]),
        ],
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'request_node'

    # request is made with correct data
    requests.request.assert_called_once()
    args, kwargs = requests.request.call_args

    assert args[0] == 'POST'
    assert args[1] == 'http://localhost/'

    assert kwargs['data'] == '{{"names": ["{}","{}","{}"]}}'.format(*names)
    assert kwargs['headers'] == {
        'content-type': 'application/json',
    }
