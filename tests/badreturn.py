from typing import Self

from cacahuate.auth.base import BaseHierarchyProvider


class BadreturnHierarchyProvider(BaseHierarchyProvider):

    def find_users(self: Self, **params: str) -> list | None:
        if params.get('opt') == 'return':
            return None

        if params.get('opt') == 'item':
            return [None]

        return None
