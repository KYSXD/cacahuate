from itertools import chain

from cacahuate.database import db_session
from cacahuate.handler.legacy import (
    Handler,
    compact_values,
    unpack_pointer_values,
)
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.mongo import (
    execution_entry,
    pointer_entry,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
)


def test_exit_interaction(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    main_ptr_01 = make_pointer('exit.2018-05-03.xml', 'start_node')
    main_exe = main_ptr_01.execution
    xml = Xml.load(config, main_exe.process_name, direct=True)

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_ptr_01.execution,
        Xml.load(config, main_ptr_01.execution.process_name).get_state(),
    ))

    # first node
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan', 'Juan').identifier,
        'input': [],
    })
    main_ptr_02 = next(
        p_ for p_ in main_ptr_01.execution.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id

    # exit node
    assert json.loads(handle.delay.call_args[0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'user_identifier': '__system__',
        'input': [],
    }
    handler.step(json.loads(handle.delay.call_args[0][0]))

    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).count() == 0
    assert db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).count() == 0

    # state is coherent
    assert next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': main_ptr_01.execution.id,
    }))['state'] == {
        '_type': ':sorted_map',
        'items': {
            'start_node': {
                **xml.get_node('start_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'juan': {
                            '_type': 'actor',
                            'forms': [],
                            'state': 'valid',
                            'user': {
                                '_type': 'user',
                                'identifier': 'juan',
                                'fullname': 'Juan',
                                'email': 'juan@mailinator.com',
                            },
                        },
                    },
                },
                'state': 'valid',
            },

            'exit': {
                **xml.get_node('exit').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        '__system__': {
                            '_type': 'actor',
                            'forms': [],
                            'state': 'valid',
                            'user': {
                                '_type': 'user',
                                'identifier': '__system__',
                                'fullname': 'System',
                                'email': '',
                            },
                        },
                    },
                },
                'state': 'valid',
            },

            'final_node': {
                **xml.get_node('final_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {},
                },
            },
        },
        'item_order': ['start_node', 'exit', 'final_node'],
    }

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
    }
