from itertools import chain

from cacahuate.database import db_session
from cacahuate.handler.legacy import (
    Handler,
    compact_values,
    unpack_pointer_values,
)
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.mongo import (
    execution_entry,
    pointer_entry,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
    random_string,
)


def test_call_node(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    main_ptr_01 = make_pointer('call.2018-05-18.xml', 'start_node')
    main_exe = main_ptr_01.execution
    test_value = random_string()

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    # teardown of first node and wakeup of call node
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('start_form', [
            {
                'name': 'data',
                'value': test_value,
                'value_caption': test_value,
            },
        ])],
    })

    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'call'

    alt_exe = db_session.query(Execution).filter(
        Execution.status == Execution.Status.ONGOING,
        Execution.process_name == 'simple.2018-02-19.xml',
    ).first()

    alt_ptr_01 = next(
        p_ for p_ in alt_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )

    # aditional rabbit call for new process
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': alt_ptr_01.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('start_form', [
            {
                'label': 'Info',
                'name': 'data',
                'state': 'valid',
                'type': 'text',
                'value': test_value,
                'value_caption': test_value,
                'hidden': False,
            },
        ])],
    }

    # normal rabbit call
    assert json.loads(handle.delay.call_args_list[1][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('call', [
            {
                'name': 'execution',
                'state': 'valid',
                'type': 'text',
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ])],
    }

    # mongo log registry created for new process
    assert next(mongo[config["POINTER_COLLECTION"]].find({
        'id': alt_ptr_01.id,
    }))['node']['id'] == 'start_node'

    # mongo execution registry created for new process
    assert next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': alt_exe.id,
    }))['name'] == 'Simplest process ever started with: ' + test_value

    # teardown of the call node and end of first execution
    handler.step(json.loads(handle.delay.call_args_list[1][0][0]))

    # old execution is gone, new is here
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    assert main_ptr_02.execution.status == Execution.Status.FINISHED
    assert db_session.query(Execution).filter(
        Execution.status == Execution.Status.ONGOING,
    ).first().process_name == 'simple.2018-02-19.xml'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'data',
                'form_id': 'start_form',
                'form_index': 0,
                'ref': 'start_node.juan.0:start_form.data',
                'task_form_index': 0,
                'value': test_value,
                'value_caption': test_value,
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'execution',
                'form_id': 'call',
                'form_index': 0,
                'ref': 'call.__system__.0:call.execution',
                'task_form_index': 0,
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'call': [
            {
                'execution': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'call.__system__.0:call.execution',
                    },
                    'value': alt_exe.id,
                    'value_caption': alt_exe.id,
                },
            },
        ],
        'start_form': [
            {
                'data': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:start_form.data',
                    },
                    'value': test_value,
                    'value_caption': test_value,
                },
            },
        ],
    }


def test_call_node_render(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    main_ptr_01 = make_pointer('call-render.2020-04-24.xml', 'start_node')
    main_exe = main_ptr_01.execution
    test_value = random_string()

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    # teardown of first node and wakeup of call node
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('start_form', [
            {
                'name': 'data',
                'value': test_value,
                'value_caption': test_value,
            },
        ])],
    })

    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'call'

    alt_exe = db_session.query(Execution).filter(
        Execution.status == Execution.Status.ONGOING,
        Execution.process_name == 'simple.2018-02-19.xml',
    ).first()

    alt_ptr_01 = next(
        p_ for p_ in alt_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )

    # aditional rabbit call for new process
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': alt_ptr_01.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('start_form', [
            {
                'label': 'Info',
                'name': 'data',
                'state': 'valid',
                'type': 'text',
                'value': test_value,
                'value_caption': test_value,
                'hidden': False,
            },
        ])],
    }

    # normal rabbit call
    assert json.loads(handle.delay.call_args_list[1][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('call', [
            {
                'name': 'execution',
                'state': 'valid',
                'type': 'text',
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ])],
    }

    # mongo log registry created for new process
    assert next(mongo[config["POINTER_COLLECTION"]].find({
        'id': alt_ptr_01.id,
    }))['node']['id'] == 'start_node'

    # mongo execution registry created for new process
    assert next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': alt_exe.id,
    }))['name'] == 'Simplest process ever started with: ' + test_value

    # teardown of the call node and end of first execution
    handler.step(json.loads(handle.delay.call_args_list[1][0][0]))

    # old execution is gone, new is here
    db_session.refresh(main_ptr_02)
    db_session.refresh(main_exe)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    assert main_exe.status == Execution.Status.FINISHED
    assert db_session.query(Execution).filter(
        Execution.status == Execution.Status.ONGOING,
    ).first().process_name == 'simple.2018-02-19.xml'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'data',
                'form_id': 'start_form',
                'form_index': 0,
                'ref': 'start_node.juan.0:start_form.data',
                'task_form_index': 0,
                'value': test_value,
                'value_caption': test_value,
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'execution',
                'form_id': 'call',
                'form_index': 0,
                'ref': 'call.__system__.0:call.execution',
                'task_form_index': 0,
                'value': alt_ptr_01.execution.id,
                'value_caption': alt_ptr_01.execution.id,
            },
        ],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'call': [
            {
                'execution': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'call.__system__.0:call.execution',
                    },
                    'value': alt_ptr_01.execution.id,
                    'value_caption': alt_ptr_01.execution.id,
                },
            },
        ],
        'start_form': [
            {
                'data': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:start_form.data',
                    },
                    'value': test_value,
                    'value_caption': test_value,
                },
            },
        ],
    }
