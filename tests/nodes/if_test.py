from itertools import chain

from cacahuate.database import db_session
from cacahuate.handler.legacy import (
    Handler,
    compact_values,
    unpack_pointer_values,
)
from cacahuate.models import Pointer
from cacahuate.mongo import (
    execution_entry,
    pointer_entry,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

from pika.adapters.blocking_connection import BlockingChannel

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
)


def test_true_condition_node(config, mongo, mocker):
    ''' conditional node will be executed if its condition is true '''
    mocker.patch('cacahuate.tasks.handle.delay')

    # test setup
    handler = Handler(config)

    main_ptr_01 = make_pointer('condition.2018-05-17.xml', 'start_node')
    main_exe = main_ptr_01.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('mistery', [
            {
                'name': 'password',
                'type': 'text',
                'value': 'abrete sésamo',
                'value_caption': 'abrete sésamo',
            },
        ])],
    })

    # pointer moved
    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'condition1'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'input': [Form.state_json('condition1', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[0][0][0]))

    # pointer moved
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'mistical_node'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'password',
                'form_id': 'mistery',
                'form_index': 0,
                'ref': 'start_node.juan.0:mistery.password',
                'task_form_index': 0,
                'value': 'abrete sésamo',
                'value_caption': 'abrete sésamo',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'condition1',
                'form_index': 0,
                'ref': 'condition1.__system__.0:condition1.condition',
                'task_form_index': 0,
                'value': True,
                'value_caption': 'True',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'condition1': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'condition1.__system__.0:condition1.condition',
                    },
                    'value': True,
                    'value_caption': 'True',
                },
            },
        ],
        'mistery': [
            {
                'password': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:mistery.password',
                    },
                    'value': 'abrete sésamo',
                    'value_caption': 'abrete sésamo',
                },
            },
        ],
    }


def test_false_condition_node(config, mongo, mocker):
    ''' conditional node won't be executed if its condition is false '''
    mocker.patch('cacahuate.tasks.handle.delay')

    # test setup
    handler = Handler(config)

    main_ptr_01 = make_pointer('condition.2018-05-17.xml', 'start_node')
    main_exe = main_ptr_01.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('mistery', [
            {
                'name': 'password',
                'type': 'text',
                'value': '123456',
                'value_caption': '123456',
            },
        ])],
    })

    # assertions
    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'condition1'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'input': [Form.state_json('condition1', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[0][0][0]))

    # pointer moved
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'condition2'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'password',
                'form_id': 'mistery',
                'form_index': 0,
                'ref': 'start_node.juan.0:mistery.password',
                'task_form_index': 0,
                'value': '123456',
                'value_caption': '123456',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'condition1',
                'form_index': 0,
                'ref': 'condition1.__system__.0:condition1.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'condition1': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'condition1.__system__.0:condition1.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
        'mistery': [
            {
                'password': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:mistery.password',
                    },
                    'value': '123456',
                    'value_caption': '123456',
                },
            },
        ],
    }


def test_anidated_conditions(config, mongo, mocker):
    ''' conditional node won't be executed if its condition is false '''
    mocker.patch('cacahuate.tasks.handle.delay')

    # test setup
    handler = Handler(config)

    main_ptr_01 = make_pointer('anidated-conditions.2018-05-17.xml', 'a')
    main_exe = main_ptr_01.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('a', [
            {
                'name': 'a',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })

    # assertions
    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'outer'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'input': [Form.state_json('outer', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[0][0][0]))

    # assertions
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'b'

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_03.id,
        'user_identifier': make_user('paco').identifier,
        'input': [Form.state_json('b', [
            {
                'name': 'b',
                'value': '-1',
                'value_caption': '-1',
            },
        ])],
    })

    # assertions
    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.FINISHED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'inner1'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[1][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_04.id,
        'input': [Form.state_json('inner1', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[1][0][0]))

    # assertions
    db_session.refresh(main_ptr_04)
    assert main_ptr_04.status == Pointer.Status.FINISHED
    main_ptr_05 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_05.node_id == 'f'

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_05.id,
        'user_identifier': make_user('hugo').identifier,
        'input': [Form.state_json('f', [
            {
                'name': 'f',
                'value': '-1',
                'value_caption': '-1',
            },
        ])],
    })

    # assertions
    db_session.refresh(main_ptr_05)
    assert main_ptr_05.status == Pointer.Status.FINISHED
    main_ptr_06 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_06.node_id == 'g'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'a',
                'form_id': 'a',
                'form_index': 0,
                'ref': 'a.juan.0:a.a',
                'task_form_index': 0,
                'value': '1',
                'value_caption': '1',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'outer',
                'form_index': 0,
                'ref': 'outer.__system__.0:outer.condition',
                'task_form_index': 0,
                'value': True,
                'value_caption': 'True',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'paco',
                'field_id': 'b',
                'form_id': 'b',
                'form_index': 0,
                'ref': 'b.paco.0:b.b',
                'task_form_index': 0,
                'value': '-1',
                'value_caption': '-1',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'inner1',
                'form_index': 0,
                'ref': 'inner1.__system__.0:inner1.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'hugo',
                'field_id': 'f',
                'form_id': 'f',
                'form_index': 0,
                'ref': 'f.hugo.0:f.f',
                'task_form_index': 0,
                'value': '-1',
                'value_caption': '-1',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'a': [
            {
                'a': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'a.juan.0:a.a',
                    },
                    'value': '1',
                    'value_caption': '1',
                },
            },
        ],
        'b': [
            {
                'b': {
                    'authors': {
                        'paco',
                    },
                    'refs': {
                        'b.paco.0:b.b',
                    },
                    'value': '-1',
                    'value_caption': '-1',
                },
            },
        ],
        'f': [
            {
                'f': {
                    'authors': {
                        'hugo',
                    },
                    'refs': {
                        'f.hugo.0:f.f',
                    },
                    'value': '-1',
                    'value_caption': '-1',
                },
            },
        ],
        'inner1': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'inner1.__system__.0:inner1.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
        'outer': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'outer.__system__.0:outer.condition',
                    },
                    'value': True,
                    'value_caption': 'True',
                },
            },
        ],
    }


def test_ifelifelse_if(config, mongo, mocker):
    ''' else will be executed if preceding condition is false'''
    mocker.patch('cacahuate.tasks.handle.delay')
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.basic_publish'
    ))

    # test setup
    handler = Handler(config)

    main_ptr_01 = make_pointer('else.2018-07-10.xml', 'start_node')
    main_exe = main_ptr_01.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('secret01', [
            {
                'name': 'password',
                'type': 'text',
                'value': 'incorrect!',
                'value_caption': 'incorrect!',
            },
        ])],
    })

    # pointer moved
    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'condition01'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'input': [Form.state_json('condition01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[0][0][0]))

    # pointer moved
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'action01'

    # rabbit called to notify the user
    args = BlockingChannel.basic_publish.call_args[1]
    assert args['exchange'] == 'charpe_notify'

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_03.id,
        'user_identifier': make_user('jose').identifier,
        'input': [Form.state_json('form01', [
            {
                'name': 'answer',
                'value': 'answer',
                'value_caption': 'answer',
            },
        ])],
    })

    # arrives to final_action
    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.FINISHED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'final_action'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'password',
                'form_id': 'secret01',
                'form_index': 0,
                'ref': 'start_node.juan.0:secret01.password',
                'task_form_index': 0,
                'value': 'incorrect!',
                'value_caption': 'incorrect!',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'condition01',
                'form_index': 0,
                'ref': 'condition01.__system__.0:condition01.condition',
                'task_form_index': 0,
                'value': True,
                'value_caption': 'True',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'jose',
                'field_id': 'answer',
                'form_id': 'form01',
                'form_index': 0,
                'ref': 'action01.jose.0:form01.answer',
                'task_form_index': 0,
                'value': 'answer',
                'value_caption': 'answer',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'condition01': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'condition01.__system__.0:condition01.condition',
                    },
                    'value': True,
                    'value_caption': 'True',
                },
            },
        ],
        'form01': [
            {
                'answer': {
                    'authors': {
                        'jose',
                    },
                    'refs': {
                        'action01.jose.0:form01.answer',
                    },
                    'value': 'answer',
                    'value_caption': 'answer',
                },
            },
        ],
        'secret01': [
            {
                'password': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:secret01.password',
                    },
                    'value': 'incorrect!',
                    'value_caption': 'incorrect!',
                },
            },
        ],
    }


def test_ifelifelse_elif(config, mongo, mocker):
    ''' else will be executed if preceding condition is false'''
    mocker.patch('cacahuate.tasks.handle.delay')
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.basic_publish'
    ))

    # test setup
    handler = Handler(config)

    main_ptr_01 = make_pointer('else.2018-07-10.xml', 'start_node')
    main_exe = main_ptr_01.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('secret01', [
            {
                'name': 'password',
                'type': 'text',
                'value': 'hocus pocus',
                'value_caption': 'hocus pocus',
            },
        ])],
    })

    # pointer moved
    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'condition01'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'input': [Form.state_json('condition01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[0][0][0]))

    # pointer moved
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'elif01'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[1][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_03.id,
        'input': [Form.state_json('elif01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[1][0][0]))

    # pointer moved
    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.FINISHED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'action02'

    # rabbit called to notify the user
    BlockingChannel.basic_publish.assert_called_once()
    args = BlockingChannel.basic_publish.call_args[1]
    assert args['exchange'] == 'charpe_notify'

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_04.id,
        'user_identifier': make_user('jose').identifier,
        'input': [Form.state_json('form01', [
            {
                'name': 'answer',
                'value': 'answer',
                'value_caption': 'answer',
            },
        ])],
    })

    # arrives to final_action
    db_session.refresh(main_ptr_04)
    assert main_ptr_04.status == Pointer.Status.FINISHED
    main_ptr_05 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_05.node_id == 'final_action'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'password',
                'form_id': 'secret01',
                'form_index': 0,
                'ref': 'start_node.juan.0:secret01.password',
                'task_form_index': 0,
                'value': 'hocus pocus',
                'value_caption': 'hocus pocus',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'condition01',
                'form_index': 0,
                'ref': 'condition01.__system__.0:condition01.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'elif01',
                'form_index': 0,
                'ref': 'elif01.__system__.0:elif01.condition',
                'task_form_index': 0,
                'value': True,
                'value_caption': 'True',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'jose',
                'field_id': 'answer',
                'form_id': 'form01',
                'form_index': 0,
                'ref': 'action02.jose.0:form01.answer',
                'task_form_index': 0,
                'value': 'answer',
                'value_caption': 'answer',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'condition01': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'condition01.__system__.0:condition01.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
        'elif01': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'elif01.__system__.0:elif01.condition',
                    },
                    'value': True,
                    'value_caption': 'True',
                },
            },
        ],
        'form01': [
            {
                'answer': {
                    'authors': {
                        'jose',
                    },
                    'refs': {
                        'action02.jose.0:form01.answer',
                    },
                    'value': 'answer',
                    'value_caption': 'answer',
                },
            },
        ],
        'secret01': [
            {
                'password': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:secret01.password',
                    },
                    'value': 'hocus pocus',
                    'value_caption': 'hocus pocus',
                },
            },
        ],
    }


def test_ifelifelse_else(config, mongo, mocker):
    ''' else will be executed if preceding condition is false'''
    mocker.patch('cacahuate.tasks.handle.delay')
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.basic_publish'
    ))

    # test setup
    handler = Handler(config)

    main_ptr_01 = make_pointer('else.2018-07-10.xml', 'start_node')
    main_exe = main_ptr_01.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('secret01', [
            {
                'name': 'password',
                'type': 'text',
                'value': 'cuca',
                'value_caption': 'cuca',
            },
        ])],
    })

    # pointer moved
    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'condition01'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'input': [Form.state_json('condition01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[0][0][0]))

    # pointer moved
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'elif01'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[1][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_03.id,
        'input': [Form.state_json('elif01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[1][0][0]))

    # pointer moved
    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.FINISHED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'else01'

    # rabbit called
    assert json.loads(handle.delay.call_args_list[2][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_04.id,
        'input': [Form.state_json('else01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[2][0][0]))

    # pointer moved
    db_session.refresh(main_ptr_04)
    assert main_ptr_04.status == Pointer.Status.FINISHED
    main_ptr_05 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_05.node_id == 'action03'

    # rabbit called to notify the user
    BlockingChannel.basic_publish.assert_called_once()
    args = BlockingChannel.basic_publish.call_args[1]
    assert args['exchange'] == 'charpe_notify'

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_05.id,
        'user_identifier': make_user('jose').identifier,
        'input': [Form.state_json('form01', [
            {
                'name': 'answer',
                'value': 'answer',
                'value_caption': 'answer',
            },
        ])],
    })

    # arrives to final_action
    db_session.refresh(main_ptr_05)
    assert main_ptr_05.status == Pointer.Status.FINISHED
    main_ptr_06 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_06.node_id == 'final_action'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'password',
                'form_id': 'secret01',
                'form_index': 0,
                'ref': 'start_node.juan.0:secret01.password',
                'task_form_index': 0,
                'value': 'cuca',
                'value_caption': 'cuca',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'condition01',
                'form_index': 0,
                'ref': 'condition01.__system__.0:condition01.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'elif01',
                'form_index': 0,
                'ref': 'elif01.__system__.0:elif01.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'else01',
                'form_index': 0,
                'ref': 'else01.__system__.0:else01.condition',
                'task_form_index': 0,
                'value': True,
                'value_caption': 'True',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'jose',
                'field_id': 'answer',
                'form_id': 'form01',
                'form_index': 0,
                'ref': 'action03.jose.0:form01.answer',
                'task_form_index': 0,
                'value': 'answer',
                'value_caption': 'answer',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'condition01': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'condition01.__system__.0:condition01.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
        'elif01': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'elif01.__system__.0:elif01.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
        'else01': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'else01.__system__.0:else01.condition',
                    },
                    'value': True,
                    'value_caption': 'True',
                },
            },
        ],
        'form01': [
            {
                'answer': {
                    'authors': {
                        'jose',
                    },
                    'refs': {
                        'action03.jose.0:form01.answer',
                    },
                    'value': 'answer',
                    'value_caption': 'answer',
                },
            },
        ],
        'secret01': [
            {
                'password': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:secret01.password',
                    },
                    'value': 'cuca',
                    'value_caption': 'cuca',
                },
            },
        ],
    }


def test_invalidated_conditional(config, mongo, mocker):
    ''' a condiitonal depends on an invalidated field, if it changes during
    the second response it must take the second value '''
    mocker.patch('cacahuate.tasks.handle.delay')

    # test setup
    handler = Handler(config)

    process_name = 'condition_invalidated.2019-10-08.xml'
    main_ptr_01 = make_pointer(process_name, 'start_node')
    main_exe = main_ptr_01.execution
    xml = Xml.load(config, main_exe.process_name, direct=True)

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    # initial rabbit call
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan', 'Juan').identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'value',
                'type': 'int',
                'value': 3,
                'value_caption': '3',
            },
        ])],
    })

    # arrives to if_node
    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'if_node'

    # if rabbit call
    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'input': [Form.state_json('if_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[0][0][0]))

    # arrives to if_validation
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'if_validation_node'

    # if's call to validation
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_03.id,
        'user_identifier': make_user('jose', 'Jose').identifier,
        'input': [Form.state_json('if_validation_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'I do not like it',
                'value_caption': 'I do not like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:form1.value',
                }],
                'value_caption': '',
            },
        ])],
    })

    # returns to start_node
    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.FINISHED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'start_node'

    # second lap
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_04.id,
        'user_identifier': make_user('luis', 'Luis').identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'value',
                'type': 'int',
                'value': -3,
                'value_caption': '-3',
            },
        ])],
    })

    # arrives to if_node again
    db_session.refresh(main_ptr_04)
    assert main_ptr_04.status == Pointer.Status.FINISHED
    main_ptr_05 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_05.node_id == 'if_node'

    assert json.loads(handle.delay.call_args_list[1][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_05.id,
        'input': [Form.state_json('if_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[1][0][0]))

    # arrives to elif_node
    db_session.refresh(main_ptr_05)
    assert main_ptr_05.status == Pointer.Status.FINISHED
    main_ptr_06 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_06.node_id == 'elif_node'

    # elif node's condition is true
    assert json.loads(handle.delay.call_args_list[2][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_06.id,
        'input': [Form.state_json('elif_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[2][0][0]))

    # arrives to elif_validation
    db_session.refresh(main_ptr_06)
    assert main_ptr_06.status == Pointer.Status.FINISHED
    main_ptr_07 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_07.node_id == 'elif_validation_node'

    # elif's call to validation
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_07.id,
        'user_identifier': make_user('pepe', 'Pepe').identifier,
        'input': [Form.state_json('elif_validation_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'Ugly... nope',
                'value_caption': 'Ugly... nope',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.luis.0:form1.value',
                }],
                'value_caption': '',
            },
        ])],
    })

    # returns to start_node
    db_session.refresh(main_ptr_07)
    assert main_ptr_07.status == Pointer.Status.FINISHED
    main_ptr_08 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_08.node_id == 'start_node'

    # third lap
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_08.id,
        'user_identifier': make_user('hugo', 'Hugo').identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'value',
                'type': 'int',
                'value': 0,
                'value_caption': '0',
            },
        ])],
    })

    # arrives to if_node again again
    db_session.refresh(main_ptr_08)
    assert main_ptr_08.status == Pointer.Status.FINISHED
    main_ptr_09 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_09.node_id == 'if_node'

    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': main_ptr_09.id,
        'input': [Form.state_json('if_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    # if third rabbit call
    handler.step(rabbit_call)

    # arrives to elif_node again
    main_ptr_10 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_10.node_id == 'elif_node'

    assert json.loads(handle.delay.call_args_list[4][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_10.id,
        'input': [Form.state_json('elif_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[4][0][0]))

    # arrives to else_node
    main_ptr_11 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_11.node_id == 'else_node'

    # else node's condition is true
    assert json.loads(handle.delay.call_args_list[5][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_11.id,
        'input': [Form.state_json('else_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    handler.step(json.loads(handle.delay.call_args_list[5][0][0]))

    # arrives to if_validation
    main_ptr_12 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_12.node_id == 'else_validation_node'

    # else's call to validation
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_12.id,
        'user_identifier': make_user('paco', 'Paco').identifier,
        'input': [Form.state_json('else_validation_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'What? No!',
                'value_caption': 'What? No!',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.hugo.0:form1.value',
                }],
                'value_caption': '',
            },
        ])],
    })

    # returns to start_node
    main_ptr_13 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_13.node_id == 'start_node'

    # state is coherent
    assert next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': main_exe.id,
    }))['state'] == {
        '_type': ':sorted_map',
        'items': {
            'start_node': {
                **xml.get_node('start_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'juan': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'form1',
                                [
                                    {
                                        'name': 'value',
                                        'type': 'int',
                                        'value': 3,
                                        'value_caption': '3',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            )],
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'identifier': 'juan',
                                'fullname': 'Juan',
                                'email': 'juan@mailinator.com',
                            },
                        },
                        'luis': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'form1',
                                [
                                    {
                                        'name': 'value',
                                        'type': 'int',
                                        'value': -3,
                                        'value_caption': '-3',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            )],
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'identifier': 'luis',
                                'fullname': 'Luis',
                                'email': 'luis@mailinator.com',
                            },
                        },
                        'hugo': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'form1',
                                [
                                    {
                                        'name': 'value',
                                        'type': 'int',
                                        'value': 0,
                                        'value_caption': '0',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            )],
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'identifier': 'hugo',
                                'fullname': 'Hugo',
                                'email': 'hugo@mailinator.com',
                            },
                        },
                    },
                },
                'state': 'ongoing',
                'comment': 'What? No!',
            },

            'if_node': {
                **xml.get_node('if_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        '__system__': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'if_node',
                                [
                                    {
                                        'name': 'condition',
                                        'value': False,
                                        'value_caption': 'False',
                                        'type': 'bool',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            )],
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'identifier': '__system__',
                                'fullname': 'System',
                                'email': '',
                            },
                        },
                    },
                },
                'state': 'invalid',
                'comment': 'What? No!',
            },

            'if_validation_node': {
                **xml.get_node('if_validation_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'jose': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'if_validation_node',
                                [
                                    {
                                        'name': 'response',
                                        'value': 'reject',
                                        'value_caption': 'reject',
                                        'state': 'invalid',
                                    },
                                    {
                                        'name': 'comment',
                                        'value': 'I do not like it',
                                        'value_caption': (
                                            'I do not like it'
                                        ),
                                    },
                                    {
                                        'name': 'inputs',
                                        'value': [{
                                            'ref': (
                                                'start_node.juan.0:form1'
                                                '.value'
                                            ),
                                        }],
                                        'value_caption': '',
                                    },
                                ],
                                state='invalid',
                            )],
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'fullname': 'Jose',
                                'identifier': 'jose',
                                'email': 'jose@mailinator.com',
                            },
                        },
                    },
                },
                'state': 'invalid',
                'comment': 'What? No!',
            },

            'elif_node': {
                **xml.get_node('elif_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        '__system__': {
                            '_type': 'actor',
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'fullname': 'System',
                                'identifier': '__system__',
                                'email': '',
                            },
                            'forms': [Form.state_json(
                                'elif_node',
                                [
                                    {
                                        'name': 'condition',
                                        'value': False,
                                        'value_caption': 'False',
                                        'type': 'bool',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            )],
                        },
                    },
                },
                'state': 'invalid',
                'comment': 'What? No!',
            },

            'elif_validation_node': {
                **xml.get_node('elif_validation_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'pepe': {
                            '_type': 'actor',
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'fullname': 'Pepe',
                                'identifier': 'pepe',
                                'email': 'pepe@mailinator.com',
                            },
                            'forms': [Form.state_json(
                                'elif_validation_node',
                                [
                                    {
                                        'name': 'response',
                                        'value': 'reject',
                                        'value_caption': 'reject',
                                        'state': 'invalid',
                                    },
                                    {
                                        'name': 'comment',
                                        'value': 'Ugly... nope',
                                        'value_caption': (
                                            'Ugly... nope'
                                        ),
                                    },
                                    {
                                        'name': 'inputs',
                                        'value': [{
                                            'ref': (
                                                'start_node.luis.0:form1'
                                                '.value'
                                            ),
                                        }],
                                        'value_caption': '',
                                    },
                                ],
                                state='invalid',
                            )],
                        },
                    },
                },
                'state': 'invalid',
                'comment': 'What? No!',
            },

            'else_node': {
                **xml.get_node('else_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        '__system__': {
                            '_type': 'actor',
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'fullname': 'System',
                                'identifier': '__system__',
                                'email': '',
                            },
                            'forms': [Form.state_json(
                                'else_node',
                                [
                                    {
                                        'name': 'condition',
                                        'value': True,
                                        'value_caption': 'True',
                                        'type': 'bool',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            )],
                        },
                    },
                },
                'state': 'invalid',
                'comment': 'What? No!',
            },

            'else_validation_node': {
                **xml.get_node('else_validation_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'paco': {
                            '_type': 'actor',
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'fullname': 'Paco',
                                'identifier': 'paco',
                                'email': 'paco@mailinator.com',
                            },
                            'forms': [Form.state_json(
                                'else_validation_node',
                                [
                                    {
                                        'name': 'response',
                                        'value': 'reject',
                                        'value_caption': 'reject',
                                        'state': 'invalid',
                                    },
                                    {
                                        'name': 'comment',
                                        'value': 'What? No!',
                                        'value_caption': 'What? No!',
                                    },
                                    {
                                        'name': 'inputs',
                                        'value': [{
                                            'ref': (
                                                'start_node.hugo.0:form1'
                                                '.value'
                                            ),
                                        }],
                                        'value_caption': '',
                                    },
                                ],
                                state='invalid'),
                            ],
                        },
                    },
                },
                'state': 'invalid',
                'comment': 'What? No!',
            },
        },
        'item_order': [
            'start_node',
            'if_node',
            'if_validation_node',
            'elif_node',
            'elif_validation_node',
            'else_node',
            'else_validation_node',
        ],
    }

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'value',
                'form_id': 'form1',
                'form_index': 0,
                'ref': 'start_node.juan.0:form1.value',
                'task_form_index': 0,
                'value': 3,
                'value_caption': '3',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'if_node',
                'form_index': 0,
                'ref': 'if_node.__system__.0:if_node.condition',
                'task_form_index': 0,
                'value': True,
                'value_caption': 'True',
            },
        ],
        [
            {
                'action': 'delete',
                'author': 'jose',
                'field_id': 'value',
                'form_id': 'form1',
                'form_index': None,
                'ref': 'start_node.juan.0:form1.value',
                'task_form_index': 0,
                'value': None,
                'value_caption': '',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'luis',
                'field_id': 'value',
                'form_id': 'form1',
                'form_index': 0,
                'ref': 'start_node.luis.0:form1.value',
                'task_form_index': 0,
                'value': -3,
                'value_caption': '-3',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'if_node',
                'form_index': 0,
                'ref': 'if_node.__system__.0:if_node.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'elif_node',
                'form_index': 0,
                'ref': 'elif_node.__system__.0:elif_node.condition',
                'task_form_index': 0,
                'value': True,
                'value_caption': 'True',
            },
        ],
        [
            {
                'action': 'delete',
                'author': 'pepe',
                'field_id': 'value',
                'form_id': 'form1',
                'form_index': None,
                'ref': 'start_node.luis.0:form1.value',
                'task_form_index': 0,
                'value': None,
                'value_caption': '',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'hugo',
                'field_id': 'value',
                'form_id': 'form1',
                'form_index': 0,
                'ref': 'start_node.hugo.0:form1.value',
                'task_form_index': 0,
                'value': 0,
                'value_caption': '0',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'if_node',
                'form_index': 0,
                'ref': 'if_node.__system__.0:if_node.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'elif_node',
                'form_index': 0,
                'ref': 'elif_node.__system__.0:elif_node.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'else_node',
                'form_index': 0,
                'ref': 'else_node.__system__.0:else_node.condition',
                'task_form_index': 0,
                'value': True,
                'value_caption': 'True',
            },
        ],
        [
            {
                'action': 'delete',
                'author': 'paco',
                'field_id': 'value',
                'form_id': 'form1',
                'form_index': None,
                'ref': 'start_node.hugo.0:form1.value',
                'task_form_index': 0,
                'value': None,
                'value_caption': '',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'elif_node': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'elif_node.__system__.0:elif_node.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
        'else_node': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'else_node.__system__.0:else_node.condition',
                    },
                    'value': True,
                    'value_caption': 'True',
                },
            },
        ],
        'form1': [
            {
                'value': {
                    'authors': {
                        'hugo',
                        'jose',
                        'juan',
                        'luis',
                        'paco',
                        'pepe',
                    },
                    'refs': {
                        'start_node.hugo.0:form1.value',
                        'start_node.juan.0:form1.value',
                        'start_node.luis.0:form1.value',
                    },
                    'value': None,
                    'value_caption': '',
                },
            },
        ],
        'if_node': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'if_node.__system__.0:if_node.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
    }
