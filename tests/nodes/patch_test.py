from itertools import chain

from cacahuate.database import db_session
from cacahuate.handler.legacy import (
    Handler,
    compact_values,
    unpack_pointer_values,
)
from cacahuate.models import Pointer
from cacahuate.mongo import (
    execution_entry,
    pointer_entry,
)
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

from tests.utils import make_pointer, make_user


def test_patch_invalidate(config, mongo, mocker):
    ''' patch that only invalidates '''
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    main_ptr_01 = make_pointer('exit_request.2018-03-20.xml', 'requester')
    main_exe = main_ptr_01.execution
    xml = Xml.load(config, main_exe.process_name, direct=True)

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    # requester fills the form
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan', 'Juan').identifier,
        'input': [
            Form.state_json('exit_form', [
                {
                    'name': 'reason',
                    'state': 'valid',
                    'type': 'text',
                    'value': 'want to pee',
                    'value_caption': 'want to pee',
                },
            ]),
            Form.state_json('code_form', [
                {
                    'name': 'code',
                    'state': 'valid',
                    'type': 'text',
                    'value': 'kadabra',
                    'value_caption': 'kadabra',
                },
            ]),
        ],
    })

    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'manager'

    # manager says yes
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'user_identifier': make_user('luis', 'Luis').identifier,
        'input': [Form.state_json('auth_form', [
            {
                'name': 'auth',
                'state': 'valid',
                'type': 'radio',
                'value': 'yes',
                'value_caption': 'Ándale mijito, ve',
            },
        ])],
    })

    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'security'

    # patch request happens
    handler.patch({
        'command': 'patch',
        'execution_id': main_exe.id,
        'user_identifier': make_user('pepe', 'Pepe').identifier,
        'comment': 'pee is not a valid reason',
        'inputs': [{
            'ref': 'requester.juan.0:exit_form.reason',
        }],
    })

    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.CANCELLED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'requester'

    # nodes with pointers are marked as unfilled or invalid in execution state
    assert next(mongo[config['EXECUTION_COLLECTION']].find({
        'id': main_exe.id,
    }))['state'] == {
        '_type': ':sorted_map',
        'items': {
            'requester': {
                **xml.get_node('requester').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'juan': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'exit_form',
                                [
                                    {
                                        'name': 'reason',
                                        'type': 'text',
                                        'value': 'want to pee',
                                        'value_caption': 'want to pee',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            ), Form.state_json(
                                'code_form',
                                [
                                    {
                                        'name': 'code',
                                        'type': 'text',
                                        'value': 'kadabra',
                                        'value_caption': 'kadabra',
                                        'state': 'valid',
                                    },
                                ],
                                state='valid',
                            )],
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'identifier': 'juan',
                                'fullname': 'Juan',
                                'email': 'juan@mailinator.com',
                            },
                        },
                    },
                },
                'state': 'ongoing',
                'comment': 'pee is not a valid reason',
            },

            'manager': {
                **xml.get_node('manager').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'luis': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'auth_form',
                                [
                                    {
                                        'name': 'auth',
                                        'type': 'radio',
                                        'value': 'yes',
                                        'value_caption': 'Ándale mijito, ve',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            )],
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'identifier': 'luis',
                                'fullname': 'Luis',
                                'email': 'luis@mailinator.com',
                            },
                        },
                    },
                },
                'state': 'invalid',
                'comment': 'pee is not a valid reason',
            },

            'security': {
                **xml.get_node('security').get_state(),
            },

            'noauth': {
                **xml.get_node('noauth').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {},
                },
            },
        },
        'item_order': ['requester', 'manager', 'security', 'noauth'],
    }

    # entries in pointer collection get a state of cancelled by patch request
    security_pointer_state = mongo[config['POINTER_COLLECTION']].find_one({
        'id': main_ptr_03.id,
    })

    assert security_pointer_state['state'] == 'cancelled'
    assert security_pointer_state['patch']['actor'] == {
        '_type': 'user',
        'fullname': 'Pepe',
        'identifier': 'pepe',
        'email': 'pepe@mailinator.com',
    }
    assert security_pointer_state['patch']['inputs'] == [
        {
            'ref': 'requester.juan.0:exit_form.reason',
        },
    ]

    execution_document = mongo[config["EXECUTION_COLLECTION"]].find_one({
        'id': main_exe.id,
    })

    expected_values = {
        'auth_form': [{'auth': 'yes'}],
        'code_form': [{'code': 'kadabra'}],
        'exit_form': [{'reason': 'want to pee'}],
    }
    assert {
        'auth_form': execution_document['values']['auth_form'],
        'code_form': execution_document['values']['code_form'],
        'exit_form': execution_document['values']['exit_form'],
    } == expected_values

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'reason',
                'form_id': 'exit_form',
                'form_index': 0,
                'ref': 'requester.juan.0:exit_form.reason',
                'task_form_index': 0,
                'value': 'want to pee',
                'value_caption': 'want to pee',
            },
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'code',
                'form_id': 'code_form',
                'form_index': 0,
                'ref': 'requester.juan.1:code_form.code',
                'task_form_index': 1,
                'value': 'kadabra',
                'value_caption': 'kadabra',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'luis',
                'field_id': 'auth',
                'form_id': 'auth_form',
                'form_index': 0,
                'ref': 'manager.luis.0:auth_form.auth',
                'task_form_index': 0,
                'value': 'yes',
                'value_caption': 'Ándale mijito, ve',
            },
        ],
        [
            {
                'action': 'delete',
                'author': 'pepe',
                'field_id': 'reason',
                'form_id': 'exit_form',
                'form_index': None,
                'ref': 'requester.juan.0:exit_form.reason',
                'task_form_index': 0,
                'value': None,
                'value_caption': None,
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'auth_form': [
            {
                'auth': {
                    'authors': {
                        'luis',
                    },
                    'refs': {
                        'manager.luis.0:auth_form.auth',
                    },
                    'value': 'yes',
                    'value_caption': 'Ándale mijito, ve',
                },
            },
        ],
        'code_form': [
            {
                'code': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'requester.juan.1:code_form.code',
                    },
                    'value': 'kadabra',
                    'value_caption': 'kadabra',
                },
            },
        ],
        'exit_form': [
            {
                'reason': {
                    'authors': {
                        'juan',
                        'pepe',
                    },
                    'refs': {
                        'requester.juan.0:exit_form.reason',
                    },
                    'value': None,
                    'value_caption': None,
                },
            },
        ],
    }


def test_patch_set_value(config, mongo):
    ''' patch and set new data '''
    handler = Handler(config)

    main_ptr_01 = make_pointer('exit_request.2018-03-20.xml', 'requester')
    main_exe = main_ptr_01.execution
    xml = Xml.load(config, main_exe.process_name, direct=True)

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    # requester fills the form
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan', 'Juan').identifier,
        'input': [
            Form.state_json('exit_form', [
                {
                    'name': 'reason',
                    'state': 'valid',
                    'type': 'text',
                    'value': 'want to pee',
                    'value_caption': 'want to pee',
                },
            ]),
            Form.state_json('code_form', [
                {
                    'name': 'code',
                    'state': 'valid',
                    'type': 'text',
                    'value': 'kadabra',
                    'value_caption': 'kadabra',
                },
            ]),
        ],
    })

    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'manager'

    # manager says yes
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'user_identifier': make_user('luis', 'Luis').identifier,
        'input': [Form.state_json('auth_form', [
            {
                'name': 'auth',
                'state': 'valid',
                'type': 'radio',
                'value': 'yes',
                'value_caption': 'Ándale mijito, ve',
            },
        ])],
    })

    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'security'

    # patch request happens
    handler.patch({
        'command': 'patch',
        'execution_id': main_exe.id,
        'user_identifier': make_user('pepe', 'Pepe').identifier,
        'comment': 'pee is not a valid reason',
        'inputs': [
            {
                'ref': 'requester.juan.0:exit_form.reason',
                'value': 'am hungry',
                'value_caption': 'am hungry',
            },
            {
                'ref': 'requester.juan.1:code_form.code',
                'value': 'alakazam',
                'value_caption': 'alakazam',
            },
        ],
    })

    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.CANCELLED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'manager'

    # nodes with pointers are marked as unfilled or invalid in execution state
    assert next(mongo[config['EXECUTION_COLLECTION']].find({
        'id': main_exe.id,
    }))['state'] == {
        '_type': ':sorted_map',
        'items': {
            'requester': {
                **xml.get_node('requester').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'juan': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'exit_form',
                                [
                                    {
                                        'name': 'reason',
                                        'type': 'text',
                                        'value': 'am hungry',
                                        'value_caption': 'am hungry',
                                        'state': 'valid',
                                    },
                                ],
                                state='valid',
                            ), Form.state_json(
                                'code_form',
                                [
                                    {
                                        'name': 'code',
                                        'type': 'text',
                                        'value': 'alakazam',
                                        'value_caption': 'alakazam',
                                        'state': 'valid',
                                    },
                                ],
                                state='valid',
                            )],
                            'state': 'valid',
                            'user': {
                                '_type': 'user',
                                'identifier': 'juan',
                                'fullname': 'Juan',
                                'email': 'juan@mailinator.com',
                            },
                        },
                    },
                },
                'state': 'valid',
                'comment': 'pee is not a valid reason',
            },

            'manager': {
                **xml.get_node('manager').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'luis': {
                            '_type': 'actor',
                            'forms': [Form.state_json(
                                'auth_form',
                                [
                                    {
                                        'name': 'auth',
                                        'type': 'radio',
                                        'value': 'yes',
                                        'value_caption': 'Ándale mijito, ve',
                                        'state': 'invalid',
                                    },
                                ],
                                state='invalid',
                            )],
                            'state': 'invalid',
                            'user': {
                                '_type': 'user',
                                'identifier': 'luis',
                                'fullname': 'Luis',
                                'email': 'luis@mailinator.com',
                            },
                        },
                    },
                },
                'state': 'ongoing',
                'comment': 'pee is not a valid reason',
            },

            'security': {
                **xml.get_node('security').get_state(),
            },

            'noauth': {
                **xml.get_node('noauth').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {},
                },
            },
        },
        'item_order': ['requester', 'manager', 'security', 'noauth'],
    }

    # entries in pointer collection get a state of cancelled by patch request
    security_pointer_state = mongo[config['POINTER_COLLECTION']].find_one({
        'id': main_ptr_03.id,
    })

    assert security_pointer_state['state'] == 'cancelled'
    assert security_pointer_state['patch']['actor'] == {
        '_type': 'user',
        'fullname': 'Pepe',
        'identifier': 'pepe',
        'email': 'pepe@mailinator.com',
    }
    assert security_pointer_state['patch']['inputs'] == [
        {
            'ref': 'requester.juan.0:exit_form.reason',
            'value': 'am hungry',
            'value_caption': 'am hungry',
        },
        {
            'ref': 'requester.juan.1:code_form.code',
            'value': 'alakazam',
            'value_caption': 'alakazam',
        },
    ]

    execution_document = mongo[config["EXECUTION_COLLECTION"]].find_one({
        'id': main_exe.id,
    })

    expected_values = {
        'auth_form': [{'auth': 'yes'}],
        'code_form': [{'code': 'alakazam'}],
        'exit_form': [{'reason': 'am hungry'}],
    }
    assert {
        'auth_form': execution_document['values']['auth_form'],
        'code_form': execution_document['values']['code_form'],
        'exit_form': execution_document['values']['exit_form'],
    } == expected_values

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'reason',
                'form_id': 'exit_form',
                'form_index': 0,
                'ref': 'requester.juan.0:exit_form.reason',
                'task_form_index': 0,
                'value': 'want to pee',
                'value_caption': 'want to pee',
            },
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'code',
                'form_id': 'code_form',
                'form_index': 0,
                'ref': 'requester.juan.1:code_form.code',
                'task_form_index': 1,
                'value': 'kadabra',
                'value_caption': 'kadabra',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'luis',
                'field_id': 'auth',
                'form_id': 'auth_form',
                'form_index': 0,
                'ref': 'manager.luis.0:auth_form.auth',
                'task_form_index': 0,
                'value': 'yes',
                'value_caption': 'Ándale mijito, ve',
            },
        ],
        [
            {
                'action': 'update',
                'author': 'pepe',
                'field_id': 'reason',
                'form_id': 'exit_form',
                'form_index': None,
                'ref': 'requester.juan.0:exit_form.reason',
                'task_form_index': 0,
                'value': 'am hungry',
                'value_caption': 'am hungry',
            },
            {
                'action': 'update',
                'author': 'pepe',
                'field_id': 'code',
                'form_id': 'code_form',
                'form_index': None,
                'ref': 'requester.juan.1:code_form.code',
                'task_form_index': 1,
                'value': 'alakazam',
                'value_caption': 'alakazam',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'auth_form': [
            {
                'auth': {
                    'authors': {
                        'luis',
                    },
                    'refs': {
                        'manager.luis.0:auth_form.auth',
                    },
                    'value': 'yes',
                    'value_caption': 'Ándale mijito, ve',
                },
            },
        ],
        'code_form': [
            {
                'code': {
                    'authors': {
                        'juan',
                        'pepe',
                    },
                    'refs': {
                        'requester.juan.1:code_form.code',
                    },
                    'value': 'alakazam',
                    'value_caption': 'alakazam',
                },
            },
        ],
        'exit_form': [
            {
                'reason': {
                    'authors': {
                        'juan',
                        'pepe',
                    },
                    'refs': {
                        'requester.juan.0:exit_form.reason',
                    },
                    'value': 'am hungry',
                    'value_caption': 'am hungry',
                },
            },
        ],
    }


def test_patch_set_value_multiple(config, mongo, mocker):
    ''' patch and set new data (multiple)'''
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    main_ptr_01 = make_pointer('gift-request.2020-04-05.xml', 'solicitud')
    main_exe = main_ptr_01.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    # requester fills the form
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('kysxd', 'KYSXD').identifier,
        'input': [
            Form.state_json('viaticos', [
                {
                    'name': 'galletas',
                    'state': 'select',
                    'type': 'field',
                    'value': 'yes',
                    'value_caption': 'Si',
                },
            ]),
            Form.state_json('condicionales', [
                {
                    'name': 'comportamiento',
                    'state': 'valid',
                    'type': 'select',
                    'value': 'bueno',
                    'value_caption': 'Si',
                },
            ]),
            Form.state_json('regalos', [
                {
                    'name': 'regalo',
                    'state': 'valid',
                    'type': 'text',
                    'value': 'Max Iron',
                    'value_caption': 'Max Iron',
                },
                {
                    'name': 'costo',
                    'state': 'valid',
                    'type': 'currency',
                    'value': 350.0,
                    'value_caption': '350.0',
                },
            ]),
            Form.state_json('regalos', [
                {
                    'name': 'regalo',
                    'state': 'valid',
                    'type': 'text',
                    'value': 'Mega boy',
                    'value_caption': 'Mega boy',
                },
                {
                    'name': 'costo',
                    'state': 'valid',
                    'type': 'field',
                    'value': 120.0,
                    'value_caption': '120.0',
                },
            ]),
            Form.state_json('regalos', [
                {
                    'name': 'regalo',
                    'state': 'valid',
                    'type': 'text',
                    'value': 'Brobocop',
                    'value_caption': 'Brobocop',
                },
            ]),
        ],
    })

    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'if_malo'

    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('if_malo', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
    })

    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'preparacion'

    # patch request happens
    handler.patch({
        'command': 'patch',
        'execution_id': main_exe.id,
        'user_identifier': make_user('alex', 'Alex').identifier,
        'comment': 'Informacion equivocada',
        'inputs': [
            {
                'ref': 'solicitud.kysxd.3:regalos.regalo',
                'value': 'Mega bro',
                'value_caption': 'Mega bro',
            },
            {
                'ref': 'solicitud.kysxd.2:regalos.regalo',
                'value': 'Action bro',
                'value_caption': 'Action bro',
            },
            {
                'ref': 'solicitud.kysxd.5:regalos.costo',
                'value': 350.0,
                'value_caption': '350.0',
            },
            {
                'ref': 'solicitud.kysxd.2:regalos.costo',
                'value': 10.0,
                'value_caption': '10.0',
            },
        ],
    })

    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.CANCELLED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'if_malo'

    # nodes with pointers are marked as unfilled or invalid in execution state
    execution_document = mongo[config['EXECUTION_COLLECTION']].find_one({
        'id': main_exe.id,
    })

    # values sent are set
    actors = execution_document['state']['items']['solicitud']['actors']
    actor = actors['items']['kysxd']

    # sanity check for the non-multiple forms
    _form = actor['forms'][0]
    _input = _form['inputs']['items']['galletas']
    assert _input['value'] == 'yes'
    assert _input['value_caption'] == 'Si'

    _form = actor['forms'][1]
    _input = _form['inputs']['items']['comportamiento']
    assert _input['value'] == 'bueno'
    assert _input['value_caption'] == 'Si'

    # check multiforms
    # first
    _form = actor['forms'][2]
    _input = _form['inputs']['items']['regalo']
    assert _input['value'] == 'Action bro'
    assert _input['value_caption'] == 'Action bro'

    _form = actor['forms'][2]
    _input = _form['inputs']['items']['costo']
    assert _input['value'] == 10.0
    assert _input['value_caption'] == '10.0'

    # second
    _form = actor['forms'][3]
    _input = _form['inputs']['items']['regalo']
    assert _input['value'] == 'Mega bro'
    assert _input['value_caption'] == 'Mega bro'

    _form = actor['forms'][3]
    _input = _form['inputs']['items']['costo']
    assert _input['value'] == 120.0
    assert _input['value_caption'] == '120.0'

    # third
    _form = actor['forms'][4]
    _input = _form['inputs']['items']['regalo']
    assert _input['value'] == 'Brobocop'
    assert _input['value_caption'] == 'Brobocop'

    # unexistant key doesn't update
    assert 'costo' not in _form['inputs']['items']

    expected_values = {
        'viaticos': [{'galletas': 'yes'}],
        'condicionales': [{'comportamiento': 'bueno'}],
        'if_malo': [{'condition': False}],
        'regalos': [
            {'regalo': 'Action bro', 'costo': 10.0},
            {'regalo': 'Mega bro', 'costo': 120.0},
            {'regalo': 'Brobocop'},
        ],
    }
    assert {
        'viaticos': execution_document['values']['viaticos'],
        'condicionales': execution_document['values']['condicionales'],
        'if_malo': execution_document['values']['if_malo'],
        'regalos': execution_document['values']['regalos'],
    } == expected_values

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'kysxd',
                'field_id': 'galletas',
                'form_id': 'viaticos',
                'form_index': 0,
                'ref': 'solicitud.kysxd.0:viaticos.galletas',
                'task_form_index': 0,
                'value': 'yes',
                'value_caption': 'Si',
            },
            {
                'action': 'create',
                'author': 'kysxd',
                'field_id': 'comportamiento',
                'form_id': 'condicionales',
                'form_index': 0,
                'ref': 'solicitud.kysxd.1:condicionales.comportamiento',
                'task_form_index': 1,
                'value': 'bueno',
                'value_caption': 'Si',
            },
            {
                'action': 'create',
                'author': 'kysxd',
                'field_id': 'regalo',
                'form_id': 'regalos',
                'form_index': 0,
                'ref': 'solicitud.kysxd.2:regalos.regalo',
                'task_form_index': 2,
                'value': 'Max Iron',
                'value_caption': 'Max Iron',
            },
            {
                'action': 'create',
                'author': 'kysxd',
                'field_id': 'costo',
                'form_id': 'regalos',
                'form_index': 0,
                'ref': 'solicitud.kysxd.2:regalos.costo',
                'task_form_index': 2,
                'value': 350.0,
                'value_caption': '350.0',
            },
            {
                'action': 'create',
                'author': 'kysxd',
                'field_id': 'regalo',
                'form_id': 'regalos',
                'form_index': 1,
                'ref': 'solicitud.kysxd.3:regalos.regalo',
                'task_form_index': 3,
                'value': 'Mega boy',
                'value_caption': 'Mega boy',
            },
            {
                'action': 'create',
                'author': 'kysxd',
                'field_id': 'costo',
                'form_id': 'regalos',
                'form_index': 1,
                'ref': 'solicitud.kysxd.3:regalos.costo',
                'task_form_index': 3,
                'value': 120.0,
                'value_caption': '120.0',
            },
            {
                'action': 'create',
                'author': 'kysxd',
                'field_id': 'regalo',
                'form_id': 'regalos',
                'form_index': 2,
                'ref': 'solicitud.kysxd.4:regalos.regalo',
                'task_form_index': 4,
                'value': 'Brobocop',
                'value_caption': 'Brobocop',
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'if_malo',
                'form_index': 0,
                'ref': 'if_malo.__system__.0:if_malo.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'update',
                'author': 'alex',
                'field_id': 'regalo',
                'form_id': 'regalos',
                'form_index': None,
                'ref': 'solicitud.kysxd.3:regalos.regalo',
                'task_form_index': 3,
                'value': 'Mega bro',
                'value_caption': 'Mega bro',
            },
            {
                'action': 'update',
                'author': 'alex',
                'field_id': 'regalo',
                'form_id': 'regalos',
                'form_index': None,
                'ref': 'solicitud.kysxd.2:regalos.regalo',
                'task_form_index': 2,
                'value': 'Action bro',
                'value_caption': 'Action bro',
            },
            {
                'action': 'update',
                'author': 'alex',
                'field_id': 'costo',
                'form_id': 'regalos',
                'form_index': None,
                'ref': 'solicitud.kysxd.5:regalos.costo',
                'task_form_index': 5,
                'value': 350.0,
                'value_caption': '350.0',
            },
            {
                'action': 'update',
                'author': 'alex',
                'field_id': 'costo',
                'form_id': 'regalos',
                'form_index': None,
                'ref': 'solicitud.kysxd.2:regalos.costo',
                'task_form_index': 2,
                'value': 10.0,
                'value_caption': '10.0',
            },
        ],
        [],
    ]
    assert compact_values([
        item
        for index, item in enumerate(chain.from_iterable(values))
        if index != 10
    ]) == {
        'condicionales': [
            {
                'comportamiento': {
                    'authors': {
                        'kysxd',
                    },
                    'refs': {
                        'solicitud.kysxd.1:condicionales.comportamiento',
                    },
                    'value': 'bueno',
                    'value_caption': 'Si',
                },
            },
        ],
        'if_malo': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'if_malo.__system__.0:if_malo.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
        'regalos': [
            {
                'costo': {
                    'authors': {
                        'alex',
                        'kysxd',
                    },
                    'refs': {
                        'solicitud.kysxd.2:regalos.costo',
                    },
                    'value': 10.0,
                    'value_caption': '10.0',
                },
                'regalo': {
                    'authors': {
                        'alex',
                        'kysxd',
                    },
                    'refs': {
                        'solicitud.kysxd.2:regalos.regalo',
                    },
                    'value': 'Action bro',
                    'value_caption': 'Action bro',
                },
            },
            {
                'costo': {
                    'authors': {
                        'kysxd',
                    },
                    'refs': {
                        'solicitud.kysxd.3:regalos.costo',
                    },
                    'value': 120.0,
                    'value_caption': '120.0',
                },
                'regalo': {
                    'authors': {
                        'alex',
                        'kysxd',
                    },
                    'refs': {
                        'solicitud.kysxd.3:regalos.regalo',
                    },
                    'value': 'Mega bro',
                    'value_caption': 'Mega bro',
                },
            },
            {
                'regalo': {
                    'authors': {
                        'kysxd',
                    },
                    'refs': {
                        'solicitud.kysxd.4:regalos.regalo',
                    },
                    'value': 'Brobocop',
                    'value_caption': 'Brobocop',
                },
            },
        ],
        'viaticos': [
            {
                'galletas': {
                    'authors': {
                        'kysxd',
                    },
                    'refs': {
                        'solicitud.kysxd.0:viaticos.galletas',
                    },
                    'value': 'yes',
                    'value_caption': 'Si',
                },
            },
        ],
    }
