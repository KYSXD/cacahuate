import random
from itertools import chain
from random import randint
from unittest.mock import MagicMock
from xml.dom.minidom import parseString

from cacahuate.database import db_session
from cacahuate.handler.legacy import (
    Handler,
    compact_values,
    unpack_pointer_values,
)
from cacahuate.models import Pointer
from cacahuate.mongo import pointer_entry
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import (
    Capture,
    CaptureValue,
    Form,
)

import pytest

import requests

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
    random_string,
)


def test_handle_request_node(config, mocker, mongo):
    mocker.patch('cacahuate.tasks.handle.delay')

    process_name = 'request.2018-05-18.xml'
    xml = Xml.load(config, process_name, direct=True)

    class ResponseMock:
        status_code = 200
        text = 'request response'

    mock = MagicMock(return_value=ResponseMock())

    mocker.patch(
        'requests.request',
        new=mock,
    )

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('request.2018-05-18.xml', 'start_node')

    execution = ptr.execution
    main_exe = ptr.execution

    value = random_string()

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, 'request').get_state(),
    })

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    # teardown of first node and wakeup of request node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('request', [
            {
                'name': 'data',
                'value': value,
                'value_caption': value,
            },
        ])],
    })
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = next(
        p_ for p_ in execution.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert ptr.node_id == 'request_node'

    # assert requests is called
    requests.request.assert_called_once()
    args, kwargs = requests.request.call_args

    assert args[0] == 'GET'
    assert args[1] == 'http://localhost/mirror?data=' + value

    assert kwargs['data'] == '{"data":"' + value + '"}'
    assert kwargs['headers'] == {
        'content-type': 'application/json',
        'x-url-data': value,
    }

    # aditional rabbit call for new process
    args = handle.delay.call_args[0][0]

    expected_inputs = [Form.state_json('request_node', [
        {
            'name': 'status_code',
            'state': 'valid',
            'type': 'int',
            'value': 200,
            'value_caption': '200',
            'hidden': False,
            'label': 'Status Code',
        },
        {
            'name': 'raw_response',
            'state': 'valid',
            'type': 'text',
            'value': 'request response',
            'value_caption': 'request response',
            'hidden': False,
            'label': 'Response',
        },
    ])]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': expected_inputs,
    }

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': expected_inputs,
    })

    state = mongo[config["EXECUTION_COLLECTION"]].find_one({
        'id': execution.id,
    })

    assert state['state']['items']['request_node'] == {
        **xml.get_node('request_node').get_state(),
        'actors': {
            '_type': ':map',
            'items': {
                '__system__': {
                    '_type': 'actor',
                    'state': 'valid',
                    'user': {
                        '_type': 'user',
                        'fullname': 'System',
                        'identifier': '__system__',
                        'email': '',
                    },
                    'forms': expected_inputs,
                },
            },
        },
        'state': 'valid',
    }

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'data',
                'form_id': 'request',
                'form_index': 0,
                'ref': 'start_node.juan.0:request.data',
                'task_form_index': 0,
                'value': value,
                'value_caption': value,
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'status_code',
                'form_id': 'request_node',
                'form_index': 0,
                'ref': 'request_node.__system__.0:request_node.status_code',
                'task_form_index': 0,
                'value': 200,
                'value_caption': '200',
            },
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'raw_response',
                'form_id': 'request_node',
                'form_index': 0,
                'ref': 'request_node.__system__.0:request_node.raw_response',
                'task_form_index': 0,
                'value': 'request response',
                'value_caption': 'request response',
            },
        ],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'request': [
            {
                'data': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:request.data',
                    },
                    'value': value,
                    'value_caption': value,
                },
            },
        ],
        'request_node': [
            {
                'raw_response': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'request_node.__system__.0:request_node.raw_response',
                    },
                    'value': 'request response',
                    'value_caption': 'request response',
                },
                'status_code': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'request_node.__system__.0:request_node.status_code',
                    },
                    'value': 200,
                    'value_caption': '200',
                },
            },
        ],
    }


@pytest.mark.skip
def test_store_failed_decoding(config, mocker, mongo):
    # TODO: set it up like test_store_data_from_response but make the json
    # decoding fail and test that the machine stays in a reasonably safe state
    raise AssertionError()


@pytest.mark.skip
def test_store_failed_path(config, mocker, mongo):
    # TODO: set it up like test_store_data_from_response but make the path not
    # match anything and test that the machine stays in a reasonably safe state
    raise AssertionError()


def test_capture():
    name = random_string()
    name_label = random_string()
    name_field = random_string()
    name_type = 'text'

    score = str(random.uniform(0.1, 0.9))
    score_label = random_string()
    score_field = random_string()
    score_type = 'float'

    age = str(randint(0, 100))
    age_label = random_string()
    age_field = random_string()
    age_type = 'int'

    dom = parseString((
        '<capture id="capture1">'
        '<value path="name" name="{name_field}"'
        ' label="{name_label}" type="{name_type}"></value>'
        '<value path="score" name="{score_field}"'
        ' label="{score_label}" type="{score_type}"></value>'
        '<value path="age" name="{age_field}"'
        ' label="{age_label}" type="{age_type}"></value>'
        '</capture>'
    ).format(
        name_field=name_field,
        name_label=name_label,
        name_type=name_type,
        score_field=score_field,
        score_label=score_label,
        score_type=score_type,
        age_field=age_field,
        age_label=age_label,
        age_type=age_type,
    )).documentElement

    capture = Capture(
        id_=dom.getAttribute('id'),
        multiple=bool(dom.getAttribute('multiple')),
        path=dom.getAttribute('path'),
        values=[
            CaptureValue(
                path=value.getAttribute('path'),
                name=value.getAttribute('name'),
                label=value.getAttribute('label'),
                type_=value.getAttribute('type'),
            )
            for value in dom.getElementsByTagName('value')
        ],
    )

    assert capture.capture({
        'name': name,
        'score': score,
        'age': age,
    }) == [{
        'id': 'capture1',
        'items': [{
            'label': name_label,
            'name': name_field,
            'type': name_type,
            'value': name,
            'value_caption': str(name),
        }, {
            'label': score_label,
            'name': score_field,
            'type': score_type,
            'value': float(score),
            'value_caption': str(score),
        }, {
            'label': age_label,
            'name': age_field,
            'type': age_type,
            'value': int(age),
            'value_caption': str(age),
        }],
    }]


def test_capture_parent_path():
    name = random_string()
    label = random_string()
    field_name = random_string()

    dom = parseString('''<capture id="capture1" path="props">
      <value path="name" name="{}" label="{}" type="text"></value>
    </capture>'''.format(field_name, label)).documentElement
    capture = Capture(
        id_=dom.getAttribute('id'),
        multiple=bool(dom.getAttribute('multiple')),
        path=dom.getAttribute('path'),
        values=[
            CaptureValue(
                path=value.getAttribute('path'),
                name=value.getAttribute('name'),
                label=value.getAttribute('label'),
                type_=value.getAttribute('type'),
            )
            for value in dom.getElementsByTagName('value')
        ],
    )

    assert capture.capture({
        'props': {
            'name': name,
        },
    }) == [{
        'id': 'capture1',
        'items': [{
            'label': label,
            'name': field_name,
            'type': 'text',
            'value': name,
            'value_caption': name,
        }],
    }]


def test_capture_multiple():
    name1 = random_string()
    name2 = random_string()
    label = random_string()
    field_name = random_string()

    dom = parseString((
        '<capture id="capture1" path="items" multiple="multiple">'
        '<value path="name" name="{}" label="{}" type="text"></value>'
        '</capture>'
    ).format(field_name, label)).documentElement
    capture = Capture(
        id_=dom.getAttribute('id'),
        multiple=bool(dom.getAttribute('multiple')),
        path=dom.getAttribute('path'),
        values=[
            CaptureValue(
                path=value.getAttribute('path'),
                name=value.getAttribute('name'),
                label=value.getAttribute('label'),
                type_=value.getAttribute('type'),
            )
            for value in dom.getElementsByTagName('value')
        ],
    )

    assert capture.capture({
        'items': [
            {
                'name': name1,
            },
            {
                'name': name2,
            },
        ],
    }) == [{
        'id': 'capture1',
        'items': [{
            'label': label,
            'name': field_name,
            'type': 'text',
            'value': name1,
            'value_caption': name1,
        }],
    }, {
        'id': 'capture1',
        'items': [{
            'label': label,
            'name': field_name,
            'type': 'text',
            'value': name2,
            'value_caption': name2,
        }],
    }]


def test_capture_multiple_union():
    name1 = random_string()
    name2 = random_string()
    name3 = random_string()
    name4 = random_string()
    label = random_string()
    field_name = random_string()

    dom = parseString((
        '<capture id="capture1" path="(items2|items)" multiple="multiple">'
        '<value path="name" name="{}" label="{}" type="text"></value>'
        '</capture>'
    ).format(field_name, label)).documentElement
    capture = Capture(
        id_=dom.getAttribute('id'),
        multiple=bool(dom.getAttribute('multiple')),
        path=dom.getAttribute('path'),
        values=[
            CaptureValue(
                path=value.getAttribute('path'),
                name=value.getAttribute('name'),
                label=value.getAttribute('label'),
                type_=value.getAttribute('type'),
            )
            for value in dom.getElementsByTagName('value')
        ],
    )

    assert capture.capture({
        'items': [
            {
                'name': name1,
            },
            {
                'name': name2,
            },
        ],
        'items2': [
            {
                'name': name3,
            },
            {
                'name': name4,
            },
        ],
    }) == [{
        'id': 'capture1',
        'items': [{
            'label': label,
            'name': field_name,
            'type': 'text',
            'value': name3,
            'value_caption': name3,
        }],
    }, {
        'id': 'capture1',
        'items': [{
            'label': label,
            'name': field_name,
            'type': 'text',
            'value': name4,
            'value_caption': name4,
        }],
    }, {
        'id': 'capture1',
        'items': [{
            'label': label,
            'name': field_name,
            'type': 'text',
            'value': name1,
            'value_caption': name1,
        }],
    }, {
        'id': 'capture1',
        'items': [{
            'label': label,
            'name': field_name,
            'type': 'text',
            'value': name2,
            'value_caption': name2,
        }],
    }]


def test_store_data_from_response(config, mocker, mongo):
    mocker.patch('cacahuate.tasks.handle.delay')

    process_name = 'request-captures.2019-08-08.xml'
    xml = Xml.load(config, process_name, direct=True)

    expected_name = random_string()
    expected_age_1 = randint(0, 100)
    expected_age_2 = randint(0, 100)

    request_response = {
        'params': {
            'name': expected_name,
        },
        'items': [
            [
                {
                    'age': expected_age_1,
                },
                {
                    'age': expected_age_2,
                },
            ],
        ],
    }
    request_response_s = json.dumps(request_response)

    class ResponseMock:
        status_code = 200
        text = request_response_s

        def json(self):
            return request_response

    mock = MagicMock(return_value=ResponseMock())

    mocker.patch(
        'requests.request',
        new=mock,
    )

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('request-captures.2019-08-08.xml', 'start_node')

    execution = ptr.execution
    main_exe = ptr.execution

    value = random_string()

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, 'request-captures').get_state(),
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    # teardown of first node and wakeup of request node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('request', [
            {
                'name': 'data',
                'value': value,
                'value_caption': value,
            },
        ])],
    })
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = next(
        p_ for p_ in execution.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert ptr.node_id == 'request_node'

    # assert requests is called
    requests.request.assert_called_once()
    args, kwargs = requests.request.call_args

    assert args[0] == 'GET'
    assert args[1] == 'http://localhost/'

    assert kwargs['data'] == ''
    assert kwargs['headers'] == {
        'content-type': 'application/json',
    }

    # aditional rabbit call for new process
    args = handle.delay.call_args[0][0]

    expected_inputs = [
        Form.state_json('request_node', [
            {
                'name': 'status_code',
                'state': 'valid',
                'type': 'int',
                'value': 200,
                'value_caption': '200',
                'hidden': False,
                'label': 'Status Code',
            },
            {
                'name': 'raw_response',
                'state': 'valid',
                'type': 'text',
                'value': request_response_s,
                'value_caption': request_response_s,
                'hidden': False,
                'label': 'Response',
            },
        ]),
        Form.state_json('capture1', [
            {
                'name': 'name',
                'state': 'valid',
                'type': 'text',
                'value': expected_name,
                'value_caption': expected_name,
                'hidden': False,
                'label': 'Nombre',
            },
        ]),
        Form.state_json('capture2', [
            {
                'name': 'age',
                'state': 'valid',
                'type': 'int',
                'value': expected_age_1,
                'value_caption': str(expected_age_1),
                'hidden': False,
                'label': 'Edad',
            },
        ]),
        Form.state_json('capture2', [
            {
                'name': 'age',
                'state': 'valid',
                'type': 'int',
                'value': expected_age_2,
                'value_caption': str(expected_age_2),
                'hidden': False,
                'label': 'Edad',
            },
        ]),
    ]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': expected_inputs,
    }

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': expected_inputs,
    })

    state = mongo[config["EXECUTION_COLLECTION"]].find_one({
        'id': execution.id,
    })

    assert state['state']['items']['request_node'] == {
        **xml.get_node('request_node').get_state(),
        'actors': {
            '_type': ':map',
            'items': {
                '__system__': {
                    '_type': 'actor',
                    'state': 'valid',
                    'user': {
                        '_type': 'user',
                        'fullname': 'System',
                        'identifier': '__system__',
                        'email': '',
                    },
                    'forms': expected_inputs,
                },
            },
        },
        'state': 'valid',
    }
    assert state['values'] == {
        '_execution': [{
            'name': '',
            'description': '',
        }],
        'capture1': [{
            'name': expected_name,
        }],
        'capture2': [
            {
                'age': expected_age_1,
            },
            {
                'age': expected_age_2,
            },
        ],
        'request': [{
            'data': value,
        }],
        'request_node': [{
            'raw_response': request_response_s,
            'status_code': 200,
        }],
    }

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'data',
                'form_id': 'request',
                'form_index': 0,
                'ref': 'start_node.juan.0:request.data',
                'task_form_index': 0,
                'value': value,
                'value_caption': value,
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'status_code',
                'form_id': 'request_node',
                'form_index': 0,
                'ref': 'request_node.__system__.0:request_node.status_code',
                'task_form_index': 0,
                'value': 200,
                'value_caption': '200',
            },
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'raw_response',
                'form_id': 'request_node',
                'form_index': 0,
                'ref': 'request_node.__system__.0:request_node.raw_response',
                'task_form_index': 0,
                'value': request_response_s,
                'value_caption': request_response_s,
            },
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'name',
                'form_id': 'capture1',
                'form_index': 0,
                'ref': 'request_node.__system__.1:capture1.name',
                'task_form_index': 1,
                'value': expected_name,
                'value_caption': expected_name,
            },
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'age',
                'form_id': 'capture2',
                'form_index': 0,
                'ref': 'request_node.__system__.2:capture2.age',
                'task_form_index': 2,
                'value': expected_age_1,
                'value_caption': str(expected_age_1),
            },
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'age',
                'form_id': 'capture2',
                'form_index': 1,
                'ref': 'request_node.__system__.3:capture2.age',
                'task_form_index': 3,
                'value': expected_age_2,
                'value_caption': str(expected_age_2),
            },
        ],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'capture1': [
            {
                'name': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'request_node.__system__.1:capture1.name',
                    },
                    'value': expected_name,
                    'value_caption': expected_name,
                },
            },
        ],
        'capture2': [
            {
                'age': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'request_node.__system__.2:capture2.age',
                    },
                    'value': expected_age_1,
                    'value_caption': str(expected_age_1),
                },
            },
            {
                'age': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'request_node.__system__.3:capture2.age',
                    },
                    'value': expected_age_2,
                    'value_caption': str(expected_age_2),
                },
            },
        ],
        'request': [
            {
                'data': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:request.data',
                    },
                    'value': value,
                    'value_caption': value,
                },
            },
        ],
        'request_node': [
            {
                'raw_response': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'request_node.__system__.0:request_node.raw_response',
                    },
                    'value': request_response_s,
                    'value_caption': request_response_s,
                },
                'status_code': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'request_node.__system__.0:request_node.status_code',
                    },
                    'value': 200,
                    'value_caption': '200',
                },
            },
        ],
    }
