from itertools import chain

from cacahuate.database import db_session
from cacahuate.handler.legacy import (
    Handler,
    compact_values,
    unpack_pointer_values,
)
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.mongo import (
    execution_entry,
    pointer_entry,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
    random_string,
)


def test_connection_node_execution_all(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    main_ptr_01 = make_pointer(
        'proc-connector.2023-03-01.xml',
        'first_node',
    )
    main_exe = main_ptr_01.execution

    value_01 = random_string()
    value_02 = random_string()
    value_03 = random_string()

    mongo[config["POINTER_COLLECTION"]].insert_one(pointer_entry(main_ptr_01))
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    # node teardown
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_01.id,
        'user_identifier': make_user('juan').identifier,
        'input': [Form.state_json('first_form', [
            {
                'name': 'data',
                'label': 'Info',
                'value': value_01,
                'value_caption': value_01,
            },
        ])],
    })

    # check db status
    db_session.refresh(main_ptr_01)
    assert main_ptr_01.status == Pointer.Status.FINISHED
    main_ptr_02 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_02.node_id == 'exe_create'

    # test 01: execution create
    alt_exe = db_session.query(Execution).filter(
        Execution.status == Execution.Status.ONGOING,
        Execution.process_name == 'simple.2018-02-19.xml',
    ).first()

    alt_ptr_01 = next(
        p_ for p_ in alt_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )

    assert json.loads(handle.delay.call_args_list[0][0][0]) == {
        'command': 'step',
        'pointer_id': alt_ptr_01.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('start_form', [
            {
                'label': 'Info',
                'name': 'data',
                'state': 'valid',
                'type': 'text',
                'value': value_01,
                'value_caption': value_01,
                'hidden': False,
            },
        ])],
    }
    handler.step(json.loads(handle.delay.call_args_list[0][0][0]))

    assert next(mongo[config["POINTER_COLLECTION"]].find({
        'id': alt_ptr_01.id,
    }))['node']['id'] == 'start_node'

    assert next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': alt_exe.id,
    }))['name'] == 'Simplest process ever started with: ' + value_01

    assert db_session.query(Execution).filter(
        Execution.status == Execution.Status.ONGOING,
        Execution.process_name == 'simple.2018-02-19.xml',
        Execution.id == alt_exe.id,
    ).count() == 1

    # node teardown
    assert json.loads(handle.delay.call_args_list[1][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_02.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('creation', [
            {
                'name': 'id',
                'label': 'Id',
                'state': 'valid',
                'type': 'text',
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ])],
    }
    handler.step(json.loads(handle.delay.call_args_list[1][0][0]))

    # check db status
    db_session.refresh(main_ptr_02)
    assert main_ptr_02.status == Pointer.Status.FINISHED
    main_ptr_03 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_03.node_id == 'second_node'

    # node teardown
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_03.id,
        'user_identifier': make_user('jose').identifier,
        'input': [Form.state_json('second_form', [
            {
                'name': 'data',
                'label': 'Info',
                'value': value_02,
                'value_caption': value_02,
            },
        ])],
    })

    # check db status
    db_session.refresh(main_ptr_03)
    assert main_ptr_03.status == Pointer.Status.FINISHED
    main_ptr_04 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_04.node_id == 'exe_search'

    # node teardown
    # test 02: execution search
    assert json.loads(handle.delay.call_args_list[2][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_04.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('search', [
            {
                'name': 'id',
                'label': 'Id',
                'state': 'valid',
                'type': 'text',
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ])],
    }
    handler.step(json.loads(handle.delay.call_args_list[2][0][0]))

    # check db status
    db_session.refresh(main_ptr_04)
    assert main_ptr_04.status == Pointer.Status.FINISHED
    main_ptr_05 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_05.node_id == 'third_node'

    # node teardown
    handler.step({
        'command': 'step',
        'pointer_id': main_ptr_05.id,
        'user_identifier': make_user('luis').identifier,
        'input': [Form.state_json('third_form', [
            {
                'name': 'data',
                'label': 'Info',
                'value': value_03,
                'value_caption': value_03,
            },
        ])],
    })

    # check db status
    db_session.refresh(main_ptr_05)
    assert main_ptr_05.status == Pointer.Status.FINISHED
    main_ptr_06 = next(
        p_ for p_ in main_exe.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert main_ptr_06.node_id == 'exe_update'

    # test 03: execution update
    assert json.loads(handle.delay.call_args_list[3][0][0]) == {
        'command': 'patch',
        'execution_id': alt_exe.id,
        'comment': 'System update',
        'user_identifier': '__system__',
        'inputs': [
            {
                'ref': 'start_node.__system__.0:start_form.data',
                'value': value_03,
                'value_caption': value_03,
            },
        ],
    }
    handler.patch(json.loads(handle.delay.call_args_list[3][0][0]))

    # node teardown
    assert json.loads(handle.delay.call_args_list[4][0][0]) == {
        'command': 'step',
        'pointer_id': main_ptr_06.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('update', [
            {
                'name': 'id',
                'label': 'Id',
                'state': 'valid',
                'type': 'text',
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ])],
    }
    handler.step(json.loads(handle.delay.call_args_list[4][0][0]))

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'data',
                'form_id': 'first_form',
                'form_index': 0,
                'ref': 'first_node.juan.0:first_form.data',
                'task_form_index': 0,
                'value': value_01,
                'value_caption': value_01,
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'id',
                'form_id': 'creation',
                'form_index': 0,
                'ref': 'exe_create.__system__.0:creation.id',
                'task_form_index': 0,
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ],
        [
            {
                'action': 'create',
                'author': 'jose',
                'field_id': 'data',
                'form_id': 'second_form',
                'form_index': 0,
                'ref': 'second_node.jose.0:second_form.data',
                'task_form_index': 0,
                'value': value_02,
                'value_caption': value_02,
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'id',
                'form_id': 'search',
                'form_index': 0,
                'ref': 'exe_search.__system__.0:search.id',
                'task_form_index': 0,
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ],
        [
            {
                'action': 'create',
                'author': 'luis',
                'field_id': 'data',
                'form_id': 'third_form',
                'form_index': 0,
                'ref': 'third_node.luis.0:third_form.data',
                'task_form_index': 0,
                'value': value_03,
                'value_caption': value_03,
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'id',
                'form_id': 'update',
                'form_index': 0,
                'ref': 'exe_update.__system__.0:update.id',
                'task_form_index': 0,
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'creation': [
            {
                'id': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'exe_create.__system__.0:creation.id',
                    },
                    'value': alt_exe.id,
                    'value_caption': alt_exe.id,
                },
            },
        ],
        'first_form': [
            {
                'data': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'first_node.juan.0:first_form.data',
                    },
                    'value': value_01,
                    'value_caption': value_01,
                },
            },
        ],
        'search': [
            {
                'id': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'exe_search.__system__.0:search.id',
                    },
                    'value': alt_exe.id,
                    'value_caption': alt_exe.id,
                },
            },
        ],
        'second_form': [
            {
                'data': {
                    'authors': {
                        'jose',
                    },
                    'refs': {
                        'second_node.jose.0:second_form.data',
                    },
                    'value': value_02,
                    'value_caption': value_02,
                },
            },
        ],
        'third_form': [
            {
                'data': {
                    'authors': {
                        'luis',
                    },
                    'refs': {
                        'third_node.luis.0:third_form.data',
                    },
                    'value': value_03,
                    'value_caption': value_03,
                },
            },
        ],
        'update': [
            {
                'id': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'exe_update.__system__.0:update.id',
                    },
                    'value': alt_exe.id,
                    'value_caption': alt_exe.id,
                },
            },
        ],
    }
