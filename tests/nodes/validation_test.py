from datetime import datetime
from itertools import chain

from cacahuate.database import db_session
from cacahuate.handler.legacy import (
    Handler,
    compact_values,
    unpack_pointer_values,
)
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.mongo import (
    pointer_entry,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

import simplejson as json

from tests.utils import (
    assert_near_date,
    make_pointer,
    make_user,
)


def test_approve(config, mongo):
    ''' tests that a validation node can go forward on approval '''
    # test setup
    handler = Handler(config)
    user = make_user('juan', 'Juan')
    ptr = make_pointer(
        'validation.2018-05-09.xml',
        'approval_node',
        node_type=Pointer.NodeType.VALIDATION,
    )
    ptr.started_at = datetime(2018, 4, 1, 21, 45)
    main_exe = ptr.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': ptr.execution.id,
        'state': Xml.load(config, 'validation.2018-05-09').get_state(),
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    # thing to test
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('approval_node', [
            {
                'name': 'response',
                'value': 'accept',
                'value_caption': 'accept',
            },
            {
                'name': 'comment',
                'value': 'I like it',
                'value_caption': 'I like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0.task',
                }],
                'value_caption': '',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None

    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first().node_id == 'final_node'

    reg = next(mongo[config["POINTER_COLLECTION"]].find())

    assert reg['started_at'] == datetime(2018, 4, 1, 21, 45)
    assert_near_date(reg['finished_at'])
    assert reg['node']['id'] == 'approval_node'

    assert reg['actors'] == {
        '_type': ':map',
        'items': {
            'juan': {
                '_type': 'actor',
                'state': 'valid',
                'user': {
                    '_type': 'user',
                    'identifier': 'juan',
                    'fullname': 'Juan',
                    'email': 'juan@mailinator.com',
                },
                'forms': [Form.state_json('approval_node', [
                    {
                        'name': 'response',
                        'value': 'accept',
                        'value_caption': 'accept',
                    },
                    {
                        'name': 'comment',
                        'value': 'I like it',
                        'value_caption': 'I like it',
                    },
                    {
                        'name': 'inputs',
                        'value': [{
                            'ref': 'start_node.juan.0.task',
                        }],
                        'value_caption': '',
                    },
                ])],
            },
        },
    }

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
    }


def test_reject(config, mongo):
    ''' tests that a rejection moves the pointer to a backward position '''
    # test setup
    process_name = 'validation.2018-05-09.xml'
    xml = Xml.load(config, process_name, direct=True)

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer(
        'validation.2018-05-09.xml',
        'approval_node',
        node_type=Pointer.NodeType.VALIDATION,
    )
    ptr.started_at = datetime(2018, 4, 1, 21, 45)

    execution = ptr.execution
    main_exe = ptr.execution

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    state = Xml.load(config, 'validation.2018-05-09').get_state()

    state['items']['start_node']['state'] = 'valid'
    state['items']['start_node']['actors']['items']['juan'] = {
        '_type': 'actor',
        'state': 'valid',
        'user': {
            '_type': 'user',
            'identifier': 'juan',
            'fullname': 'Juan',
            'email': 'juan@mailinator.com',
        },
        'forms': [Form.state_json('work', [
            {
                'name': 'task',
                '_type': 'field',
                'state': 'valid',
                'value': '2',
            },
        ])],
    }

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': state,
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    # will teardown the approval node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('approval_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'I do not like it',
                'value_caption': 'I do not like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:work.task',
                }],
                'value_caption': '',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None

    new_ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert new_ptr.node_id == 'start_node'

    assert new_ptr in user.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    )

    # data is invalidated
    state = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': execution.id,
    }))

    del state['_id']

    assert state == {
        '_type': 'execution',
        'id': execution.id,
        'name': '',
        'description': '',
        'state': {
            '_type': ':sorted_map',
            'items': {
                'start_node': {
                    **xml.get_node('start_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('work', [
                                    {
                                        'name': 'task',
                                        '_type': 'field',
                                        'state': 'invalid',
                                        'value': '2',
                                    },
                                ], state='invalid')],
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': 'juan@mailinator.com',
                                },
                            },
                        },
                    },
                    'state': 'ongoing',
                    'comment': 'I do not like it',
                },

                'approval_node': {
                    **xml.get_node('approval_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('approval_node', [
                                    {
                                        'name': 'response',
                                        'state': 'invalid',
                                        'value': 'reject',
                                        'value_caption': 'reject',
                                    },
                                    {
                                        'name': 'comment',
                                        'value': 'I do not like it',
                                        'value_caption': 'I do not like it',
                                    },
                                    {
                                        'name': 'inputs',
                                        'value': [{
                                            'ref': 'start_node.'
                                                   'juan.0:work.task',
                                        }],
                                        'value_caption': '',
                                    },
                                ], state='invalid')],
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': 'juan@mailinator.com',
                                },
                            },
                        },
                    },
                    'state': 'invalid',
                    'comment': 'I do not like it',
                },

                'final_node': {
                    **xml.get_node('final_node').get_state(),
                },
            },
            'item_order': ['start_node', 'approval_node', 'final_node'],
        },
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
            'approval_node': [{
                'comment': 'I do not like it',
                'response': 'reject',
                'inputs': [{'ref': 'start_node.juan.0:work.task'}],
            }],
        },
    }

    # mongo has the data
    reg = next(mongo[config["POINTER_COLLECTION"]].find())

    assert reg['started_at'] == datetime(2018, 4, 1, 21, 45)
    assert (reg['finished_at'] - datetime.now()).total_seconds() < 2
    assert reg['node']['id'] == 'approval_node'

    assert reg['actors'] == {
        '_type': ':map',
        'items': {
            'juan': {
                '_type': 'actor',
                'forms': [Form.state_json('approval_node', [
                    {
                        'name': 'response',
                        'value': 'reject',
                        'value_caption': 'reject',
                    },
                    {
                        'name': 'comment',
                        'value': 'I do not like it',
                        'value_caption': 'I do not like it',
                    },
                    {
                        'name': 'inputs',
                        'value': [{
                            'ref': 'start_node.juan.0:work.task',
                        }],
                        'value_caption': '',
                    },
                ])],
                'state': 'valid',
                'user': {
                    '_type': 'user',
                    'identifier': 'juan',
                    'fullname': 'Juan',
                    'email': 'juan@mailinator.com',
                },
            },
        },
    }

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'delete',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'work',
                'form_index': None,
                'ref': 'start_node.juan.0:work.task',
                'task_form_index': 0,
                'value': None,
                'value_caption': '',
            },
        ],
        [],
    ]
    aux = [
        {
            'action': 'create',
            'author': 'juan',
            'field_id': 'task',
            'form_id': 'work',
            'form_index': 0,
            'ref': 'start_node.juan.0:work.task',
            'task_form_index': 0,
            'value': '2',
            'value_caption': '2',
        },
    ]
    # TODO: Fix test with pointer
    assert compact_values(aux + list(chain.from_iterable(values))) == {
        'work': [
            {
                'task': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:work.task',
                    },
                    'value': None,
                    'value_caption': '',
                },
            },
        ],
    }


def test_reject_with_dependencies(config, mongo):
    handler = Handler(config)

    process_name = 'validation-reloaded.2018-05-17.xml'
    xml = Xml.load(config, process_name, direct=True)

    user = make_user('juan', 'Juan')
    ptr = make_pointer(
        'validation-reloaded.2018-05-17.xml',
        'node1',
        node_type=Pointer.NodeType.ACTION,
    )
    execution = ptr.execution
    main_exe = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, 'validation-reloaded').get_state(),
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    # first call to node1
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'task',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node2'

    # first call to node2
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form2', [
            {
                'name': 'task',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node3'

    # first call to node3
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form3', [
            {
                'name': 'task',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node4'

    # first call to validation
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('node4', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'I do not like it',
                'value_caption': 'I do not like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'node1.juan.0:form1.task',
                }],
                'value_caption': '',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node1'

    # second call to node1
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'task',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node2'

    # second call to node2
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form2', [
            {
                'name': 'task',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node4'

    # second call to validation
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('node4', [
            {
                'name': 'response',
                'value': 'accept',
                'value_caption': 'accept',
            },
            {
                'name': 'comment',
                'value': 'I like it',
                'value_caption': 'I like it',
            },
            {
                'name': 'inputs',
                'value': None,
                'value_caption': 'None',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node5'

    # first call to last node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form5', [
            {
                'name': 'task',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })
    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).count() == 0

    # state is coherent
    state = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': execution.id,
    }))

    del state['_id']
    del state['finished_at']

    assert state == {
        '_type': 'execution',
        'id': execution.id,
        'name': '',
        'description': '',
        'state': {
            '_type': ':sorted_map',
            'items': {
                'node1': {
                    **xml.get_node('node1').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('form1', [
                                    {
                                        'name': 'task',
                                        'value': '2',
                                        'value_caption': '2',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': 'juan@mailinator.com',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                    'comment': 'I do not like it',
                },

                'node2': {
                    **xml.get_node('node2').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('form2', [
                                    {
                                        'name': 'task',
                                        'value': '2',
                                        'value_caption': '2',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': 'juan@mailinator.com',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                    'comment': 'I do not like it',
                },

                'node3': {
                    **xml.get_node('node3').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('form3', [
                                    {
                                        'name': 'task',
                                        'value': '1',
                                        'value_caption': '1',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': 'juan@mailinator.com',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                },

                'node4': {
                    **xml.get_node('node4').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('node4', [
                                    {
                                        'name': 'response',
                                        'value': 'accept',
                                        'value_caption': 'accept',
                                    },
                                    {
                                        'name': 'comment',
                                        'value': 'I like it',
                                        'value_caption': 'I like it',
                                    },
                                    {
                                        'name': 'inputs',
                                        'value': None,
                                        'value_caption': 'None',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': 'juan@mailinator.com',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                    'comment': 'I do not like it',
                },

                'node5': {
                    **xml.get_node('node5').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('form5', [
                                    {
                                        'name': 'task',
                                        'value': '1',
                                        'value_caption': '1',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': 'juan@mailinator.com',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                },
            },
            'item_order': ['node1', 'node2', 'node3', 'node4', 'node5'],
        },
        'status': 'finished',
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
            'node4': [{
                'comment': 'I like it',
                'inputs': None,
                'response': 'accept',
            }],
            'form1': [{'task': '2'}],
            'form2': [{'task': '2'}],
            'form3': [{'task': '1'}],
            'form5': [{'task': '1'}],
        },
    }

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'form1',
                'form_index': 0,
                'ref': 'node1.juan.0:form1.task',
                'task_form_index': 0,
                'value': '1',
                'value_caption': '1',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'form2',
                'form_index': 0,
                'ref': 'node2.juan.0:form2.task',
                'task_form_index': 0,
                'value': '1',
                'value_caption': '1',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'form3',
                'form_index': 0,
                'ref': 'node3.juan.0:form3.task',
                'task_form_index': 0,
                'value': '1',
                'value_caption': '1',
            },
        ],
        [
            {
                'action': 'delete',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'form1',
                'form_index': None,
                'ref': 'node1.juan.0:form1.task',
                'task_form_index': 0,
                'value': None,
                'value_caption': '',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'form1',
                'form_index': 0,
                'ref': 'node1.juan.0:form1.task',
                'task_form_index': 0,
                'value': '2',
                'value_caption': '2',
            },
        ],
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'form2',
                'form_index': 0,
                'ref': 'node2.juan.0:form2.task',
                'task_form_index': 0,
                'value': '2',
                'value_caption': '2',
            },
        ],
        [],
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'form5',
                'form_index': 0,
                'ref': 'node5.juan.0:form5.task',
                'task_form_index': 0,
                'value': '1',
                'value_caption': '1',
            },
        ],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'form1': [
            {
                'task': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'node1.juan.0:form1.task',
                    },
                    'value': '2',
                    'value_caption': '2',
                },
            },
        ],
        'form2': [
            {
                'task': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'node2.juan.0:form2.task',
                    },
                    'value': '2',
                    'value_caption': '2',
                },
            },
        ],
        'form3': [
            {
                'task': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'node3.juan.0:form3.task',
                    },
                    'value': '1',
                    'value_caption': '1',
                },
            },
        ],
        'form5': [
            {
                'task': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'node5.juan.0:form5.task',
                    },
                    'value': '1',
                    'value_caption': '1',
                },
            },
        ],
    }


def test_invalidate_all_nodes(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')
    mocker.patch('cacahuate.xml.node.Request.make_request')

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer(
        'all-nodes-invalidated.2018-05-24.xml',
        'start_node',
        node_type=Pointer.NodeType.ACTION,
    )

    execution = ptr.execution
    main_exe = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, 'all-nodes-invalidated').get_state(),
    })

    mongo[config["POINTER_COLLECTION"]].insert_one(
        pointer_entry(ptr),
    )

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('work', [
            {
                'name': 'task',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })
    ptr = next(
        p_ for p_ in execution.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert ptr.node_id == 'request_node'
    args = handle.delay.call_args[0][0]

    handler.step(json.loads(args))
    ptr = next(
        p_ for p_ in execution.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert ptr.node_id == 'call_node'
    args = handle.delay.call_args[0][0]

    handler.step(json.loads(args))
    ptr = next(
        p_ for p_ in execution.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert ptr.node_id == 'if_node'
    args = handle.delay.call_args[0][0]

    handler.step(json.loads(args))
    ptr = next(
        p_ for p_ in execution.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert ptr.node_id == 'validation_node'

    alt_exe = db_session.query(Execution).filter(
        Execution.status == Execution.Status.ONGOING,
        Execution.process_name == 'simple.2018-02-19.xml',
    ).first()

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('validation_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:work.task',
                }],
                'value_caption': '',
            },
            {
                'name': 'comment',
                'value': '',
                'value_caption': '',
            },
        ])],
    })
    ptr = next(
        p_ for p_ in execution.pointers
        if p_.status == Pointer.Status.ONGOING
    )
    assert ptr.node_id == 'start_node'

    # check values
    collection = mongo[config['POINTER_COLLECTION']]
    documents = [collection.find_one({'id': p.id}) for p in main_exe.pointers]
    values = [doc['values'] for doc in documents]
    assert values == [unpack_pointer_values(doc) for doc in documents]
    assert values == [
        [
            {
                'action': 'create',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'work',
                'form_index': 0,
                'ref': 'start_node.juan.0:work.task',
                'task_form_index': 0,
                'value': '2',
                'value_caption': '2',
            },
        ],
        [],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'execution',
                'form_id': 'call_node',
                'form_index': 0,
                'ref': 'call_node.__system__.0:call_node.execution',
                'task_form_index': 0,
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ],
        [
            {
                'action': 'create',
                'author': '__system__',
                'field_id': 'condition',
                'form_id': 'if_node',
                'form_index': 0,
                'ref': 'if_node.__system__.0:if_node.condition',
                'task_form_index': 0,
                'value': False,
                'value_caption': 'False',
            },
        ],
        [
            {
                'action': 'delete',
                'author': 'juan',
                'field_id': 'task',
                'form_id': 'work',
                'form_index': None,
                'ref': 'start_node.juan.0:work.task',
                'task_form_index': 0,
                'value': None,
                'value_caption': '',
            },
        ],
        [],
    ]
    assert compact_values(chain.from_iterable(values)) == {
        'call_node': [
            {
                'execution': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'call_node.__system__.0:call_node.execution',
                    },
                    'value': alt_exe.id,
                    'value_caption': alt_exe.id,
                },
            },
        ],
        'if_node': [
            {
                'condition': {
                    'authors': {
                        '__system__',
                    },
                    'refs': {
                        'if_node.__system__.0:if_node.condition',
                    },
                    'value': False,
                    'value_caption': 'False',
                },
            },
        ],
        'work': [
            {
                'task': {
                    'authors': {
                        'juan',
                    },
                    'refs': {
                        'start_node.juan.0:work.task',
                    },
                    'value': None,
                    'value_caption': '',
                },
            },
        ],
    }
