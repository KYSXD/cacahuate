import itertools

from cacahuate.database import db_session
from cacahuate.database.permissions import create_default_permissions
from cacahuate.database.query import (
    query_categories,
    query_userquirks,
)
from cacahuate.models import (
    Category,
    UserQuirk,
)

from case_conversion import snakecase

import tests.utils


def test_categories_requires_auth(client):
    for mthd in ['get', 'post']:
        r = getattr(client, mthd)(
            '/v1/categories',
        )

        assert r.request.url and r.request.method and r.status_code == 401
        # TODO: check error data

    for mthd in ['get', 'put', 'delete']:
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
        )

        assert r.request.url and r.request.method and r.status_code == 401
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post', 'put'],
        ['quirks'],
    )):
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
        )

        assert r.request.url and r.request.method and r.status_code == 401
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['delete'],
        ['quirks'],
    )):
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}/{attr}/{child_id}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
                child_id=tests.utils.random_string(),
            ),
        )

        assert r.request.url and r.request.method and r.status_code == 401
        # TODO: check error data


def test_categories_requires_json(client):
    for mthd in ['post']:
        r = getattr(client, mthd)(
            '/v1/categories',
            headers={
                **tests.utils.make_auth_header(),
            },
        )

        assert r.request.url and r.request.method and r.status_code == 400
        # TODO: check error data

    for mthd in ['put']:
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(),
            },
        )

        assert r.request.url and r.request.method and r.status_code == 400
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['post', 'put'],
        ['quirks'],
    )):
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(),
            },
        )

        assert r.request.url and r.request.method and r.status_code == 400
        # TODO: check error data


def test_categories_requires_permission(client):
    some_user = tests.utils.make_user()

    for mthd in ['get', 'post']:
        r = getattr(client, mthd)(
            '/v1/categories',
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert r.request.url and r.request.method and r.status_code == 403
        # TODO: check error data

    for mthd in ['get', 'put', 'delete']:
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert r.request.url and r.request.method and r.status_code == 403
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post'],
        ['quirks'],
    )):
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert r.request.url and r.request.method and r.status_code == 403
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['delete'],
        ['quirks'],
    )):
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}/{attr}/{child_id}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
                child_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert r.request.url and r.request.method and r.status_code == 403
        # TODO: check error data


def test_categories_not_found(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Category),
    )
    some_user.permissions.extend(
        create_default_permissions(UserQuirk),
    )

    db_session.commit()

    for mthd in ['get', 'delete']:
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert r.request.url and r.request.method and r.status_code == 404
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post'],
        ['quirks'],
    )):
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert r.request.url and r.request.method and r.status_code == 404
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['delete'],
        ['quirks'],
    )):
        r = getattr(client, mthd)(
            '/v1/categories/{item_id}/{attr}/{child_id}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
                child_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert r.request.url and r.request.method and r.status_code == 404
        # TODO: check error data


def test_categories_create(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Category, 'add'),
    )

    db_session.commit()

    current_tests = [
        {
            'name': tests.utils.random_string(),
            'codename': snakecase(tests.utils.random_string()),
            'quirks': [],
        },
        {
            'name': tests.utils.random_string(),
            'codename': snakecase(tests.utils.random_string()),
            'quirks': [
                {
                    'name': tests.utils.random_string(),
                    'codename': snakecase(tests.utils.random_string()),
                },
            ],
        },
    ]

    for index, test_data in enumerate(current_tests, start=1):
        r = client.post(
            '/v1/categories',
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json=test_data,
        )

        assert r.request.url and r.request.method and r.status_code == 201
        # TODO: check response data

        assert sorted([
            item.codename
            for item in query_categories()
        ]) == sorted([
            d['codename']
            for d in current_tests[:index]
        ])


def test_categories_read(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Category, 'view'),
    )

    some_category = Category(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
    )

    db_session.add(some_category)
    db_session.commit()

    read_res = client.get(
        '/v1/categories',
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert {
        **read_res.json,
        'items': sorted(read_res.json['items'], key=lambda d: d['codename']),
    } == {
        'items': sorted([{
            **some_category.as_json(),
        }], key=lambda d: d['codename']),
    }


def test_categories_update(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Category, 'change'),
    )

    db_session.commit()

    current_tests = [
        {
            'name': tests.utils.random_string(),
            'codename': snakecase(tests.utils.random_string()),
            'quirks': [
                {
                    'name': tests.utils.random_string(),
                    'codename': snakecase(tests.utils.random_string()),
                },
            ],
        },
        {
            'name': tests.utils.random_string(),
            'codename': snakecase(tests.utils.random_string()),
            'quirks': [],
        },
    ]

    item_id = snakecase(tests.utils.random_string())
    for index, test_data in enumerate(current_tests, start=1):
        r = client.put(
            '/v1/categories/{item_id}'.format(
                item_id=item_id,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json=test_data,
        )

        assert r.request.url and r.request.method and r.status_code == 202
        # TODO: check response data

        assert sorted([
            item.codename
            for item in query_categories()
        ]) == sorted([
            item_id,
        ])


def test_categories_delete(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Category, 'delete'),
    )

    some_category = Category(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
    )
    some_other_category = Category(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
    )

    db_session.add(some_category)
    db_session.add(some_other_category)
    db_session.commit()

    add_res = client.delete(
        '/v1/categories/{item_id}'.format(
            item_id=some_category.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in query_categories()
    ]) == sorted([
        some_other_category.codename,
    ])


def test_categories_quirks_create(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Category, 'change'),
    )

    some_category = Category(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
    )
    some_other_category = Category(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
    )

    db_session.add(some_category)
    db_session.add(some_other_category)
    db_session.commit()

    test_data = {
        'name': tests.utils.random_string(),
        'codename': snakecase(tests.utils.random_string()),
    }

    add_res = client.post(
        '/v1/categories/{item_id}/quirks'.format(
            item_id=some_category.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json={
            **test_data,
        },
    )

    assert add_res.status_code == 201
    # TODO: check response data

    assert sorted([
        item.codename
        for item in query_userquirks()
    ]) == sorted([
        test_data['codename'],
    ])

    add_res = client.post(
        '/v1/categories/{item_id}/quirks'.format(
            item_id=some_category.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json={
            **test_data,
        },
    )

    assert add_res.status_code == 400
    # TODO: check response data

    add_res = client.post(
        '/v1/categories/{item_id}/quirks'.format(
            item_id=some_other_category.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json={
            **test_data,
        },
    )

    assert add_res.status_code == 201
    # TODO: check response data

    assert sorted([
        item.codename
        for item in query_userquirks()
    ]) == sorted([
        test_data['codename'],
        test_data['codename'],
    ])


def test_categories_quirks_read(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Category, 'view'),
    )

    some_category = Category(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=snakecase(tests.utils.random_string()),
    )

    some_quirk = UserQuirk(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=snakecase(tests.utils.random_string()),
    )
    some_quirk.category = some_category

    db_session.add(some_category)
    db_session.add(some_quirk)
    db_session.commit()

    read_res = client.get(
        '/v1/categories/{item_id}/quirks'.format(
            item_id=some_category.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert read_res.json == {
        'items': [{
            **some_quirk.as_json(),
        }],
    }


def test_categories_quirks_update(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Category, 'change'),
    )

    some_category = Category(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
    )
    some_other_category = Category(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
    )

    db_session.add(some_category)
    db_session.add(some_other_category)
    db_session.commit()

    current_tests = [
        [
            {
                'name': tests.utils.random_string(),
                'codename': 'hardcoded',
            },
            {
                'name': tests.utils.random_string(),
                'codename': snakecase(tests.utils.random_string()),
            },
        ],
        [
            {
                'name': tests.utils.random_string(),
                'codename': 'hardcoded',
            },
        ],
        [
            {
                'name': tests.utils.random_string(),
                'codename': snakecase(tests.utils.random_string()),
            },
        ],
    ]

    for index, test_data in enumerate(current_tests):
        r = client.put(
            '/v1/categories/{item_id}/quirks'.format(
                item_id=some_category.codename,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json=test_data,
        )

        assert r.request.url and r.request.method and r.status_code == 202
        # TODO: check response data

        assert sorted([
            item.codename
            for item in query_userquirks()
        ]) == sorted([
            item['codename']
            for item in current_tests[index]
        ])


def test_users_quirks_delete(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(UserQuirk, 'delete'),
    )

    some_category = Category(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
    )
    some_quirk = UserQuirk(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
        category=some_category,
    )
    some_other_quirk = UserQuirk(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
        category=some_category,
    )

    db_session.add(some_quirk)
    db_session.add(some_other_quirk)
    db_session.commit()

    add_res = client.delete(
        '/v1/categories/{item_id}/quirks/{child_id}'.format(
            item_id=some_category.codename,
            child_id=some_quirk.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_category.quirks
    ]) == sorted([
        some_other_quirk.codename,
    ])
