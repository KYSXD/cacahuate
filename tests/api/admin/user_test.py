import itertools
import random

from cacahuate.database import db_session
from cacahuate.database.permissions import create_default_permissions
from cacahuate.models import (
    Category,
    Group,
    Permission,
    User,
    UserQuirk,
)

from case_conversion import snakecase

import tests.utils


def test_users_requires_auth(client):
    for mthd in ['get', 'post']:
        res = getattr(client, mthd)(
            '/v1/users',
        )

        assert res.status_code == 401
        # TODO: check error data

    for mthd in ['get', 'put', 'delete']:
        res = getattr(client, mthd)(
            '/v1/users/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
        )

        assert res.status_code == 401
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post', 'put'],
        ['groups', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/users/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
        )

        assert res.status_code == 401
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['delete'],
        ['groups', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/users/{item_id}/{attr}/{child_id}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
                child_id=tests.utils.random_string(),
            ),
        )

        assert res.status_code == 401
        # TODO: check error data


def test_users_requires_json(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    for mthd in ['post']:
        res = getattr(client, mthd)(
            '/v1/users',
            headers={
                **tests.utils.make_auth_header(some_user),
            },
        )

        assert res.status_code == 400
        # TODO: check error data

    for mthd in ['put']:
        res = getattr(client, mthd)(
            '/v1/users/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
        )

        assert res.status_code == 400
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['post', 'put'],
        ['groups', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/users/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
        )

        assert res.status_code == 400
        # TODO: check error data


def test_users_requires_permission(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    for mthd in ['get', 'post']:
        res = getattr(client, mthd)(
            '/v1/users',
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data

    for mthd in ['get', 'put', 'delete']:
        res = getattr(client, mthd)(
            '/v1/users/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post', 'put'],
        ['groups', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/users/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['delete'],
        ['groups', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/users/{item_id}/{attr}/{child_id}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
                child_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data


def test_users_not_found(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User),
    )

    db_session.commit()

    for mthd in ['get', 'delete']:
        res = getattr(client, mthd)(
            '/v1/users/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 404
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post', 'put'],
        ['groups', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/users/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 404
        # TODO: check error data


def test_users_create(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User),
    )

    db_session.commit()

    test_data = {
        'fullname': tests.utils.random_string(),
        'identifier': tests.utils.random_string(),
        'email': tests.utils.random_string(),
        'is_superuser': bool(random.getrandbits(1)),
        'is_admin': bool(random.getrandbits(1)),
        'is_staff': bool(random.getrandbits(1)),
        'is_active': bool(random.getrandbits(1)),
    }

    add_res = client.post(
        '/v1/users',
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=test_data,
    )

    assert add_res.status_code == 201
    # TODO: check response data

    assert sorted([
        item.identifier
        for item in db_session.query(User)
    ]) == sorted([
        some_user.identifier,
        test_data['identifier'],
    ])


def test_users_read(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'view'),
    )

    some_other_user = User(
        fullname=tests.utils.random_string(),
        identifier=tests.utils.random_string(),
    )

    db_session.add(some_other_user)
    db_session.commit()

    read_res = client.get(
        '/v1/users',
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert {
        **read_res.json,
        'items': sorted(read_res.json['items'], key=lambda d: d['identifier']),
    } == {
        'items': sorted([{
            **some_user.as_json(),
            'pending_task_count': len(some_user.pending_tasks),
        }, {
            **some_other_user.as_json(),
            'pending_task_count': len(some_other_user.pending_tasks),
        }], key=lambda d: d['identifier']),
    }


def test_users_update(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    db_session.commit()

    test_data = {
        'fullname': tests.utils.random_string(),
        'email': tests.utils.random_string(),
        'is_superuser': False,
        'is_admin': True,
        'is_staff': True,
        'is_active': bool(random.getrandbits(1)),
    }

    add_res = client.put(
        '/v1/users/{item_id}'.format(
            item_id=some_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=test_data,
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.identifier
        for item in db_session.query(User)
    ]) == sorted([
        some_user.identifier,
    ])


def test_users_delete(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User),
    )

    some_other_user = User(
        fullname=tests.utils.random_string(),
        identifier=tests.utils.random_string(),
        email=tests.utils.random_string(),
    )
    some_another_user = User(
        fullname=tests.utils.random_string(),
        identifier=tests.utils.random_string(),
        email=tests.utils.random_string(),
    )

    db_session.add(some_other_user)
    db_session.add(some_another_user)
    db_session.commit()

    add_res = client.delete(
        '/v1/users/{item_id}'.format(
            item_id=some_other_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.identifier for item in db_session.query(User)
    ]) == sorted([
        some_user.identifier,
        some_another_user.identifier,
    ])


def test_users_permissions_create(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_permission)
    db_session.commit()

    add_res = client.post(
        '/v1/users/{item_id}/permissions'.format(
            item_id=some_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json={
            'codename': some_permission.codename,
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.permissions
    ]) == sorted([
        'auth.change_user',
        some_permission.codename,
    ])


def test_users_permissions_read(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'view'),
    )

    db_session.commit()

    read_res = client.get(
        '/v1/users/{item_id}/permissions'.format(
            item_id=some_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert read_res.json == {
        'items': [{
            **db_session.query(Permission).filter(
                Permission.codename == 'auth.view_user',
            ).first().as_json(),
        }],
    }


def test_users_permissions_update(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_permission)
    db_session.commit()

    add_res = client.put(
        '/v1/users/{item_id}/permissions'.format(
            item_id=some_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=[
            {'codename': some_permission.codename},
        ],
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.permissions
    ]) == sorted([
        some_permission.codename,
    ])


def test_users_permissions_delete(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )
    some_permission.users.append(some_user)

    db_session.add(some_permission)
    db_session.commit()

    add_res = client.delete(
        '/v1/users/{item_id}/permissions/{child_id}'.format(
            item_id=some_user.identifier,
            child_id=some_permission.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.permissions
    ]) == sorted([
        'auth.change_user',
    ])


def test_users_groups_create(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.commit()

    add_res = client.post(
        '/v1/users/{item_id}/groups'.format(
            item_id=some_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json={
            'codename': some_group.codename,
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.groups
    ]) == sorted([
        some_group.codename,
    ])


def test_users_groups_read(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'view'),
    )

    some_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )
    some_group.users.append(some_user)

    db_session.add(some_group)
    db_session.commit()

    read_res = client.get(
        '/v1/users/{item_id}/groups'.format(
            item_id=some_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert read_res.json == {
        'items': [{
            **some_group.as_json(),
        }],
    }


def test_users_groups_update(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.commit()

    add_res = client.put(
        '/v1/users/{item_id}/groups'.format(
            item_id=some_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=[
            {'codename': some_group.codename},
        ],
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.groups
    ]) == sorted([
        some_group.codename,
    ])


def test_users_groups_delete(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )
    some_other_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )
    some_group.users.append(some_user)
    some_other_group.users.append(some_user)

    db_session.add(some_group)
    db_session.add(some_other_group)
    db_session.commit()

    add_res = client.delete(
        '/v1/users/{item_id}/groups/{child_id}'.format(
            item_id=some_user.identifier,
            child_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.groups
    ]) == sorted([
        some_other_group.codename,
    ])


def test_users_quirks_create(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_quirk = UserQuirk(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
        category=Category(
            name=tests.utils.random_string(),
            codename=snakecase(tests.utils.random_string()),
        ),
    )

    db_session.add(some_quirk)
    db_session.commit()

    add_res = client.post(
        '/v1/users/{item_id}/{attr}'.format(
            item_id=some_user.identifier,
            attr=some_quirk.category.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json={
            'codename': some_quirk.codename,
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.quirks
    ]) == sorted([
        some_quirk.codename,
    ])


def test_users_quirks_read(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'view'),
    )

    some_quirk = UserQuirk(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
        category=Category(
            name=tests.utils.random_string(),
            codename=snakecase(tests.utils.random_string()),
        ),
    )
    some_quirk.users.append(some_user)

    db_session.add(some_quirk)
    db_session.commit()

    read_res = client.get(
        '/v1/users/{item_id}/{attr}'.format(
            item_id=some_user.identifier,
            attr=some_quirk.category.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert read_res.json == {
        'items': [{
            **some_quirk.as_json(),
        }],
    }


def test_users_quirks_update(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_quirk = UserQuirk(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
        category=Category(
            name=tests.utils.random_string(),
            codename=snakecase(tests.utils.random_string()),
        ),
    )

    db_session.add(some_quirk)
    db_session.commit()

    add_res = client.put(
        '/v1/users/{item_id}/{attr}'.format(
            item_id=some_user.identifier,
            attr=some_quirk.category.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=[
            {'codename': some_quirk.codename},
        ],
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.quirks
    ]) == sorted([
        some_quirk.codename,
    ])


def test_users_quirks_delete(client):
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(User, 'change'),
    )

    some_quirk = UserQuirk(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
        category=Category(
            name=tests.utils.random_string(),
            codename=snakecase(tests.utils.random_string()),
        ),
    )
    some_other_quirk = UserQuirk(
        name=tests.utils.random_string(),
        codename=snakecase(tests.utils.random_string()),
        category=Category(
            name=tests.utils.random_string(),
            codename=snakecase(tests.utils.random_string()),
        ),
    )
    some_quirk.users.append(some_user)
    some_other_quirk.users.append(some_user)

    db_session.add(some_quirk)
    db_session.add(some_other_quirk)
    db_session.commit()

    add_res = client.delete(
        '/v1/users/{item_id}/{attr}/{child_id}'.format(
            item_id=some_user.identifier,
            attr=some_quirk.category.codename,
            child_id=some_quirk.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_user.quirks
    ]) == sorted([
        some_other_quirk.codename,
    ])
