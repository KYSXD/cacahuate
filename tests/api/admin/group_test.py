import itertools

from cacahuate.database import db_session
from cacahuate.database.permissions import create_default_permissions
from cacahuate.models import (
    Group,
    Permission,
)

import tests.utils


def test_groups_requires_auth(client):
    """Check if the methods require auth headers"""
    for mthd in ['get', 'post']:
        res = getattr(client, mthd)(
            '/v1/groups',
        )

        assert res.status_code == 401
        # TODO: check error data

    for mthd in ['get', 'put', 'delete']:
        res = getattr(client, mthd)(
            '/v1/groups/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
        )

        assert res.status_code == 401
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post', 'put'],
        ['users', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/groups/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
        )

        assert res.status_code == 401
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['delete'],
        ['users', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/groups/{item_id}/{attr}/{identifier}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
                identifier=tests.utils.random_string(),
            ),
        )

        assert res.status_code == 401
        # TODO: check error data


def test_groups_requires_json(client):
    """Check for json when needed"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    for mthd in ['post']:
        res = getattr(client, mthd)(
            '/v1/groups',
            headers={
                **tests.utils.make_auth_header(some_user),
            },
        )

        assert res.status_code == 400
        # TODO: check error data

    for mthd in ['put']:
        res = getattr(client, mthd)(
            '/v1/groups/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
        )

        assert res.status_code == 400
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['post', 'put'],
        ['users', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/groups/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
        )

        assert res.status_code == 400
        # TODO: check error data


def test_groups_requires_permission(client):
    """Check if the user has permission"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    for mthd in ['get', 'post']:
        res = getattr(client, mthd)(
            '/v1/groups',
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data

    for mthd in ['get', 'put', 'delete']:
        res = getattr(client, mthd)(
            '/v1/groups/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post', 'put'],
        ['users', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/groups/{item_id}/{attr}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['delete'],
        ['users', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/groups/{item_id}/{attr}/{child_id}'.format(
                item_id=tests.utils.random_string(),
                attr=attr,
                child_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data


def test_groups_not_found(client):
    """Check if the user exists"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group),
    )

    db_session.commit()

    for mthd in ['get', 'delete']:
        res = getattr(client, mthd)(
            '/v1/groups/{identifier}'.format(
                identifier=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 404
        # TODO: check error data

    for (mthd, attr) in list(itertools.product(
        ['get', 'post'],
        ['users', 'permissions'],
    )):
        res = getattr(client, mthd)(
            '/v1/groups/{identifier}/{attr}'.format(
                identifier=tests.utils.random_string(),
                attr=attr,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 404
        # TODO: check error data


def test_groups_create(client):
    """Add item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'add'),
    )

    db_session.commit()

    test_data = {
        'name': tests.utils.random_string(),
        'codename': tests.utils.random_string(),
    }

    add_res = client.post(
        '/v1/groups',
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=test_data,
    )

    assert add_res.status_code == 201
    # TODO: check response data

    assert sorted([
        item.codename
        for item in db_session.query(Group)
    ]) == sorted([
        test_data['codename'],
    ])


def test_groups_read(client):
    """Get items"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'view'),
    )

    some_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.commit()

    read_res = client.get(
        '/v1/groups',
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert read_res.json == {
        'items': [{
            **some_group.as_json(),
        }],
    }


def test_groups_update(client):
    """Add item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.commit()

    test_data = {
        'name': tests.utils.random_string(),
        'codename': tests.utils.random_string(),
    }

    add_res = client.put(
        '/v1/groups/{item_id}'.format(
            item_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=test_data,
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename
        for item in db_session.query(Group)
    ]) == sorted([
        some_group.codename,
    ])


def test_groups_delete(client):
    """Delete item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'delete'),
    )

    some_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )
    some_other_group = Group(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.add(some_other_group)
    db_session.commit()

    add_res = client.delete(
        '/v1/groups/{item_id}'.format(
            item_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in db_session.query(Group)
    ]) == sorted([
        some_other_group.codename,
    ])


def test_groups_users_create(client):
    """Add item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.commit()

    add_res = client.post(
        '/v1/groups/{item_id}/users'.format(
            item_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json={
            'identifier': some_user.identifier,
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.identifier for item in some_group.users
    ]) == sorted([
        some_user.identifier,
    ])


def test_groups_users_read(client):
    """Get items"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'view'),
    )

    some_group = Group(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )
    some_group.users.append(some_user)

    db_session.add(some_group)
    db_session.commit()

    read_res = client.get(
        '/v1/groups/{item_id}/users'.format(
            item_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert read_res.json == {
        'items': [{
            **some_user.as_json(),
        }],
    }


def test_groups_users_update(client):
    """Set item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.commit()

    add_res = client.put(
        '/v1/groups/{item_id}/users'.format(
            item_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=[
            {'identifier': some_user.identifier},
        ],
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.identifier for item in some_group.users
    ]) == sorted([
        some_user.identifier,
    ])


def test_groups_users_delete(client):
    """Delete item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )
    some_group.users.append(some_user)

    db_session.add(some_group)
    db_session.commit()

    add_res = client.delete(
        '/v1/groups/{item_id}/users/{identifier}'.format(
            item_id=some_group.codename,
            identifier=some_user.identifier,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.identifier for item in some_group.users
    ]) == sorted([
    ])


def test_groups_permissions_create(client):
    """Add item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.add(some_permission)
    db_session.commit()

    add_res = client.post(
        '/v1/groups/{item_id}/permissions'.format(
            item_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json={
            'codename': some_permission.codename,
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_group.permissions
    ]) == sorted([
        some_permission.codename,
    ])


def test_groups_permissions_read(client):
    """Get items"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'view'),
    )

    some_group = Group(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )
    some_permission.groups.append(some_group)

    db_session.add(some_group)
    db_session.add(some_permission)
    db_session.commit()

    read_res = client.get(
        '/v1/groups/{item_id}/permissions'.format(
            item_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert read_res.json == {
        'items': [{
            **some_permission.as_json(),
        }],
    }


def test_groups_permissions_update(client):
    """Set item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    db_session.add(some_group)
    db_session.add(some_permission)
    db_session.commit()

    add_res = client.put(
        '/v1/groups/{item_id}/permissions'.format(
            item_id=some_group.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=[
            {'codename': some_permission.codename},
        ],
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_group.permissions
    ]) == sorted([
        some_permission.codename,
    ])


def test_groups_permissions_delete(client):
    """Delete item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Group, 'change'),
    )

    some_group = Group(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )
    some_other_permission = Permission(
        name=tests.utils.random_string(),  # TODO: use and check default name
        codename=tests.utils.random_string(),
    )

    some_permission.groups.append(some_group)
    some_other_permission.groups.append(some_group)

    db_session.add(some_permission)
    db_session.add(some_other_permission)
    db_session.commit()

    add_res = client.delete(
        '/v1/groups/{item_id}/permissions/{child_id}'.format(
            item_id=some_group.codename,
            child_id=some_permission.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename for item in some_group.permissions
    ]) == sorted([
        some_other_permission.codename,
    ])
