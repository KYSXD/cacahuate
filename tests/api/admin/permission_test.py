from cacahuate.database import db_session
from cacahuate.database.permissions import create_default_permissions
from cacahuate.database.query import query_permissions
from cacahuate.models import (
    Permission,
)

import tests.utils


def test_permissions_requires_auth(client):
    """Check if the methods require auth headers"""
    for mthd in ['get', 'post']:
        res = getattr(client, mthd)(
            '/v1/permissions',
        )

        assert res.status_code == 401
        # TODO: check error data

    for mthd in ['put', 'delete']:
        res = getattr(client, mthd)(
            '/v1/permissions/{codename}'.format(
                codename=tests.utils.random_string(),
            ),
        )

        assert res.status_code == 401
        # TODO: check error data


def test_permissions_requires_json(client):
    """Check for json when needed"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    for mthd in ['post']:
        res = getattr(client, mthd)(
            '/v1/permissions',
            headers={
                **tests.utils.make_auth_header(some_user),
            },
        )

        assert res.status_code == 400
        # TODO: check error data

    for mthd in ['put']:
        r = getattr(client, mthd)(
            '/v1/permissions/{item_id}'.format(
                item_id=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(),
            },
        )

        assert r.request.url and r.request.method and r.status_code == 400
        # TODO: check error data


def test_permissions_requires_permission(client):
    """Check if the user has permission"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    for mthd in ['post']:
        res = getattr(client, mthd)(
            '/v1/permissions',
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data

    for mthd in ['put', 'delete']:
        res = getattr(client, mthd)(
            '/v1/permissions/{codename}'.format(
                codename=tests.utils.random_string(),
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json={},
        )

        assert res.status_code == 403
        # TODO: check error data


def test_permissions_create(client):
    """Add item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Permission, 'add'),
    )

    db_session.commit()

    test_data = {
        'name': tests.utils.random_string(),
        'codename': tests.utils.random_string(),
    }

    add_res = client.post(
        '/v1/permissions',
        headers={
            **tests.utils.make_auth_header(some_user),
        },
        json=test_data,
    )

    assert add_res.status_code == 201
    # TODO: check response data

    assert sorted([
        item.codename
        for item in query_permissions(codename__eq=test_data['codename'])
    ]) == sorted([
        test_data['codename'],
    ])


def test_permissions_read(client):
    """Get items"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Permission, 'view'),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_permission)
    db_session.commit()

    read_res = client.get(
        '/v1/permissions',
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert {
        **read_res.json,
        'items': sorted(read_res.json['items'], key=lambda d: d['codename']),
    } == {
        'items': sorted((
            item.as_json()
            for item in query_permissions()
        ), key=lambda d: d['codename']),
    }


def test_permissions_update(client):
    some_user = tests.utils.make_user()

    some_user.permissions.extend(
        create_default_permissions(Permission, 'change'),
    )

    db_session.commit()

    current_tests = [
        {
            'name': tests.utils.random_string(),
            'codename': tests.utils.random_string(),
        },
        {
            'name': tests.utils.random_string(),
            'codename': tests.utils.random_string(),
        },
    ]

    item_id = tests.utils.random_string()
    for index, test_data in enumerate(current_tests, start=1):
        r = client.put(
            '/v1/permissions/{item_id}'.format(
                item_id=item_id,
            ),
            headers={
                **tests.utils.make_auth_header(some_user),
            },
            json=test_data,
        )

        assert r.request.url and r.request.method and r.status_code == 202
        # TODO: check response data

        assert sorted([
            item.codename
            for item in query_permissions(codename__eq=item_id)
        ]) == sorted([
            item_id,
        ])


def test_permissions_delete(client):
    """Delete item"""
    some_user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    some_user.permissions.extend(
        create_default_permissions(Permission, 'delete'),
    )

    some_permission = Permission(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )
    some_other_permission = Permission(
        name=tests.utils.random_string(),
        codename=tests.utils.random_string(),
    )

    db_session.add(some_permission)
    db_session.add(some_other_permission)
    db_session.commit()

    add_res = client.delete(
        '/v1/permissions/{codename}'.format(
            codename=some_permission.codename,
        ),
        headers={
            **tests.utils.make_auth_header(some_user),
        },
    )

    assert add_res.status_code == 202
    # TODO: check response data

    assert sorted([
        item.codename
        for item in query_permissions(codename__eq=some_permission.codename)
    ]) == sorted([
    ])
