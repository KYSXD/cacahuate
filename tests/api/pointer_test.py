from random import choice
from string import ascii_letters

from cacahuate.database import db_session
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.mongo import (
    execution_entry,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

from flask import json

import tests.utils
from tests.utils import (
    make_auth_header,
    make_pointer,
    make_user,
)


def test_continue_process_asks_for_user(client):
    res = client.post('/v1/pointer')

    assert res.status_code == 401
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'You must provide basic authorization headers',
            'where': 'request.authorization',
        }],
    }


def test_continue_process_requires(client):
    juan = make_user('juan', 'Juan')

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({}))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'execution_id' is required",
                'code': 'validation.required',
                'where': 'request.body.execution_id',
            },
            {
                'detail': "'node_id' is required",
                'code': 'validation.required',
                'where': 'request.body.node_id',
            },
        ],
    }


def test_continue_process_asks_living_objects(client):
    ''' the app must validate that the ids sent are real objects '''
    juan = make_user('juan', 'Juan')

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': 'verde',
        'node_id': 'nada',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'execution_id is not valid',
                'code': 'validation.invalid',
                'where': 'request.body.execution_id',
            },
        ],
    }


def test_continue_process_requires_valid_node(client):
    juan = make_user('juan', 'Juan')
    exc = Execution(
        process_name='simple.2018-02-19.xml',
        name=tests.utils.random_string(),
        description=tests.utils.random_string(),
        status='ongoing',
    )
    db_session.add(exc)
    db_session.commit()

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'notarealnode',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'node_id is not a valid node',
                'code': 'validation.invalid_node',
                'where': 'request.body.node_id',
            },
        ],
    }


def test_continue_process_requires_living_pointer(config, client, mongo):
    juan = make_user('juan', 'Juan')
    exc = Execution(
        process_name='simple.2018-02-19.xml',
        name=tests.utils.random_string(),
        description=tests.utils.random_string(),
        status='ongoing',
    )
    db_session.add(exc)
    db_session.commit()

    main_exe = exc
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'mid_node',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'node_id does not have a live pointer',
                'code': 'validation.no_live_pointer',
                'where': 'request.body.node_id',
            },
        ],
    }


def test_continue_process_requires_user_hierarchy(config, client, mongo):
    ''' a node whose auth has a filter must be completed by a person matching
    the filter '''
    juan = make_user('juan', 'Juan')
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')

    main_exe = ptr.execution
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': ptr.execution.id,
        'node_id': ptr.node_id,
    }))

    assert res.status_code == 403
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'Provided user does not have this task assigned',
            'where': 'request.authorization',
        }],
    }


def test_continue_process_requires_data(config, client, mongo):
    manager = make_user('juan_manager', 'Juanote')
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')

    main_exe = ptr.execution
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    manager.assigned_tasks.extend([ptr])

    db_session.commit()

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(manager)}, data=json.dumps({
        'execution_id': ptr.execution.id,
        'node_id': ptr.node_id,
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [{
            'detail': "form count lower than expected for ref mid_form",
            'where': 'request.body.form_array',
        }],
    }


def test_continue_process(client, mocker, config, mongo):
    mocker.patch('cacahuate.tasks.handle.delay')

    manager = make_user('juan_manager', 'Juanote')
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')

    main_exe = ptr.execution
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    manager.assigned_tasks.extend([ptr])

    db_session.commit()

    exc = ptr.execution

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(manager)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': ptr.node_id,
        'form_array': [
            {
                'ref': 'mid_form',
                'data': {
                    'data': 'yes',
                },
            },
        ],
    }))

    assert res.status_code == 202
    assert json.loads(res.data) == {
        'data': 'accepted',
    }

    # rabbit is called
    handle.delay.assert_called_once()

    json_message = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': 'juan_manager',
        'input': [Form.state_json('mid_form', [
            {
                "name": "data",
                "type": "text",
                "value": "yes",
                'label': 'data',
                'value_caption': 'yes',
                'state': 'valid',
                'hidden': False,
            },
        ])],
    }

    args = handle.delay.call_args[0][0]
    body = json.loads(args)
    assert body == json_message

    pointer = db_session.query(Pointer).filter(
        Pointer.id == json_message['pointer_id'],
    ).first()

    assert pointer.id == ptr.id


def test_validation_requirements(client, config, mongo):
    juan = make_user('juan', 'Juan')
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')

    exc = ptr.execution

    main_exe = exc
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    juan.assigned_tasks.append(ptr)

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'response' is required",
                'code': 'validation.required',
                'where': 'request.body.response',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': ''.join(choice(ascii_letters) for c in range(10)),
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'response' value invalid",
                'code': 'validation.invalid',
                'where': 'request.body.response',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs' is required",
                'code': 'validation.required',
                'where': 'request.body.inputs',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': 'de',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs' must be a list",
                'code': 'validation.required_list',
                'where': 'request.body.inputs',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs' must be a list",
                'code': 'validation.required_list',
                'where': 'request.body.inputs',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': ['de'],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs.0' must be an object",
                'code': 'validation.required_dict',
                'where': 'request.body.inputs.0',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [{
        }],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs.0.ref' is required",
                'code': 'validation.required',
                'where': 'request.body.inputs.0.ref',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [{
            'ref': 'de',
        }],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs.0.ref' value invalid",
                'code': 'validation.invalid',
                'where': 'request.body.inputs.0.ref',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [{
            'ref': 'start_node.juan.0:work.task',
        }],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'comment' is required",
                'code': 'validation.required',
                'where': 'request.body.comment',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [{
            'ref': 'start_node.juan.0:work.task',
        }],
        'comment': [],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'comment' must be a str",
                'code': 'validation.invalid',
                'where': 'request.body.comment',
            },
        ],
    }


def test_validation_approval(client, mocker, config, mongo):
    ''' the api for an approval '''
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')

    exc = ptr.execution

    main_exe = exc
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    juan.assigned_tasks.append(ptr)

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'accept',
        'comment': 'I like the previous work',
    }))

    assert res.status_code == 202

    # rabbit is called
    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': 'juan',
        'input': [Form.state_json('approval_node', [
            {
                'name': 'response',
                'value': 'accept',
                'value_caption': 'accept',
            },
            {
                'name': 'comment',
                'value': 'I like the previous work',
                'value_caption': 'I like the previous work',
            },
            {
                'name': 'inputs',
                'value': None,
                'value_caption': 'null',
            },
        ])],
    }


def test_validation_reject(client, mocker, config, mongo):
    ''' the api for a reject '''
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')

    exc = ptr.execution

    main_exe = exc
    mongo[config["EXECUTION_COLLECTION"]].insert_one(execution_entry(
        main_exe,
        Xml.load(config, main_exe.process_name).get_state(),
    ))

    juan.assigned_tasks.append(ptr)

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': ptr.node_id,
        'response': 'reject',
        'comment': 'I dont like it',
        'inputs': [{
            'ref': 'start_node.juan.0:work.task',
        }],
    }))

    assert res.status_code == 202

    # rabbit is called
    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': 'juan',
        'input': [Form.state_json('approval_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'I dont like it',
                'value_caption': 'I dont like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:work.task',
                }],
                'value_caption': '[{"ref": "start_node.juan.0:work.task"}]',
            },
        ])],
    }
