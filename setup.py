#!/usr/bin/env python3
from os import path

from setuptools import setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst')) as f:
    long_description = f.read()

with open(path.join(here, 'cacahuate', 'version.txt')) as f:
    version = f.read().strip()

setup(
    name='cacahuate',
    description='The process virtual machine',
    long_description=long_description,
    url='https://github.com/tracsa/cacahuate',

    version=version,

    author='Abraham Toriz Cruz',
    author_email='categulario@gmail.com',
    license='MIT',

    classifiers=[
        'Development Status :: 4 - Beta',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries :: Python Modules',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],

    keywords='process',

    packages=[
        'cacahuate',
        'cacahuate.models',
        'cacahuate.mongo',
        'cacahuate.handler',
        'cacahuate.http',
        'cacahuate.http.views',
        'cacahuate.http.views.admin',
        'cacahuate.http.views.api',
        'cacahuate.http.views.workflow',
        'cacahuate.auth',
        'cacahuate.auth.backends',
        'cacahuate.auth.hierarchy',
        'cacahuate.xml',
        'cacahuate.database',
        'cacahuate.migrations',
        'cacahuate.migrations.versions',
    ],

    package_data={
        'cacahuate': [
            'grammars/*.g',
            'xml/*.rng',
            'version.txt',
            'templates/*.html',
            'alembic.ini',
        ],
    },

    entry_points={
        'console_scripts': [
            'cacahuated = cacahuate.main:main',
            'xmlvalidate = cacahuate.xml.validate:xmlvalidate',
            'rng_path = cacahuate.main:rng_path',
            'migrate = cacahuate.migrations.migrate:main',
            'makemigrations = cacahuate.migrations.makemigrations:main',
        ],
    },

    install_requires=[
        'alembic',
        'case_conversion',
        'celery',
        'flask >= 2.3',
        'Flask-Cors',
        'Flask_PyMongo < 2.0',
        (
            'graphene-sqlalchemy @ '
            'git+https://github.com/graphql-python/graphene-sqlalchemy@'
            'b30bc921cb3881a7d8cf9873d9b192788e749c6b'
            '#egg=graphene-sqlalchemy'
        ),
        'graphql-server == 3.0.0b6',
        'itacate',
        'jinja2',
        'jsonpath-ng',
        'lark-parser >= 0.6',
        'ldap3',
        'passlib',
        'pika >= 1.0',
        'psycopg2-binary',
        'pyjwt >= 2.0',
        'pymongo < 4.0',
        'python-dateutil',
        'pyyaml',
        'requests',
        'simplejson',
        'SQLAlchemy >= 2.0',
    ],

    setup_requires=[
        'pytest-runner',
    ],

    tests_require=[
        'pytest',
        'pytest-mock',
    ],
)
