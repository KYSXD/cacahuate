import cacahuate.models

import graphene
import graphene.relay

import graphene_sqlalchemy
import graphene_sqlalchemy.filters


class Execution(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Execution
        interfaces = (
            graphene.relay.Node,
        )


class Pointer(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Pointer
        interfaces = (
            graphene.relay.Node,
        )


class User(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.User
        interfaces = (
            graphene.relay.Node,
        )


class Group(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Group
        interfaces = (
            graphene.relay.Node,
        )


class Permission(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Permission
        interfaces = (
            graphene.relay.Node,
        )


class UserQuirk(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.UserQuirk
        interfaces = (
            graphene.relay.Node,
        )


class Category(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Category
        interfaces = (
            graphene.relay.Node,
        )


class Query(graphene.ObjectType):
    executions = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Execution.connection,
    )

    pointers = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Pointer.connection,
    )

    users = graphene_sqlalchemy.SQLAlchemyConnectionField(
        User.connection,
    )

    groups = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Group.connection,
    )

    permissions = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Permission.connection,
    )

    quirks = graphene_sqlalchemy.SQLAlchemyConnectionField(
        UserQuirk.connection,
    )

    category = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Category.connection,
    )


schema = graphene.Schema(
    query=Query,
)
