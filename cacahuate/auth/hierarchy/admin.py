import logging
import traceback

import cacahuate.auth.base
import cacahuate.models

LOGGER = logging.getLogger(__name__)


class AdminHierarchyProvider(cacahuate.auth.base.BaseHierarchyProvider):

    def find_users(self, **params):
        payload = {
            'is_active__eq': True,
        }

        mapper = {
            'search_query': str,
            'identifier__eq': str,
            'identifier__in': lambda x: str(x).split(','),
            'email__eq': str,
            'email__lower__eq': lambda x: str(x).lower(),
            'email__in': lambda x: str(x).split(','),
            'email__lower__in': lambda x: str(x).lower().split(','),
            'fullname__gt': str,
            'fullname__lt': str,
            'groups__codename__eq': str,
            'groups__codename__in': lambda x: str(x).split(','),
            'groups__name__eq': str,
            'groups__name__in': lambda x: str(x).split(','),
            'permissions__codename__eq': str,
            'permissions__codename__in': lambda x: str(x).split(','),
            'permissions__name__eq': str,
            'permissions__name__in': lambda x: str(x).split(','),
        }

        quirk_mapper = {
            'codename__eq': str,
            'codename__in': lambda x: str(x).split(','),
            'name__eq': str,
            'name__in': lambda x: str(x).split(','),
        }

        user_quirk_filters = {
            fltr: q_arg
            for fltr, q_arg in (params or {}).items()
            if not fltr.startswith((
                *cacahuate.models.User.get_fields(),
                'groups',
                'permissions',
            ))
        }

        for key, value in params.items():
            if value is None:
                continue

            if key in mapper:
                fn = mapper[key]
                payload[key] = fn(value)
                continue

            if key not in user_quirk_filters:
                continue

            tokens = key.split('__', 1)
            if len(tokens) == 2 and tokens[1] in quirk_mapper:
                fn = quirk_mapper[tokens[1]]
                payload[key] = fn(value)

        try:
            return [(
                user.identifier,
                {
                    'identifier': user.identifier,
                    'email': user.email,
                    'fullname': user.fullname,
                },
            ) for user in cacahuate.database.query.query_users(**payload)]
        except Exception:
            LOGGER.error(traceback.format_exc())
            return []
