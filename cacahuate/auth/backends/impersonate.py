from cacahuate.auth.base import BaseAuthProvider
from cacahuate.database import db_session
from cacahuate.errors import AuthFieldInvalid
from cacahuate.errors import AuthFieldRequired
from cacahuate.models import User

from passlib.hash import pbkdf2_sha256


class ImpersonateAuthProvider(BaseAuthProvider):

    def authenticate(self, **credentials):
        if 'username' not in credentials:
            raise AuthFieldRequired('username')
        if 'password' not in credentials:
            raise AuthFieldRequired('password')

        user = db_session.query(User).filter(
            User.identifier == credentials['username'],
        ).first()

        if not user:
            raise AuthFieldInvalid('username')

        verified = pbkdf2_sha256.verify(
            credentials['password'],
            self.config['IMPERSONATE_PASSWORD'],
        )

        if not verified:
            raise AuthFieldInvalid('password')

        return user.identifier, user.as_json()
