import sqlalchemy
import sqlalchemy.engine
import sqlalchemy.ext.declarative
import sqlalchemy.orm


engine = None
db_session = sqlalchemy.orm.scoped_session(
    lambda: sqlalchemy.orm.create_session(
        autoflush=False,
        autocommit=False,
        bind=engine,
    ),
)

Base = sqlalchemy.orm.declarative_base()
Base.query = db_session.query_property()


def init_engine(uri: str, **kwargs: any) -> None:
    global engine
    engine = sqlalchemy.create_engine(uri, **kwargs)
