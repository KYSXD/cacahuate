import cacahuate.database
import cacahuate.models


def create_default_permissions(
    model,
    actions=None,
):
    model_name = model.__name__.lower()

    if actions is None:
        actions = ("add", "change", "delete", "view")
    elif type(actions) not in [tuple, list]:
        actions = [actions]

    perms = []
    for action in actions:
        name = f'Can {action} {model_name}'
        codename = f'auth.{action}_{model_name}'

        instance = cacahuate.database.db_session.query(
            cacahuate.models.Permission,
        ).filter(cacahuate.models.Permission.codename == codename).first()

        if instance:
            instance.name = name
        else:
            instance = cacahuate.models.Permission(
                name=name,
                codename=codename,
            )
            cacahuate.database.db_session.add(instance)

        perms.append(instance)

    cacahuate.database.db_session.commit()

    return perms


def create_initial_permissions():
    perms = []

    for model in (
        cacahuate.models.User,
        cacahuate.models.Group,
        cacahuate.models.Permission,
        cacahuate.models.UserQuirk,
        cacahuate.models.Category,
        cacahuate.models.ProcessDefinition,
        cacahuate.models.Execution,
        cacahuate.models.Pointer,
        cacahuate.models.WorkflowDefinition,
        cacahuate.models.TaskDefinition,
        cacahuate.models.FieldsetDefinition,
        cacahuate.models.Field,
    ):
        perms += create_default_permissions(model)

    return perms
