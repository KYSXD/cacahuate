from cacahuate.database import db_session
from cacahuate.models import (
    Category,
    Execution,
    Group,
    Permission,
    Pointer as Task,
    ProcessDefinition as Process,
    User,
    UserQuirk,
)

from sqlalchemy import (
    Date,
    Time,
    and_,
    asc,
    desc,
    or_,
    select,
)
from sqlalchemy.orm import (
    aliased,
    selectinload,
    undefer,
)
from sqlalchemy.sql import (
    cast,
    distinct,
    func,
)

BASE_OPERATORS_MAP = {
    'eq': '__eq__',
    'in': 'in_',
    'not_in': 'not_in',
    'contains': 'contains',
    'icontains': 'icontains',
    'startswith': 'startswith',
    'istartswith': 'istartswith',
    'gt': '__gt__',
    'ge': '__ge__',
    'lt': '__lt__',
    'le': '__le__',
}

TRANSFORMERS_MAP = {
    'time__gt': (lambda x: cast(x, Time), '__gt__'),
    'time__ge': (lambda x: cast(x, Time), '__ge__'),
    'time__lt': (lambda x: cast(x, Time), '__lt__'),
    'time__le': (lambda x: cast(x, Time), '__le__'),
    'date__gt': (lambda x: cast(x, Date), '__gt__'),
    'date__ge': (lambda x: cast(x, Date), '__ge__'),
    'date__lt': (lambda x: cast(x, Date), '__lt__'),
    'date__le': (lambda x: cast(x, Date), '__le__'),
    'lower__eq': (func.lower, '__eq__'),
    'lower__in': (func.lower, 'in_'),
    'upper__eq': (func.upper, '__eq__'),
    'upper__in': (func.upper, 'in_'),
}


def extract_filters(model, prefix='', alias=None, **params):
    fields = model.get_fields()

    fltrs = []
    for fld in fields:
        fld_candidate = f'{prefix}{fld}'
        for op, fn in BASE_OPERATORS_MAP.items():
            value = params.get(f'{fld_candidate}__{op}')
            if value is not None:
                source = model
                if alias:
                    source = alias

                model_field = getattr(source, fld)
                q_fn = getattr(model_field, fn)
                fltrs.append(q_fn(value))

        for op, (astype, fn) in TRANSFORMERS_MAP.items():
            value = params.get(f'{fld_candidate}__{op}')
            if value is not None:
                source = model
                if alias:
                    source = alias

                model_field = getattr(source, fld)
                q_fn = getattr(astype(model_field), fn)
                fltrs.append(q_fn(value))

    return fltrs


def count_executions(**params):
    stmt = select(func.count(distinct(Execution.id))).select_from(Execution)

    task, task_join = (aliased(Task), False)

    q_arg = params.get('search_query')
    if q_arg is not None:
        task_join = True
        stmt = stmt.join(task, Execution.pointers)

        stmt = stmt.filter(or_(
            func.lower(Execution.name).contains(func.lower(q_arg)),
            Execution.id.__eq__(q_arg),
            func.lower(task.name).contains(func.lower(q_arg)),
            task.id.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Execution,
        **params,
    ):
        stmt = stmt.filter(fltr)

    for fltr in extract_filters(
        model=Task,
        prefix='tasks__',
        alias=task,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        stmt = stmt.filter(fltr)

    task_actor, task_actor_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='tasks__actors__',
        alias=task_actor,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_actor_join:
            task_actor_join = True
            stmt = stmt.join(task_actor, task.actors)
        stmt = stmt.filter(fltr)

    task_actor_group, task_actor_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='tasks__actors__groups__',
        alias=task_actor_group,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_actor_join:
            task_actor_join = True
            stmt = stmt.join(task_actor, task.actors)
        if not task_actor_group_join:
            task_actor_group_join = True
            stmt = stmt.join(task_actor_group, task_actor.groups)
        stmt = stmt.filter(fltr)

    task_candidate, task_candidate_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='tasks__candidates__',
        alias=task_candidate,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_candidate_join:
            task_candidate_join = True
            stmt = stmt.join(task_candidate, task.candidates)
        stmt = stmt.filter(fltr)

    task_candidate_group, task_candidate_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='tasks__candidates__groups__',
        alias=task_candidate_group,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_candidate_join:
            task_candidate_join = True
            stmt = stmt.join(task_candidate, task.candidates)
        if not task_candidate_group_join:
            task_candidate_group_join = True
            stmt = stmt.join(task_candidate_group, task_candidate.groups)
        stmt = stmt.filter(fltr)

    return db_session.scalar(stmt)


def query_processes(
    *,
    offset=None,
    limit=None,
    **params,
):
    stmt = select(Process, func.lower(Process.name))

    q_arg = params.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(Process.name).contains(func.lower(q_arg)),
            func.lower(Process.identifier).contains(func.lower(q_arg)),
        ))

    for fltr in extract_filters(
        model=Process,
        **params,
    ):
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(asc(func.lower(Process.name)))

    stmt = stmt.offset(offset).limit(limit)
    return db_session.execute(stmt).scalars()


def query_executions(
    *,
    offset=None,
    limit=None,
    selectinloaded=None,
    undeferred=None,
    **params,
):
    stmt = select(Execution)

    task, task_join = (aliased(Task), False)

    q_arg = params.get('search_query')
    if q_arg is not None:
        task_join = True
        stmt = stmt.join(task, Execution.pointers)

        stmt = stmt.filter(or_(
            func.lower(Execution.name).contains(func.lower(q_arg)),
            Execution.id.__eq__(q_arg),
            func.lower(task.name).contains(func.lower(q_arg)),
            task.id.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Execution,
        **params,
    ):
        stmt = stmt.filter(fltr)

    for fltr in extract_filters(
        model=Task,
        prefix='tasks__',
        alias=task,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        stmt = stmt.filter(fltr)

    task_actor, task_actor_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='tasks__actors__',
        alias=task_actor,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_actor_join:
            task_actor_join = True
            stmt = stmt.join(task_actor, task.actors)
        stmt = stmt.filter(fltr)

    task_actor_group, task_actor_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='tasks__actors__groups__',
        alias=task_actor_group,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_actor_join:
            task_actor_join = True
            stmt = stmt.join(task_actor, task.actors)
        if not task_actor_group_join:
            task_actor_group_join = True
            stmt = stmt.join(task_actor_group, task_actor.groups)
        stmt = stmt.filter(fltr)

    task_candidate, task_candidate_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='tasks__candidates__',
        alias=task_candidate,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_candidate_join:
            task_candidate_join = True
            stmt = stmt.join(task_candidate, task.candidates)
        stmt = stmt.filter(fltr)

    task_candidate_group, task_candidate_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='tasks__candidates__groups__',
        alias=task_candidate_group,
        **params,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_candidate_join:
            task_candidate_join = True
            stmt = stmt.join(task_candidate, task.candidates)
        if not task_candidate_group_join:
            task_candidate_group_join = True
            stmt = stmt.join(task_candidate_group, task_candidate.groups)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(desc(Execution.started_at))

    if selectinloaded:
        stmt = stmt.options(selectinload(*selectinloaded))

    if undeferred:
        stmt = stmt.options(undefer(*undeferred))

    stmt = stmt.offset(offset).limit(limit)
    return db_session.execute(stmt).scalars()


def count_tasks(**params):
    stmt = select(func.count(distinct(Task.id))).select_from(Task)

    execution, execution_join = (aliased(Execution), False)

    q_arg = params.get('search_query')
    if q_arg is not None:
        execution_join = True
        stmt = stmt.join(execution, Task.execution)

        stmt = stmt.filter(or_(
            func.lower(Task.name).contains(func.lower(q_arg)),
            Task.id.__eq__(q_arg),
            func.lower(execution.name).contains(func.lower(q_arg)),
            execution.id.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Task,
        **params,
    ):
        stmt = stmt.filter(fltr)

    for fltr in extract_filters(
        model=Execution,
        prefix='execution__',
        alias=execution,
        **params,
    ):
        if not execution_join:
            execution_join = True
            stmt = stmt.join(execution, Task.execution)
        stmt = stmt.filter(fltr)

    actor, actor_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='actors__',
        alias=actor,
        **params,
    ):
        if not actor_join:
            actor_join = True
            stmt = stmt.join(actor, Task.actors)
        stmt = stmt.filter(fltr)

    actor_group, actor_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='actors__groups__',
        alias=actor_group,
        **params,
    ):
        if not actor_join:
            actor_join = True
            stmt = stmt.join(actor, Task.actors)
        if not actor_group_join:
            actor_group_join = True
            stmt = stmt.join(actor_group, actor.groups)
        stmt = stmt.filter(fltr)

    candidate, candidate_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='candidates__',
        alias=candidate,
        **params,
    ):
        if not candidate_join:
            candidate_join = True
            stmt = stmt.join(candidate, Task.candidates)
        stmt = stmt.filter(fltr)

    candidate_group, candidate_groups_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='candidates__groups__',
        alias=candidate_group,
        **params,
    ):
        if not candidate_join:
            candidate_join = True
            stmt = stmt.join(candidate, Task.candidates)
        if not candidate_groups_join:
            candidate_groups_join = True
            stmt = stmt.join(candidate_group, candidate.groups)
        stmt = stmt.filter(fltr)

    return db_session.scalar(stmt)


def query_tasks(
    *,
    offset=None,
    limit=None,
    selectinloaded=None,
    **params,
):
    stmt = select(Task)

    execution, execution_join = (aliased(Execution), False)

    q_arg = params.get('search_query')
    if q_arg is not None:
        execution_join = True
        stmt = stmt.join(execution, Task.execution)

        stmt = stmt.filter(or_(
            func.lower(Task.name).contains(func.lower(q_arg)),
            Task.id.__eq__(q_arg),
            func.lower(execution.name).contains(func.lower(q_arg)),
            execution.id.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Task,
        **params,
    ):
        stmt = stmt.filter(fltr)

    for fltr in extract_filters(
        model=Execution,
        prefix='execution__',
        alias=execution,
        **params,
    ):
        if not execution_join:
            execution_join = True
            stmt = stmt.join(execution, Task.execution)
        stmt = stmt.filter(fltr)

    actor, actor_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='actors__',
        alias=actor,
        **params,
    ):
        if not actor_join:
            actor_join = True
            stmt = stmt.join(actor, Task.actors)
        stmt = stmt.filter(fltr)

    actor_group, actor_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='actors__groups__',
        alias=actor_group,
        **params,
    ):
        if not actor_join:
            actor_join = True
            stmt = stmt.join(actor, Task.actors)
        if not actor_group_join:
            actor_group_join = True
            stmt = stmt.join(actor_group, actor.groups)
        stmt = stmt.filter(fltr)

    candidate, candidate_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='candidates__',
        alias=candidate,
        **params,
    ):
        if not candidate_join:
            candidate_join = True
            stmt = stmt.join(candidate, Task.candidates)
        stmt = stmt.filter(fltr)

    candidate_group, candidate_groups_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='candidates__groups__',
        alias=candidate_group,
        **params,
    ):
        if not candidate_join:
            candidate_join = True
            stmt = stmt.join(candidate, Task.candidates)
        if not candidate_groups_join:
            candidate_groups_join = True
            stmt = stmt.join(candidate_group, candidate.groups)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(desc(Task.started_at))

    if selectinloaded:
        stmt = stmt.options(selectinload(*selectinloaded))

    stmt = stmt.offset(offset).limit(limit)
    return db_session.execute(stmt).scalars()


def query_users(
    *,
    offset=None,
    limit=None,
    selectinloaded=None,
    **params,
):
    stmt = select(User, func.lower(User.fullname))

    q_arg = params.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(User.fullname).contains(func.lower(q_arg)),
            func.lower(User.email).contains(func.lower(q_arg)),
            func.lower(User.identifier).contains(func.lower(q_arg)),
        ))

    for fltr in extract_filters(
        model=User,
        **params,
    ):
        stmt = stmt.filter(fltr)

    group, group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='groups__',
        alias=group,
        **params,
    ):
        if not group_join:
            group_join = True
            stmt = stmt.join(group, User.groups)
        stmt = stmt.filter(fltr)

    permission, permission_join = (aliased(Permission), False)
    for fltr in extract_filters(
        model=Permission,
        prefix='permissions__',
        alias=permission,
        **params,
    ):
        if not permission_join:
            permission_join = True
            stmt = stmt.join(permission, User.permissions)
        stmt = stmt.filter(fltr)

    user_quirk_filters = {
        fltr: q_arg
        for fltr, q_arg in (params or {}).items()
        if not fltr.startswith((
            *User.get_fields(),
            'groups',
            'permissions',
        ))
    }

    aliased_joins = {}
    for fltr, q_arg in user_quirk_filters.items():
        try:
            (category_codename, quirk_attr, op) = fltr.split('__', 2)
        except ValueError:
            stmt.filter(False)
            break

        if quirk_attr not in UserQuirk.get_fields():
            stmt.filter(False)
            break

        if op not in BASE_OPERATORS_MAP:
            stmt.filter(False)
            break

        quirk = aliased_joins.get(f'{category_codename}.{quirk_attr}')
        if not quirk:
            quirk = aliased(UserQuirk)
            aliased_joins[f'{category_codename}.{quirk_attr}'] = quirk
            stmt = stmt.join(quirk, User.quirks)

        category = aliased_joins.get(category_codename)
        if not category:
            category = aliased(Category)
            aliased_joins[category_codename] = category
            stmt = stmt.join(category, quirk.category)

        quirk_field = getattr(quirk, quirk_attr)
        fn = BASE_OPERATORS_MAP[op]
        q_fn = getattr(quirk_field, fn)

        stmt = stmt.filter(and_(
            category.codename == category_codename,
            q_fn(q_arg),
        ))

    stmt = stmt.distinct()
    stmt = stmt.order_by(asc(func.lower(User.fullname)))

    if selectinloaded:
        stmt = stmt.options(selectinload(*selectinloaded))

    stmt = stmt.offset(offset).limit(limit)
    return db_session.execute(stmt).scalars()


def query_groups(*, offset=None, limit=None, **params):
    stmt = select(Group, func.lower(Group.name))

    q_arg = params.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(Group.name).contains(func.lower(q_arg)),
            Group.codename.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Group,
        **params,
    ):
        stmt = stmt.filter(fltr)

    user, user_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='users__',
        alias=user,
        **params,
    ):
        if not user_join:
            user_join = True
            stmt = stmt.join(user, Group.users)
        stmt = stmt.filter(fltr)

    permission, permission_join = (aliased(Permission), False)
    for fltr in extract_filters(
        model=Permission,
        prefix='permissions__',
        alias=permission,
        **params,
    ):
        if not permission_join:
            permission_join = True
            stmt = stmt.join(permission, Group.permissions)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(asc(func.lower(Group.name)))

    stmt = stmt.offset(offset).limit(limit)
    return db_session.execute(stmt).scalars()


def query_permissions(*, offset=None, limit=None, **params):
    stmt = select(Permission, func.lower(Permission.name))

    q_arg = params.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(Permission.name).contains(func.lower(q_arg)),
            Permission.codename.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Permission,
        **params,
    ):
        stmt = stmt.filter(fltr)

    user, user_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='users__',
        alias=user,
        **params,
    ):
        if not user_join:
            user_join = True
            stmt = stmt.join(user, Permission.users)
        stmt = stmt.filter(fltr)

    group, group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='groups__',
        alias=group,
        **params,
    ):
        if not group_join:
            group_join = True
            stmt = stmt.join(group, Permission.groups)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(asc(func.lower(Permission.name)))

    stmt = stmt.offset(offset).limit(limit)
    return db_session.execute(stmt).scalars()


def query_categories(*, offset=None, limit=None, **params):
    stmt = select(Category, func.lower(Category.name))

    q_arg = params.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(Category.name).contains(func.lower(q_arg)),
            Category.codename.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Category,
        **params,
    ):
        stmt = stmt.filter(fltr)

    user, user_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='users__',
        alias=user,
        **params,
    ):
        if not user_join:
            user_join = True
            stmt = stmt.join(user, Category.users)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(asc(func.lower(Category.name)))

    stmt = stmt.offset(offset).limit(limit)
    return db_session.execute(stmt).scalars()


def query_userquirks(*, offset=None, limit=None, **params):
    stmt = select(UserQuirk, func.lower(UserQuirk.name))

    q_arg = params.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(UserQuirk.name).contains(func.lower(q_arg)),
            UserQuirk.codename.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=UserQuirk,
        **params,
    ):
        stmt = stmt.filter(fltr)

    user, user_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='users__',
        alias=user,
        **params,
    ):
        if not user_join:
            user_join = True
            stmt = stmt.join(user, UserQuirk.users)
        stmt = stmt.filter(fltr)

    category, category_join = (aliased(Category), False)
    for fltr in extract_filters(
        model=Category,
        prefix='category__',
        alias=category,
        **params,
    ):
        if not category_join:
            category_join = True
            stmt = stmt.join(category, UserQuirk.category)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(asc(func.lower(UserQuirk.name)))

    stmt = stmt.offset(offset).limit(limit)
    return db_session.execute(stmt).scalars()
