import typing

import cacahuate.database
from cacahuate.models.base import (
    ModelMixin,
)
from cacahuate.models.legacy import (
    Pointer,
    task_actor,
    task_candidate,
)

import case_conversion

import sqlalchemy
import sqlalchemy.sql
from sqlalchemy.orm import (
    Mapped,
    deferred,
    mapped_column,
    relationship,
    validates,
)
from sqlalchemy.schema import (
    UniqueConstraint,
)


user_permission: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'user_permission',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'user_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('user.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'permission_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('permission.id'),
        primary_key=True,
    ),
)


user_quirk: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'user_quirk',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'user_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('user.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'quirk_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('quirk.id'),
        primary_key=True,
    ),
)


group_permission: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'group_permission',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'group_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('group.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'permission_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('permission.id'),
        primary_key=True,
    ),
)


group_user: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'group_user',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'group_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('group.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'user_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('user.id'),
        primary_key=True,
    ),
)


class User(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'user'

    ''' those humans who can execute actions '''
    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
        index=True,
    )

    identifier: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
        unique=True,
        index=True,
    )
    fullname: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
    )
    email: Mapped[typing.Optional[str]] = mapped_column(
        sqlalchemy.String(80),
        nullable=True,
        default='',
    )

    permissions: Mapped[list['Permission']] = relationship(
        'Permission',
        secondary=user_permission,
        back_populates='users',
        lazy='dynamic',
    )

    quirks: Mapped[list['UserQuirk']] = relationship(
        'UserQuirk',
        secondary=user_quirk,
        back_populates='users',
        lazy='dynamic',
    )

    # Admin flags
    is_superuser: Mapped[bool] = mapped_column(
        sqlalchemy.Boolean(),
        nullable=False,
        default=False,
    )

    is_admin: Mapped[bool] = mapped_column(
        sqlalchemy.Boolean(),
        nullable=False,
        default=False,
    )

    is_staff: Mapped[bool] = mapped_column(
        sqlalchemy.Boolean(),
        nullable=False,
        default=False,
    )

    is_active: Mapped[bool] = mapped_column(
        sqlalchemy.Boolean(),
        nullable=False,
        default=True,
        server_default=sqlalchemy.sql.true(),
    )

    assigned_tasks: Mapped[list['cacahuate.models.legacy.Pointer']] = relationship(  # noqa: E501
        'cacahuate.models.legacy.Pointer',
        secondary=task_candidate,
        back_populates='candidates',
        lazy='dynamic',
    )
    pending_tasks: Mapped[list['cacahuate.models.legacy.Pointer']] = relationship(  # noqa: E501
        'cacahuate.models.legacy.Pointer',
        secondary=task_candidate,
        primaryjoin="User.id == task_candidate.c.user_id",
        secondaryjoin=sqlalchemy.and_(
            Pointer.id == task_candidate.c.pointer_id,
            Pointer.status == Pointer.Status.ONGOING,
        ),
        viewonly=True,
    )

    solved_tasks: Mapped[list['cacahuate.models.legacy.Pointer']] = relationship(  # noqa: E501
        'cacahuate.models.legacy.Pointer',
        secondary=task_actor,
        back_populates='actors',
        lazy='dynamic',
    )

    groups: Mapped[list['Group']] = relationship(
        'Group',
        secondary=group_user,
        back_populates='users',
        lazy='dynamic',
    )

    def has_perm(self, perm):
        if self.is_superuser:
            return True

        return perm in [p.codename for p in self.permissions]

    def has_perms(self, perm_list):
        for perm in perm_list:
            if not self.has_perm(perm):
                return False
        return True


class Group(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'group'

    '''Sets of User items'''
    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
        index=True,
    )

    name: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        nullable=False,
    )
    codename: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        nullable=False,
        unique=True,
        index=True,
    )

    permissions: Mapped[list['Permission']] = relationship(
        'Permission',
        secondary=group_permission,
        back_populates='groups',
        lazy='dynamic',
    )

    users: Mapped[list['User']] = relationship(
        'User',
        secondary=group_user,
        back_populates='groups',
        lazy='dynamic',
    )

    user_count: Mapped[int] = deferred(
        sqlalchemy.select(
            sqlalchemy.func.count(User.id),
        ).where(
          sqlalchemy.and_(
            group_user.c.group_id == id,
            group_user.c.user_id == User.id,
          ),
        ).scalar_subquery(),
    )


class Permission(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'permission'

    '''Model Permissions (django-like)'''
    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
    )

    name: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        nullable=False,
    )
    codename: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        nullable=False,
        unique=True,
    )

    users: Mapped[list['User']] = relationship(
        'User',
        secondary=user_permission,
        back_populates='permissions',
        lazy='dynamic',
    )

    groups: Mapped[list['Group']] = relationship(
        'Group',
        secondary=group_permission,
        back_populates='permissions',
        lazy='dynamic',
    )


class UserQuirk(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'quirk'
    __table_args__ = (
        UniqueConstraint(
            'category_id',
            'codename',
        ),
    )

    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
    )

    name: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        nullable=False,
    )
    codename: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        nullable=False,
    )

    category_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('category.id'),
    )
    category: Mapped['Category'] = relationship(
        'Category',
        back_populates='quirks',
    )

    users: Mapped[list['User']] = relationship(
        'User',
        secondary=user_quirk,
        back_populates='quirks',
        lazy='dynamic',
    )

    @validates('codename')
    def validate_codename(self, key, some_string) -> str:
        if len(some_string) < 1:
            raise ValueError('"codename" too short')

        as_snakecase = case_conversion.snakecase(some_string)
        if some_string not in [as_snakecase, as_snakecase.upper()]:
            raise ValueError('"codename" must be snakecase')
        return some_string


class Category(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'category'

    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
    )

    name: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        nullable=False,
    )
    codename: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        nullable=False,
        unique=True,
    )

    quirks: Mapped[list['UserQuirk']] = relationship(
        'UserQuirk',
        back_populates='category',
        lazy='dynamic',
        cascade='all, delete-orphan',
    )

    @validates('codename')
    def validate_codename(self, key, some_string) -> str:
        if len(some_string) < 1:
            raise ValueError('"codename" too short')

        if some_string == 'quirks':
            raise ValueError('"quirks" can\'t be a Category')

        as_snakecase = case_conversion.snakecase(some_string)
        if some_string not in [as_snakecase, as_snakecase.upper()]:
            raise ValueError('"codename" must be snakecase')
        return some_string


def clear_username(string):
    ''' because mongo usernames have special requirements '''
    string = string.strip()

    if string.startswith('$'):
        string = string[1:]

    try:
        string = string[:string.index('@')]
    except ValueError:
        pass

    return string.replace('.', '')


def get_or_create_user(identifier, data):
    identifier = clear_username(identifier)
    data['identifier'] = identifier

    user = cacahuate.database.db_session.query(User).filter(
        User.identifier == identifier,
    ).first()
    if user:
        return user
    else:
        user = User(**data)
        cacahuate.database.db_session.add(user)
        cacahuate.database.db_session.commit()
        return user
