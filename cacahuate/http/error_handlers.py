import http
import logging
import traceback

import cacahuate.errors
import cacahuate.http.errors
from cacahuate.database import db_session

import flask

import sqlalchemy.orm.exc


LOGGER = logging.getLogger(__name__)


def handle_permission_denied(e):
    return flask.make_response(
        flask.jsonify({
            'errors': [{
                'detail': 'Permission Denied',
            }],
        }),
        http.HTTPStatus.FORBIDDEN,
    )


def json_formatted_handler(e):
    return flask.jsonify(e.to_json()), e.status_code, e.headers


def handle_model_not_found(e):
    return flask.jsonify({
        'errors': [{
            'detail': str(e),
            'where': 'request.url',
        }],
    }), 404


def handle_integrity_error(e):
    db_session.rollback()
    return flask.jsonify({
        'errors': [{
            'detail': str(e),
        }],
    }), 400


def handle_input_error(e):
    return flask.jsonify({
        'errors': [e.to_json()],
    }), 400


def handler_auth_error(e):
    return flask.jsonify({
        'errors': [e.to_json()],
    }), 401


def handle_404(e):
    return flask.jsonify({
        'errors': [{
            'detail': e.description,
            'where': 'request.url',
        }],
    }), e.code


def handle_405(e):
    return flask.jsonify({
        'errors': [{
            'detail': e.description,
            'where': 'request.url',
        }],
    }), e.code


def handle_401(e):
    return flask.jsonify({
        'errors': [{
            'detail': str(e),
            'code': str(e),
            'where': 'request.authorization',
        }],
    }), e.code


def handle_500(e):
    LOGGER.error(traceback.format_exc())

    return flask.jsonify({
        'errors': [{
            'detail': str(e),
            'where': 'server',
        }],
    }), 500


def register_error_handlers(app):
    handlers = (
        (cacahuate.errors.PermissionDenied, handle_permission_denied),
        (cacahuate.http.errors.JsonReportedException, json_formatted_handler),
        (sqlalchemy.orm.exc.NoResultFound, handle_model_not_found),
        (sqlalchemy.exc.IntegrityError, handle_integrity_error),
        (cacahuate.errors.InputError, handle_input_error),
        (cacahuate.errors.AuthenticationError, handler_auth_error),
        (404, handle_404),
        (405, handle_405),
        (401, handle_401),
        (500, handle_500),
    )

    for (error, handler) in handlers:
        app.register_error_handler(error, handler)
