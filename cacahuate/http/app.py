import importlib
import logging.config
import os
import time

import cacahuate.database
import cacahuate.database.permissions
import cacahuate.http.middleware
import cacahuate.jsontypes
from cacahuate.http.error_handlers import register_error_handlers
from cacahuate.http.mongo import mongo
from cacahuate.indexes import create_indexes
from cacahuate.schema import schema
from cacahuate.tasks import app as celery

from flask import Flask
from flask.logging import default_handler

from flask_cors import CORS

from graphql_server.flask import GraphQLView


def shutdown_session(exception=None):
    cacahuate.database.db_session.remove()


def create_app():
    # The flask application
    app = Flask(__name__)
    app.config.from_object('cacahuate.settings')
    app.config.from_envvar('CACAHUATE_SETTINGS', silent=True)

    # Setup logging
    app.logger.removeHandler(default_handler)
    logging.config.dictConfig(app.config['LOGGING'])

    # Enalble cross origin
    CORS(app)

    # Timezone
    os.environ['TZ'] = app.config.get('TIMEZONE', 'UTC')
    time.tzset()

    # sql
    cacahuate.database.init_engine(
        app.config['SQLALCHEMY_DATABASE_URI'],
        connect_args={
            'connect_timeout': app.config['SQLALCHEMY_CONNECT_TIMEOUT'],
        },
    )
    cacahuate.database.permissions.create_initial_permissions()

    # The mongo database
    mongo.init_app(app)
    create_indexes(app.config)

    # Celery
    celery.conf.update(
        broker_url=app.config['CELERY_BROKER_URL'],
        task_default_queue=app.config['RABBIT_QUEUE'],
    )

    # Views
    blueprints = [
        'cacahuate.http.views.admin.categories.bp',
        'cacahuate.http.views.admin.groups.bp',
        'cacahuate.http.views.admin.permissions.bp',
        'cacahuate.http.views.admin.users.bp',
        'cacahuate.http.views.api.bp',
        'cacahuate.http.views.api.execution.bp',
        'cacahuate.http.views.api.pointer.bp',
        'cacahuate.http.views.auth.bp',
        'cacahuate.http.views.executions.bp',
        'cacahuate.http.views.pointers.bp',
        'cacahuate.http.views.processes.bp',
        'cacahuate.http.views.templates.bp',
        'cacahuate.http.views.workflow.workflow_definitions.bp',
    ]

    for bp in blueprints:
        app.register_blueprint(
            getattr(
              importlib.import_module(bp.rsplit('.', 1)[0]),
              bp.rsplit('.', 1)[1],
            ),
            url_prefix=app.config['URL_PREFIX'],
        )

    if app.config['ENABLE_GRAPHIQL']:
        graphql_view = GraphQLView.as_view(
            'graphql',
            schema=schema.graphql_schema,
            graphiql=False,
        )
        graphql_view = cacahuate.http.middleware.requires_auth(graphql_view)
        app.add_url_rule(
            '/graphql',
            view_func=graphql_view,
        )

    # Errors
    register_error_handlers(app)

    app.json_provider_class = cacahuate.jsontypes.JSONEncoder

    app.teardown_appcontext(shutdown_session)

    return app
