import re

from cacahuate.errors import (
    HierarchyError,
    InputError,
    MisconfiguredProvider,
    RequiredDictError,
    RequiredInputError,
    RequiredStrError,
)
from cacahuate.http.errors import BadRequest
from cacahuate.http.errors import Forbidden
from cacahuate.imports import user_import
from cacahuate.xml import get_element_by
from cacahuate.xml.inputs import make_input
from cacahuate.xml.node import resolve_params

import flask


def validate_json(json_data: dict, req: list):
    errors = []

    for item in req:
        if item not in json_data:
            errors.append(RequiredInputError(
                item,
                'request.body.{}'.format(item),
            ).to_json())

    if errors:
        raise BadRequest(errors)


def validate_auth(node, user, state=None):
    if not node.auth_backend:
        return

    try:
        hi_pro_cls = user_import(
            node.auth_backend,
            'HierarchyProvider',
            flask.current_app.config['CUSTOM_HIERARCHY_PROVIDERS'],
            'cacahuate.auth.hierarchy',
            flask.current_app.config['ENABLED_HIERARCHY_PROVIDERS'],
        )
    except MisconfiguredProvider:
        flask.abort(500, 'Misconfigured hierarchy provider, sorry')

    hipro = hi_pro_cls(flask.current_app.config)

    try:
        hipro.validate_user(
            user,
            **resolve_params(
                node.auth_params,
                state,
                flask.current_app.config,
            ),
        )
    except HierarchyError:
        raise Forbidden([{
            'detail': 'The provided credentials do not match the specified'
                      ' hierarchy',
            'where': 'request.authorization',
        }])


def validate_patch_inputs(inputs, state, xml, only_action=True):
    dom = xml.get_dom()

    processed_inputs = []
    for i, field in enumerate(inputs):
        if type(field) is not dict:
            raise RequiredDictError(str(i), 'request.body.inputs.{}'.format(i))

        if 'ref' not in field:
            raise RequiredInputError('id',
                                     'request.body.inputs.{}.ref'.format(i))

        if type(field['ref']) is not str:
            raise RequiredStrError('ref',
                                   'request.body.inputs.{}.ref'.format(i))

        # check down the state tree for existence of the requested ref
        processed_ref = []
        pieces = field['ref'].split('.')

        try:
            node_id = pieces.pop(0)
            node_state = state['state']['items'][node_id]
        except IndexError:
            raise InputError(
                'Missing segment in ref for node_id',
                'request.body.inputs.{}.ref'.format(i),
                'validation.invalid')
        except KeyError:
            raise InputError(
                'node {} not found'.format(node_id),
                'request.body.inputs.{}.ref'.format(i),
                'validation.invalid')

        if only_action is True and node_state['type'] != 'action':
            raise InputError(
                'only action nodes may be patched',
                'request.body.inputs.{}.ref'.format(i),
                'validation.invalid')

        processed_ref.append(node_id)

        # node xml element
        node = get_element_by(dom, 'action', 'id', node_id)

        if len(node_state['actors']['items']) == 1:
            only_key = list(node_state['actors']['items'].keys())[0]
            actor_state = node_state['actors']['items'][only_key]
        else:
            try:
                actor_username = pieces.pop(0)
                actor_state = node_state['actors']['items'][actor_username]
            except IndexError:
                raise InputError(
                    'Missing segment in ref for actor username',
                    'request.body.inputs.{}.ref'.format(i),
                    'validation.invalid')
            except KeyError:
                raise InputError(
                    'actor {} not found'.format(actor_username),
                    'request.body.inputs.{}.ref'.format(i),
                    'validation.invalid')

        processed_ref.append(actor_state['user']['identifier'])

        try:
            form_ref = pieces.pop(0)
        except IndexError:
            raise InputError(
                'Missing segment in ref for form ref',
                'request.body.inputs.{}.ref'.format(i),
                'validation.invalid')

        if re.match(r'\d+', form_ref):
            try:
                form_index = int(form_ref)
                form_state = actor_state['forms'][form_index]
            except KeyError:
                raise InputError(
                    'form index {} not found'.format(form_ref),
                    'request.body.inputs.{}.ref'.format(i),
                    'validation.invalid')
        else:
            matching_forms = [
                f['ref'] == form_ref
                for f in actor_state['forms']
            ]
            form_count = len(list(filter(lambda x: x, matching_forms)))

            if form_count == 0:
                continue

            if form_count == 1:
                form_index = matching_forms.index(True)
                form_state = actor_state['forms'][form_index]
            elif len(pieces) and re.match(r'\d+', pieces[0]):
                try:
                    form_n = int(pieces.pop(0))
                    # get index of the n-th matched form
                    form_index = list(filter(
                        lambda x: x[1],
                        enumerate(matching_forms),
                    ))[form_n][0]  # raise error if there is no n-th form
                    form_state = actor_state['forms'][form_index]
                except IndexError:
                    raise InputError(
                        'form index {} not found'.format(form_ref),
                        'request.body.inputs.{}.ref'.format(i),
                        'validation.invalid')
            else:
                raise InputError(
                    'More than one form with ref {}'.format(form_ref),
                    'request.body.inputs.{}.ref'.format(i),
                    'validation.invalid',
                )

        processed_ref.append(str(form_index) + ':' + form_state['ref'])

        # form xml element
        form = get_element_by(node, 'form', 'id', form_state['ref'])

        try:
            input_name = pieces.pop(0)
            form_state['inputs']['items'][input_name]
        except IndexError:
            raise InputError(
                'Missing segment in ref for input name',
                'request.body.inputs.{}.ref'.format(i),
                'validation.invalid',
            )
        except KeyError:
            continue

        processed_ref.append(input_name)

        processed_inputs.append({
            'ref': '.'.join(processed_ref),
        })

        # input xml element
        input_el = get_element_by(form, 'input', 'name', input_name)

        if 'value' in field:
            try:
                input_obj = make_input(input_el)
                value = input_obj.validate(field['value'], 0)
                caption = input_obj.make_caption(value)

                processed_inputs[-1]['value'] = value
                processed_inputs[-1]['value_caption'] = caption
            except InputError as e:
                raise InputError(
                    'value invalid: {}'.format(str(e)),
                    'request.body.inputs.{}.value'.format(i),
                    'validation.invalid')

    return processed_inputs
