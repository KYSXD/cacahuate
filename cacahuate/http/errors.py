class JsonReportedException(Exception):

    def __init__(self, errors, headers=None):
        self.errors = errors
        self.headers = headers if headers else {}

    def to_json(self):
        return {'errors': self.errors}


class BadRequest(JsonReportedException):
    status_code = 400


class Unauthorized(JsonReportedException):
    status_code = 401


class Forbidden(JsonReportedException):
    status_code = 403


class NotFound(JsonReportedException):
    status_code = 404


class UnprocessableEntity(JsonReportedException):
    status_code = 422
