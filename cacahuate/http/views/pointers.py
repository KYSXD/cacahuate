import collections
import dataclasses
import http
import logging
import os
import xml

from cacahuate.database import db_session
from cacahuate.database.query import (
    query_tasks,
    query_userquirks,
    query_users,
)
from cacahuate.handler.legacy import unpack_pointer_values
from cacahuate.http.middleware import (
    pagination,
    permission_required,
    requires_auth,
)
from cacahuate.http.mongo import mongo
from cacahuate.models import (
    Pointer,
)
from cacahuate.xml.parser import parse_xml
from cacahuate.xml.translator import translate_process

from dateutil.parser import parse

import flask


LOGGER = logging.getLogger(__name__)


bp = flask.Blueprint('cacahuate_pointers', __name__)


@bp.route('/pointers', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_pointer',
])
@pagination
def get_pointers():
    params = flask.request.args.to_dict()

    payload = {}

    mapper = {
        'search_query': str,
        'id__eq': str,
        'id__in': lambda x: str(x).split(','),
        'node_id__contains': str,
        'node_id__eq': str,
        'node_id__in': lambda x: str(x).split(','),
        'node_id__startswith': str,
        'node_type__contains': str,
        'node_type__eq': str,
        'node_type__in': lambda x: str(x).split(','),
        'node_type__startswith': str,
        'started_at__gt': parse,
        'started_at__lt': parse,
        'started_at__date__ge': parse,
        'started_at__date__gt': parse,
        'started_at__date__le': parse,
        'started_at__date__lt': parse,
        'finished_at__gt': parse,
        'finished_at__lt': parse,
        'status__eq': str,
        'status__in': lambda x: str(x).split(','),
        'execution__id__eq': str,
        'execution__id__in': lambda x: str(x).split(','),
        'execution__process_name__contains': str,
        'execution__process_name__eq': str,
        'execution__process_name__in': lambda x: str(x).split(','),
        'execution__process_name__startswith': str,
        'execution__status__eq': str,
        'execution__status__in': lambda x: str(x).split(','),
        'actors__identifier__eq': str,
        'actors__identifier__in': lambda x: str(x).split(','),
        'actors__groups__codename__eq': str,
        'actors__groups__codename__in': lambda x: str(x).split(','),
        'candidates__identifier__eq': str,
        'candidates__identifier__in': lambda x: str(x).split(','),
        'candidates__groups__codename__eq': str,
        'candidates__groups__codename__in': lambda x: str(x).split(','),
    }

    for key, fn in mapper.items():
        if params.get(key) is not None:
            payload[key] = fn(params[key])

    query = query_tasks(
        offset=flask.g.offset,
        limit=flask.g.limit,
        **payload,
    )

    return flask.make_response(
        flask.jsonify({
            'items': [
                {
                    **x.as_json(),
                    'execution_name': x.execution.name,
                    'execution': {
                        'finished_at': x.execution.finished_at,
                        'id': x.execution.id,
                        'name': x.execution.name,
                        'started_at': x.execution.started_at,
                        'status': x.execution.status,
                    },
                    'candidates_usernames': [
                        candidate.identifier for candidate in x.candidates
                    ],
                    'actors_usernames': [
                        actor.identifier for actor in x.actors
                    ],
                }
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route('/pointer/<pk>', methods=['GET'])
@bp.route('/pointers/<pk>', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_pointer',
])
def view_pointer(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
    ).one()

    collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]

    doc = collection.find_one({'id': pk})
    doc['values'] = unpack_pointer_values(doc)

    return flask.make_response(
        flask.jsonify({
            **pointer.as_json(),
            'data': doc or None,
        }),
        http.HTTPStatus.OK,
    )


@bp.route('/pointers/<pk>/candidates', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_pointer',
])
def view_pointer_candidates(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'items': [
                user.as_json()
                for user in pointer.candidates
            ],
        }),
        http.HTTPStatus.OK,
    )


@bp.route('/pointers/<pk>/actors', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_pointer',
])
def view_pointer_actors(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'items': [
                user.as_json()
                for user in pointer.actors
            ],
        }),
        http.HTTPStatus.OK,
    )


@bp.route('/pointers/<pk>/forms', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_pointer',
])
def get_pointer_forms(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
    ).one()

    xml_path = os.path.join(
        flask.current_app.config['XML_PATH'],
        pointer.execution.process_name,
    )
    identifier, version = pointer.execution.process_name.split('.')[:2]
    with open(xml_path) as f:
        parsed = parse_xml(xml.dom.pulldom.parse(f))
    process_schema = translate_process(parsed, identifier, version)

    forms_schema = process_schema.fieldsets
    task_fields = process_schema.tasks[pointer.node_id].fields

    forms = collections.OrderedDict()
    for (fieldset_id, field_id), permission in (
        (task_field.reference.split('.'), task_field.permission)
        for task_field in task_fields
    ):
        field = process_schema.fieldsets[fieldset_id].fields[field_id]
        forms.setdefault(
            fieldset_id,
            {
                'id': forms_schema[fieldset_id].id,
                'min_amount': forms_schema[fieldset_id].min_amount,
                'max_amount': forms_schema[fieldset_id].max_amount,
                'fields': [],
                'values': [],
            },
        )['fields'].append({
            **dataclasses.asdict(field),
            'permission': permission,
        })

    collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]
    document = collection.find_one({'id': pk})
    if 'patch' not in document:
        values = unpack_pointer_values(document)
    else:
        values = []

    related_values = {}
    for value_item in sorted(
        values,
        key=lambda item: item['form_index'],
        reverse=True,
    ):
        value_entry = value_item['value']
        fieldset_index = value_item['form_index']
        fieldset_ref = value_item['form_id']
        field_ref = value_item['field_id']
        reference = f'{fieldset_ref}.{field_ref}'
        if reference not in (
            task_field.reference
            for task_field in task_fields
        ):
            LOGGER.error(f'Non existant reference: {reference}')
            continue

        related_values.setdefault(
            fieldset_ref,
            [{}] * (fieldset_index + 1),
        )[fieldset_index][field_ref] = value_entry

    for fieldset_ref, values in related_values.items():
        forms[fieldset_ref]['values'] = values

    return flask.make_response(
        flask.jsonify(
            list(forms.values()),
        ),
        http.HTTPStatus.OK,
    )


@bp.route('/pointers/<pk>/forms/<field_reference>/choices', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_pointer',
])
def get_pointer_form_choices(pk, field_reference):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
    ).one()

    xml_path = os.path.join(
        flask.current_app.config['XML_PATH'],
        pointer.execution.process_name,
    )
    identifier, version = pointer.execution.process_name.split('.')[:2]
    with open(xml_path) as f:
        parsed = parse_xml(xml.dom.pulldom.parse(f))
    process_schema = translate_process(parsed, identifier, version)

    task_fields = process_schema.tasks[pointer.node_id].fields

    if field_reference not in (
        task_field.reference
        for task_field in task_fields
    ):
        flask.abort(404)

    fieldset_id, field_id = field_reference.split('.')
    field = process_schema.fieldsets[fieldset_id].fields[field_id]

    if field.type == 'foreignkey':
        attributes = {
            item['key']: item['value']
            for item in field.modifiers
        }

        filters = {
            key.removeprefix('filter:'): value
            for key, value in attributes.items()
            if key.startswith('filter:')
        }

        model_solvers = {
            'quirk': query_userquirks,
            'user': query_users,
        }
        model_name = attributes['model']

        fixed_choices = field.choices or []
        model_choices = (
            {
                'value': getattr(match, attributes['value_path']),
                'label': getattr(match, attributes['label_path']),
            }
            for match in model_solvers[model_name](**filters)
        )

        return flask.make_response(
            flask.jsonify(
                fixed_choices + list(model_choices),
            ),
            http.HTTPStatus.OK,
        )

    return flask.make_response(
        flask.jsonify(
            field.choices,
        ),
        http.HTTPStatus.OK,
    )
