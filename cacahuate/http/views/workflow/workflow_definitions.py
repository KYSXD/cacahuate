import http

import cacahuate.errors
import cacahuate.http.middleware
import cacahuate.http.validation
from cacahuate.database import db_session
from cacahuate.models import (
    Field,
    FieldsetDefinition,
    TaskDefinition,
    TaskField,
    WorkflowDefinition,
)

import flask

bp = flask.Blueprint('cacahuate_definitions', __name__)


@bp.route(
    '/workflow-definitions',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_workflowdefinition',
])
def view_workflowdefinitions():
    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in db_session.query(WorkflowDefinition)
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route(
    '/workflow-definitions',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.add_workflowdefinition',
])
def add_workflowdefinition():
    cacahuate.http.validation.validate_json(
        flask.request.json,
        WorkflowDefinition.get_fields(ignore=['id']),
    )

    workflow = WorkflowDefinition(
        **flask.request.json,
    )

    db_session.add(workflow)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': workflow.identifier,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
])
def set_workflowdefinition(wf_pk):
    cacahuate.http.validation.validate_json(
        flask.request.json,
        WorkflowDefinition.get_fields(ignore=['id']),
    )

    workflow = db_session.query(WorkflowDefinition).filter(
        WorkflowDefinition.id == wf_pk,
    ).first()

    if workflow is None:
        workflow = WorkflowDefinition(**{
            **flask.request.json,
            'id': wf_pk,
        })
        db_session.add(workflow)
    else:
        for k, v in {
            **flask.request.json,
            'id': wf_pk,
        }.items():
            setattr(workflow, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': workflow.id,
            'identifier': workflow.identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


@bp.route(
    '/workflow-definitions/<wf_pk>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.delete_workflowdefinition',
])
def delete_workflowdefinition(wf_pk):
    workflow = db_session.query(WorkflowDefinition).filter(
        WorkflowDefinition.id == wf_pk,
    ).one()

    db_session.delete(workflow)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': workflow.id,
            'identifier': workflow.identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_workflowdefinition',
    'auth.view_taskdefinition',
])
def view_workflowdefinition_tasks(wf_pk):
    workflow = db_session.query(WorkflowDefinition).filter(
        WorkflowDefinition.id == wf_pk,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in workflow.tasks
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.add_taskdefinition',
])
def add_workflowdefinition_task(wf_pk):
    workflow = db_session.query(WorkflowDefinition).filter(
        WorkflowDefinition.id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        TaskDefinition.get_fields(ignore=['id', 'workflow_id']),
    )

    task = TaskDefinition(**{
        **flask.request.json,
        'workflow': workflow,
    })

    db_session.add(task)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'identifier': task.identifier,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_taskdefinition',
])
def set_workflowdefinition_task(wf_pk, t_pk):
    db_session.query(WorkflowDefinition).filter(
        WorkflowDefinition.id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        TaskDefinition.get_fields(ignore=['id', 'workflow_id']),
    )

    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).first()

    if task is None:
        task = TaskDefinition(**{
            **flask.request.json,
            'id': t_pk,
            'workflow_id': wf_pk,
        })
        db_session.add(task)
    else:
        for k, v in {
            **flask.request.json,
            'id': t_pk,
            'workflow_id': wf_pk,
        }.items():
            setattr(task, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'identifier': task.identifier,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.delete_taskdefinition',
])
def delete_workflowdefinition_task(wf_pk, t_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    db_session.delete(task)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'identifier': task.identifier,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.OK,  # 200
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/preceding-tasks',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_workflowdefinition',
    'auth.view_taskdefinition',
])
def view_workflowdefinition_task_precedingtasks(wf_pk, t_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in task.preceding_tasks
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/preceding-tasks',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_taskdefinition',
])
def add_workflowdefinition_task_precedingtask(wf_pk, t_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['id'],
    )

    preceding_task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == flask.request.json['id'],
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    task.preceding_tasks.append(preceding_task)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'identifier': task.identifier,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/preceding-tasks/<pt_pk>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_taskdefinition',
])
def delete_workflowdefinition_task_precedingtask(wf_pk, t_pk, pt_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    predecessor = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == pt_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    task.preceding_tasks.remove(predecessor)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'identifier': task.identifier,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/rollback-targets',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_workflowdefinition',
    'auth.view_taskdefinition',
])
def view_workflowdefinition_task_rollbacktargets(wf_pk, t_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in task.rollback_targets
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/rollback-targets',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_taskdefinition',
])
def add_workflowdefinition_task_rollbacktarget(wf_pk, t_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['id'],
    )

    target = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == flask.request.json['id'],
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    task.rollback_targets.append(target)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'identifier': task.identifier,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/rollback-targets/<rbt_pk>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_taskdefinition',
])
def delete_workflowdefinition_task_rollbacktarget(wf_pk, t_pk, rbt_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    target = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == rbt_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    task.rollback_targets.remove(target)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'identifier': task.identifier,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/fieldset-definitions',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_workflowdefinition',
    'auth.view_fieldsetdefinition',
])
def view_workflowdefinition_fieldsetdefinitionss(wf_pk):
    workflow = db_session.query(WorkflowDefinition).filter(
        WorkflowDefinition.id == wf_pk,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in workflow.fieldsets
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/fieldset-definitions',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.add_fieldsetdefinition',
])
def add_workflowdefinition_fieldset(wf_pk):
    workflow = db_session.query(WorkflowDefinition).filter(
        WorkflowDefinition.id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        FieldsetDefinition.get_fields(ignore=['id', 'workflow_id']),
    )

    fieldset = FieldsetDefinition(**{
        **flask.request.json,
        'workflow': workflow,
    })

    db_session.add(fieldset)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': fieldset.id,
            'identifier': fieldset.identifier,
            'workflow_id': fieldset.workflow_id,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/fieldset-definitions/<fs_pk>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_fieldsetdefinition',
])
def set_workflowdefinition_fieldset(wf_pk, fs_pk):
    db_session.query(WorkflowDefinition).filter(
        WorkflowDefinition.id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        FieldsetDefinition.get_fields(ignore=['id', 'workflow_id']),
    )

    fieldset = db_session.query(FieldsetDefinition).filter(
        FieldsetDefinition.id == fs_pk,
        FieldsetDefinition.workflow_id == wf_pk,
    ).first()

    if fieldset is None:
        fieldset = FieldsetDefinition(**{
            **flask.request.json,
            'id': fs_pk,
            'workflow_id': wf_pk,
        })
        db_session.add(fieldset)
    else:
        for k, v in {
            **flask.request.json,
            'id': fs_pk,
            'workflow_id': wf_pk,
        }.items():
            setattr(fieldset, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': fieldset.id,
            'identifier': fieldset.identifier,
            'workflow_id': fieldset.workflow_id,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/fieldset-definitions/<fs_pk>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.delete_fieldsetdefinition',
])
def delete_workflowdefinition_fieldset(wf_pk, fs_pk):
    fieldset = db_session.query(FieldsetDefinition).filter(
        FieldsetDefinition.id == fs_pk,
        FieldsetDefinition.workflow_id == wf_pk,
    ).one()

    db_session.delete(fieldset)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': fieldset.id,
            'identifier': fieldset.identifier,
            'workflow_id': fieldset.workflow_id,
        }),
        http.HTTPStatus.OK,  # 200
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/fieldset-definitions/<fs_pk>/fields',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_workflowdefinition',
    'auth.view_fieldsetdefinition',
    'auth.view_field',
])
def view_workflowdefinitions_fieldsetdefinition_fields(wf_pk, fs_pk):
    fieldset = db_session.query(FieldsetDefinition).filter(
        FieldsetDefinition.id == fs_pk,
        FieldsetDefinition.workflow_id == wf_pk,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in fieldset.fields
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/fieldset-definitions/<fs_pk>/fields',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_fieldsetdefinition',
    'auth.add_field',
])
def add_workflowdefinition_fieldsetdefinition_field(wf_pk, fs_pk):
    fieldset = db_session.query(FieldsetDefinition).filter(
        FieldsetDefinition.id == fs_pk,
        FieldsetDefinition.workflow_id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        Field.get_fields(ignore=['id', 'fieldset_id']),
    )

    field = Field(**{
        **flask.request.json,
        'fieldset': fieldset,
    })

    db_session.add(field)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': field.id,
            'identifier': field.identifier,
            'fieldset_id': field.fieldset_id,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/fieldset-definitions/<fs_pk>/fields/<f_pk>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_fieldsetdefinition',
    'auth.change_field',
])
def set_workflowdefinition_fieldsetdefinition_field(wf_pk, fs_pk, f_pk):
    db_session.query(FieldsetDefinition).filter(
        FieldsetDefinition.id == fs_pk,
        FieldsetDefinition.workflow_id == wf_pk,
    ).first()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        Field.get_fields(ignore=['id', 'fieldset_id']),
    )

    field = db_session.query(Field).filter(
        Field.id == f_pk,
        Field.fieldset_id == fs_pk,
    ).first()

    if field is None:
        field = Field(**{
            **flask.request.json,
            'id': f_pk,
            'fieldset_id': fs_pk,
        })
        db_session.add(field)
    else:
        for k, v in {
            **flask.request.json,
            'id': f_pk,
            'fieldset_id': fs_pk,
        }.items():
            setattr(field, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': field.id,
            'identifier': field.identifier,
            'fieldset_id': field.fieldset_id,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/fieldset-definitions/<fs_pk>/fields/<f_pk>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_fieldsetdefinition',
    'auth.delete_field',
])
def delete_workflowdefinition_fieldsetdefinition_field(wf_pk, fs_pk, f_pk):
    fieldset = db_session.query(FieldsetDefinition).filter(
        FieldsetDefinition.id == fs_pk,
        FieldsetDefinition.workflow_id == wf_pk,
    ).one()

    field = db_session.query(Field).filter(
        Field.id == f_pk,
        Field.fieldset_id == fs_pk,
    ).one()

    fieldset.fields.remove(field)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': field.id,
            'identifier': field.identifier,
            'fieldset_id': field.fieldset_id,
        }),
        http.HTTPStatus.OK,  # 200
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/fields',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_workflowdefinition',
    'auth.view_taskdefinition',
    'auth.view_field',
])
def view_workflowdefinitions_task_fields(wf_pk, t_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in task.fields
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/fields',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_taskdefinition',
    'auth.add_field',
])
def add_workflowdefinition_task_field(wf_pk, t_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        TaskField.get_fields(ignore=['id', 'task_id']),
    )

    field = TaskField(**{
        **flask.request.json,
        'task': task,
    })

    db_session.add(field)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/fields/<tf_pk>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_taskdefinition',
    'auth.change_field',
])
def set_workflowdefinition_task_field(wf_pk, t_pk, tf_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        TaskField.get_fields(ignore=['id', 'task_id']),
    )

    field = db_session.query(TaskField).filter(
        TaskField.id == tf_pk,
        TaskField.task_id == t_pk,
    ).first()

    if field is None:
        field = TaskField(**{
            **flask.request.json,
            'id': tf_pk,
            'task_id': t_pk,
        })
        db_session.add(field)
    else:
        for k, v in {
            **flask.request.json,
            'id': tf_pk,
            'task_id': t_pk,
        }.items():
            setattr(field, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': task.id,
            'workflow_id': task.workflow_id,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


@bp.route(
    '/workflow-definitions/<wf_pk>/tasks/<t_pk>/fields/<tf_pk>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_workflowdefinition',
    'auth.change_taskdefinition',
    'auth.delete_field',
])
def delete_workflowdefinition_task_field(wf_pk, t_pk, tf_pk):
    task = db_session.query(TaskDefinition).filter(
        TaskDefinition.id == t_pk,
        TaskDefinition.workflow_id == wf_pk,
    ).one()

    field = db_session.query(TaskField).filter(
        TaskField.id == tf_pk,
        TaskField.task_id == t_pk,
    ).one()

    task.fields.remove(field)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'id': field.id,
            'identifier': field.identifier,
            'fieldset_id': field.fieldset_id,
        }),
        http.HTTPStatus.OK,  # 200
    )
