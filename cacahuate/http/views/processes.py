import http
import os

import cacahuate.http.middleware
from cacahuate.database import db_session
from cacahuate.database.query import query_processes
from cacahuate.errors import (
    ProcessNotFound,
)
from cacahuate.http.errors import (
    NotFound,
)
from cacahuate.http.mongo import mongo
from cacahuate.models import ProcessDefinition
from cacahuate.xml import (
    Xml,
    form_to_dict,
)

import flask
from flask import g
from flask import jsonify
from flask import request

from sqlalchemy import select


bp = flask.Blueprint('process', __name__)


def bool_or_value(value):
    if value in ['True', 'true', '1', 1]:
        return True
    if value in ['False', 'false', '0', 0]:
        return False
    return value


@bp.route(
    '/processes',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_processdefinition',
])
@cacahuate.http.middleware.pagination
def view_processes():
    params = flask.request.args.to_dict()

    payload = {}

    mapper = {
        'search_query': str,
        'identifier__eq': str,
        'identifier__in': lambda x: str(x).split(','),
        'is_public__eq': bool_or_value,
        'name__contains': str,
        'name__eq': str,
        'name__in': lambda x: str(x).split(','),
        'name__startswith': str,
        'version__contains': str,
        'version__eq': str,
        'version__in': lambda x: str(x).split(','),
        'version__startswith': str,
    }

    for key, fn in mapper.items():
        if params.get(key) is not None:
            payload[key] = fn(params[key])

    query = query_processes(
        offset=flask.g.offset,
        limit=flask.g.limit,
        **payload,
    )

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,
    )


@bp.route(
    '/processes/<identifier_version>',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_processdefinition',
])
def view_process(identifier_version):
    try:
        identifier, version = identifier_version.split('.')
    except ValueError:
        flask.abort(404)

    stmt = select(ProcessDefinition).filter(
        ProcessDefinition.identifier == identifier,
        ProcessDefinition.version == version,
    )
    process = db_session.execute(stmt).scalar_one()

    return flask.make_response(
        flask.jsonify({
            'identifier': process.identifier,
            'version': process.version,
            'is_public': process.is_public,
            'name': process.name,
            'name_template': process.name_template,
            'description': process.description,
            'description_template': process.description_template,
        }),
        http.HTTPStatus.OK,  # 200
    )


@bp.route(
    '/processes/<identifier_version>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_processdefinition',
])
def set_process(identifier_version):
    try:
        identifier, version = identifier_version.split('.')
    except ValueError:
        flask.abort(404)

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ProcessDefinition.get_fields(ignore=['id', 'identifier', 'version']),
    )

    stmt = select(ProcessDefinition).filter(
        ProcessDefinition.identifier == identifier,
        ProcessDefinition.version == version,
    )
    process = db_session.execute(stmt).scalar_one_or_none()

    if process is None:
        process = ProcessDefinition(**{
            **flask.request.json,
            'identifier': identifier,
            'version': version,
        })
        db_session.add(process)
    else:
        for k, v in {
            **flask.request.json,
            'identifier': identifier,
            'version': version,
        }.items():
            setattr(process, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': process.identifier,
            'version': process.version,
            'is_public': process.is_public,
            'name': process.name,
            'name_template': process.name_template,
            'description': process.description,
            'description_template': process.description_template,
        }),
        http.HTTPStatus.OK,  # 200
    )


@bp.route('/process', methods=['GET'])
@cacahuate.http.middleware.requires_auth
def list_process():
    return jsonify({
        'data': [
            process.to_json()
            for process in Xml.processes(flask.current_app.config)
            if process
        ],
    })


@bp.route('/process/<name>', methods=['GET'])
@cacahuate.http.middleware.requires_auth
def find_process(name):
    def add_form(xml):
        json_xml = xml.to_json()
        forms = []
        xmliter = iter(xml)
        first_node = next(xmliter)
        xmliter.parser.expandNode(first_node)

        for form in first_node.getElementsByTagName('form'):
            forms.append(form_to_dict(form))

        json_xml['form_array'] = forms

        return json_xml

    version = request.args.get('version', '')

    if version:
        version = ".{}".format(version)

    process_name = "{}{}".format(name, version)

    try:
        xml = Xml.load(flask.current_app.config, process_name)
    except ProcessNotFound:
        raise NotFound([{
            'detail': '{} process does not exist'
                      .format(process_name),
            'where': 'request.body.process_name',
        }])

    return jsonify({
        'data': add_form(xml),
    })


@bp.route('/process/<name>.xml', methods=['GET'])
@cacahuate.http.middleware.requires_auth
def xml_process(name):
    version = request.args.get('version', '')

    if version:
        version = ".{}".format(version)

    process_name = "{}{}".format(name, version)

    try:
        xml = Xml.load(flask.current_app.config, process_name)
    except ProcessNotFound:
        raise NotFound([{
            'detail': '{} process does not exist'
                      .format(process_name),
            'where': 'request.body.process_name',
        }])
    ruta = os.path.join(flask.current_app.config['XML_PATH'], xml.filename)
    return open(ruta).read(), {'Content-Type': 'text/xml; charset=utf-8'}


@bp.route('/process/<pk>/statistics', methods=['GET'])
@cacahuate.http.middleware.requires_auth
def node_statistics(pk):
    collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]
    query = [
        {"$match": {"process_id": pk}},
        {"$project": {
            "process_id": "$process_id",
            "node": "$node.id",
            "difference_time": {
                "$subtract": ["$finished_at", "$started_at"],
            },
        }},
        {"$group": {
            "_id": {"process_id": "$process_id", "node": "$node"},
            "process_id": {"$first": "$process_id"},
            "node": {"$first": "$node"},
            "max": {
                "$max": {
                    "$divide": ["$difference_time", 1000],
                },
            },
            "min": {
                "$min": {
                    "$divide": ["$difference_time", 1000],
                },
            },
            "average": {
                "$avg": {
                    "$divide": ["$difference_time", 1000],
                },
            },
        }},
        {"$sort": {"execution": 1, "node": 1}},
        {"$project": {"_id": False}},
    ]
    return jsonify({
        "data": list(collection.aggregate(query)),
    })


@bp.route('/process/statistics', methods=['GET'])
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.pagination
def process_statistics():
    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]
    query = [
        {"$match": {"status": "finished"}},
        {"$skip": g.offset},
        {"$limit": g.limit},
        {"$project": {
            "difference_time": {
                "$subtract": ["$finished_at", "$started_at"],
            },
            "process": {"id": "$process.id"},
        }},

        {"$group": {
            "_id": "$process.id",
            "process": {"$first": "$process.id"},
            "max": {
                "$max": {
                    "$divide": ["$difference_time", 1000],
                },
            },
            "min": {
                "$min": {
                    "$divide": ["$difference_time", 1000],
                },
            },
            "average": {
                "$avg": {
                    "$divide": ["$difference_time", 1000],
                },
            },

        }},
        {"$sort": {"process": 1}},
        {"$project": {"_id": False}},
    ]

    return jsonify({
        "data": list(collection.aggregate(query)),
    })
