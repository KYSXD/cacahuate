import http

import cacahuate.errors
import cacahuate.http.middleware
import cacahuate.http.validation
from cacahuate.database import db_session
from cacahuate.database.query import (
    query_categories,
    query_userquirks,
)
from cacahuate.models import (
    Category,
    UserQuirk,
)

import flask


bp = flask.Blueprint('cacahuate_categories', __name__)


# TODO: Delete legacy url
@bp.route(
    '/category',
    methods=['GET'],
)
@bp.route(
    '/categories',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_category',
])
@cacahuate.http.middleware.pagination
def view_categories():
    params = flask.request.args.to_dict()

    payload = {}

    mapper = {
        'search_query': str,
        'codename__eq': str,
        'codename__in': lambda x: str(x).split(','),
    }

    for key, fn in mapper.items():
        if params.get(key) is not None:
            payload[key] = fn(params[key])

    query = query_categories(
        offset=flask.g.offset,
        limit=flask.g.limit,
        **payload,
    )

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/category',
    methods=['POST'],
)
@bp.route(
    '/categories',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.add_category',
])
def add_category():
    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename', 'name', 'quirks'],
    )

    category = Category(**{
        **flask.request.json,
        'quirks': [
            UserQuirk(**x)
            for x in flask.request.json['quirks']
        ],
    })

    db_session.add(category)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': category.codename,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/category/<codename>',
    methods=['PUT'],
)
@bp.route(
    '/categories/<codename>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_category',
])
def set_category(codename):
    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename', 'name', 'quirks'],
    )

    category = db_session.query(Category).filter(
        Category.codename == codename,
    ).first()

    if category is None:
        category = Category(**{
            **flask.request.json,
            'codename': codename,
            'quirks': [
                UserQuirk(**x)
                for x in flask.request.json['quirks']
            ],
        })
        db_session.add(category)
    else:
        for k, v in {
            **flask.request.json,
            'codename': codename,
            'quirks': [
                UserQuirk(**x)
                for x in flask.request.json['quirks']
            ],
        }.items():
            setattr(category, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': category.codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route('/category/<codename>', methods=['GET'])
@bp.route('/categories/<codename>', methods=['GET'])
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_category',
])
def view_category(codename):
    category = db_session.query(Category).filter(
        Category.codename == codename,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'codename': category.codename,
            'name': category.name,
        }),
        http.HTTPStatus.OK,  # 200
    )


# TODO: Delete legacy url
@bp.route(
    '/category/<codename>',
    methods=['DELETE'],
)
@bp.route(
    '/categories/<codename>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.delete_category',
])
def delete_category(codename):
    category = db_session.query(Category).filter(
        Category.codename == codename,
    ).one()

    db_session.delete(category)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': category.codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/category/<codename>/quirks',
    methods=['GET'],
)
@bp.route(
    '/categories/<codename>/quirks',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_category',
])
@cacahuate.http.middleware.pagination
def view_category_quirks(codename):
    category = db_session.query(Category).filter(
        Category.codename == codename,
    ).one()

    params = flask.request.args.to_dict()

    payload = {}

    mapper = {
        'search_query': str,
        'codename__eq': str,
        'codename__in': lambda x: str(x).split(','),
        'users__identifier__eq': str,
        'users__identifier__in': lambda x: str(x).split(','),
        'users__email__eq': str,
        'users__email__in': lambda x: str(x).split(','),
    }

    for key, fn in mapper.items():
        if params.get(key) is not None:
            payload[key] = fn(params[key])

    query = query_userquirks(
        offset=flask.g.offset,
        limit=flask.g.limit,
        **payload,
        category__codename__eq=category.codename,
    )

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/category/<codename>/quirks',
    methods=['POST'],
)
@bp.route(
    '/categories/<codename>/quirks',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_category',
])
def add_category_quirks(codename):
    category = db_session.query(Category).filter(
        Category.codename == codename,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename', 'name'],
    )

    quirk = UserQuirk(**{
        **flask.request.json,
        'category': category,
    })

    db_session.add(quirk)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': quirk.codename,
            'category': category.codename,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/category/<codename>/quirks',
    methods=['PUT'],
)
@bp.route(
    '/categories/<codename>/quirks',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_category',
])
def set_category_quirks(codename):
    category = db_session.query(Category).filter(
        Category.codename == codename,
    ).one()

    mapped = {
      x['codename']: x
      for x in flask.request.json
    }

    items = []
    for item in category.quirks:
        if item.codename not in mapped:
            continue

        for k, v in mapped[item.codename].items():
            setattr(item, k, v)

        items.append(item)
        mapped.pop(item.codename)

    for item in mapped.values():
        items.append(
            UserQuirk(**item),
        )

    category.quirks = items

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'category': category.codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/category/<category_codename>/quirks/<quirk_codename>',
    methods=['DELETE'],
)
@bp.route(
    '/categories/<category_codename>/quirks/<quirk_codename>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.delete_userquirk',
])
def delete_category_quirks(category_codename, quirk_codename):
    quirk = db_session.query(UserQuirk).join(
        Category,
    ).filter(
        UserQuirk.codename == quirk_codename,
        Category.codename == category_codename,
    ).one()

    db_session.delete(quirk)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': quirk.codename,
            'category': quirk.category.codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )
