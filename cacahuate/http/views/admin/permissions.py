import http

import cacahuate.errors
import cacahuate.http.middleware
import cacahuate.http.validation
from cacahuate.database import db_session
from cacahuate.database.query import query_permissions
from cacahuate.models import (
    Permission,
)

import flask


bp = flask.Blueprint('cacahuate_permissions', __name__)


# TODO: Delete legacy url
@bp.route(
    '/permission',
    methods=['GET'],
)
@bp.route(
    '/permissions',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_permission',
])
@cacahuate.http.middleware.pagination
def view_permissions():
    params = flask.request.args.to_dict()

    payload = {}

    mapper = {
        'search_query': str,
        'codename__eq': str,
        'codename__in': lambda x: str(x).split(','),
        'groups__codename__eq': str,
        'groups__codename__in': lambda x: str(x).split(','),
        'users__identifier__eq': str,
        'users__identifier__in': lambda x: str(x).split(','),
        'users__email__eq': str,
        'users__email__in': lambda x: str(x).split(','),
    }

    for key, fn in mapper.items():
        if params.get(key) is not None:
            payload[key] = fn(params[key])

    query = query_permissions(
        offset=flask.g.offset,
        limit=flask.g.limit,
        **payload,
    )

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/permission',
    methods=['POST'],
)
@bp.route(
    '/permissions',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.add_permission',
])
def add_permission():
    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename', 'name'],
    )

    perm = Permission(
        name=flask.request.json['name'],
        codename=flask.request.json['codename'],
    )

    db_session.add(perm)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': perm.codename,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/permission/<codename>',
    methods=['PUT'],
)
@bp.route(
    '/permissions/<codename>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_permission',
])
def set_permission(codename):
    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename', 'name'],
    )

    permission = db_session.query(Permission).filter(
        Permission.codename == codename,
    ).first()

    if permission is None:
        permission = Permission(**{
            **flask.request.json,
            'codename': codename,
        })
        db_session.add(permission)
    else:
        for k, v in {
            **flask.request.json,
            'codename': codename,
        }.items():
            setattr(permission, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': permission.codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/permission/<codename>',
    methods=['DELETE'],
)
@bp.route(
    '/permissions/<codename>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.delete_permission',
])
def delete_permission(codename):
    perm = db_session.query(Permission).filter(
        Permission.codename == codename,
    ).one()

    db_session.delete(perm)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': perm.codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )
