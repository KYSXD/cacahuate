import datetime

import cacahuate.errors
import cacahuate.http.middleware
import cacahuate.http.validation
import cacahuate.imports
from cacahuate.database import db_session
from cacahuate.models import (
    User,
    get_or_create_user,
)

import case_conversion

import flask

import jwt


bp = flask.Blueprint('cacahuate_auth', __name__)


@bp.route('/auth/providers', methods=['GET'])
def get_providers():
    providers = flask.current_app.config['ENABLED_LOGIN_PROVIDERS']

    built_providers = []
    for item in providers:
        built_providers.append({
            'key': item,
            'label': case_conversion.pascalcase(item),
        })

    for item in flask.current_app.config['CUSTOM_LOGIN_PROVIDERS'].keys():
        built_providers.append({
            'key': item,
            'label': case_conversion.pascalcase(item),
        })

    return flask.jsonify({
        'items': built_providers,
    })


@bp.route('/auth/signin/<backend_id>', methods=['POST'])
def signin(backend_id):
    providers = flask.current_app.config['ENABLED_LOGIN_PROVIDERS']

    listed_providers = []
    for item in providers:
        if isinstance(item, str):
            listed_providers.append(item)
        elif isinstance(item, tuple):
            listed_providers.append(item[0])

    try:
        cls = cacahuate.imports.user_import(
            backend_id,
            'AuthProvider',
            flask.current_app.config['CUSTOM_LOGIN_PROVIDERS'],
            'cacahuate.auth.backends',
            listed_providers,
        )
    except cacahuate.errors.MisconfiguredProvider as e:
        flask.abort(500, str(e))

    backend = cls(flask.current_app.config)

    if (
        flask.request.content_type
        and flask.request.content_type.startswith('application/json')
    ):
        payload = flask.request.json
    else:
        payload = flask.request.form.to_dict()

    # this raises AuthenticationError exception if failed
    identifier, data = backend.authenticate(**payload)

    user = get_or_create_user(identifier, data)

    time_now = datetime.datetime.utcnow()
    time_exp = time_now + datetime.timedelta(
        seconds=flask.current_app.config['JWT_ACCESS_TOKEN_EXPIRES'],
    )
    token_str = jwt.encode(
        {
            'sub': user.identifier,
            'exp': time_exp,
            'iat': time_now,
        },
        flask.current_app.config['SECRET_KEY'],
        algorithm='HS256',
    )

    return flask.jsonify({
        'data': {
            'username': user.identifier,
            'token': token_str,
            # default fields
            **user.as_json(include={
                '_type',
                'fullname',
                'identifier',
                'email',
                'is_admin',
                'is_superuser',
                'is_staff',
            }),
            'permissions': [
                perm.codename for perm in user.permissions
            ],
        },
    })


@bp.route('/auth/signout', methods=['POST'])
def signout():
    identifier = flask.request.authorization['username']

    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).first()

    if user is None:
        raise cacahuate.http.errors.Unauthorized([{
            'detail': 'Your credentials are invalid, sorry',
            'where': 'request.authorization',
        }])

    return flask.jsonify({
        'data': {
            'username': user.identifier,
        },
    })


@bp.route('/auth/whoami')
@cacahuate.http.middleware.requires_auth
def whoami():
    user = flask.g.user

    return flask.jsonify({
        'data': {
            'username': user.identifier,
            # default fields
            **user.as_json(include={
                '_type',
                'fullname',
                'identifier',
                'email',
                'is_admin',
                'is_superuser',
                'is_staff',
            }),
            'permissions': [
                perm.codename for perm in user.permissions
            ],
        },
    })
