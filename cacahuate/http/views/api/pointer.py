import http

import cacahuate.http.middleware
from cacahuate.database import db_session
from cacahuate.errors import (
    ElementNotFound,
)
from cacahuate.http.errors import (
    BadRequest,
    Forbidden,
)
from cacahuate.http.mongo import mongo
from cacahuate.http.validation import (
    validate_json,
)
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.mongo import make_context
from cacahuate.tasks import handle
from cacahuate.xml import (
    Xml,
)
from cacahuate.xml.node import make_node

import flask


bp = flask.Blueprint('cacahuate_pointer_legacy', __name__)


@bp.route('/pointer', methods=['POST'])
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
def continue_process():
    validate_json(flask.request.json, ['execution_id', 'node_id'])

    execution_id = flask.request.json['execution_id']
    node_id = flask.request.json['node_id']

    execution = db_session.query(Execution).filter(
        Execution.id == execution_id,
        Execution.status == Execution.Status.ONGOING,
    ).first()
    if execution is None:
        raise BadRequest([{
            'detail': 'execution_id is not valid',
            'code': 'validation.invalid',
            'where': 'request.body.execution_id',
        }])

    xml = Xml.load(
        flask.current_app.config,
        execution.process_name,
        direct=True,
    )
    xmliter = iter(xml)

    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]
    state = collection.find_one({
        'id': execution.id,
    }) or {}

    try:
        continue_point = make_node(
            xmliter.find(lambda e: e.getAttribute('id') == node_id),
            xmliter,
            context=make_context(state['values'], {}),
        )
    except ElementNotFound:
        raise BadRequest([{
            'detail': 'node_id is not a valid node',
            'code': 'validation.invalid_node',
            'where': 'request.body.node_id',
        }])

    try:
        pointer = next(
            p_ for p_ in execution.pointers
            if p_.node_id == node_id and p_.status == Pointer.Status.ONGOING
        )
    except StopIteration:
        raise BadRequest([{
            'detail': 'node_id does not have a live pointer',
            'code': 'validation.no_live_pointer',
            'where': 'request.body.node_id',
        }])

    # Check for authorization
    if pointer not in flask.g.user.assigned_tasks:
        raise Forbidden([{
            'detail': 'Provided user does not have this task assigned',
            'where': 'request.authorization',
        }])

    # Check deps ready
    continue_point.validate_status(
        config=flask.current_app.config,
        state=state,
    )

    # Validate asociated forms
    collected_input = continue_point.validate_input(flask.request.json)

    # trigger rabbit
    handle.delay(flask.json.dumps({
        'command': 'step',
        'pointer_id': pointer.id,
        'user_identifier': flask.g.user.identifier,
        'input': collected_input,
    }))

    return {
        'data': 'accepted',
    }, 202


@bp.route('/pointer/<pk>/notify', methods=['POST'])
@cacahuate.http.middleware.requires_auth
def notify_pointer(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
        Pointer.status == Pointer.Status.ONGOING,
    ).first()

    if pointer is None:
        flask.abort(404)

    # trigger rabbit
    handle.delay(flask.json.dumps({
        'command': 'task_notify',
        'pointer_id': pointer.id,
    }))

    return flask.make_response(
        flask.jsonify({
            'data': 'accepted',
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


@bp.route('/pointer/<pk>/reload', methods=['POST'])
@cacahuate.http.middleware.requires_auth
def reload_pointer(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
        Pointer.status == Pointer.Status.ONGOING,
    ).first()

    if pointer is None:
        flask.abort(404)

    # trigger rabbit
    handle.delay(flask.json.dumps({
        'command': 'reload',
        'pointer_id': pointer.id,
    }))

    return flask.make_response(
        flask.jsonify({
            'data': 'accepted',
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )
