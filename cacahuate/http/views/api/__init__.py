from cacahuate.database import db_session
from cacahuate.http.errors import (
    Forbidden,
)
from cacahuate.http.middleware import (
    requires_auth,
    requires_json,
)
from cacahuate.http.mongo import mongo
from cacahuate.models import (
    Pointer,
)
from cacahuate.mongo import make_context
from cacahuate.xml import (
    Xml,
    form_to_dict,
    get_text,
)

import flask


bp = flask.Blueprint('cacahuate', __name__)


@bp.route('/', methods=['GET', 'POST'])
@requires_json
def index():
    ''' This is here to provide a successful response for the / url and also to
    provide a test for the json middleware '''
    if flask.request.method == 'GET':
        return {
            'hello': 'world',
        }
    elif flask.request.method == 'POST':
        return flask.request.json


@bp.route('/task/<pk>', methods=['GET'])
@requires_auth
def task_read(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
        Pointer.status == Pointer.Status.ONGOING,
    ).one()

    if pointer not in flask.g.user.assigned_tasks:
        raise Forbidden([{
            'detail': 'Provided user does not have this task assigned',
            'where': 'request.authorization',
        }])

    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]
    state = collection.find_one({
        'id': pointer.execution.id,
    })

    xml = Xml.load(
        flask.current_app.config,
        pointer.execution.process_name,
        direct=True,
    )
    xmliter = iter(xml)
    node = xmliter.find(lambda e: e.getAttribute('id') == pointer.node_id)
    xmliter.parser.expandNode(node)

    # Response body
    json_data = {
        **pointer.as_json(),
        'execution': pointer.execution.as_json(),
        'node_type': node.tagName,
        'form_array': [],
    }

    # Append forms
    context = make_context(state['values'], {})
    for form in node.getElementsByTagName('form'):
        json_data['form_array'].append(form_to_dict(form, context=context))

    # If any append previous work done
    node_state = state['state']['items'][pointer.node_id]
    node_actors = node_state['actors']
    user_identifier = flask.g.user.identifier
    if user_identifier in node_actors['items']:
        json_data['prev_work'] = node_actors['items'][user_identifier]['forms']

    # Append validation
    if node.tagName == 'validation':
        deps = [
            get_text(dep)
            for dep in node.getElementsByTagName('dep')
        ]

        fields = []
        for dep in deps:
            form_ref, input_name = dep.split('.')

            # TODO: this could be done in O(log N + K)
            for node in state['state']['items'].values():
                if node['state'] != 'valid':
                    continue

                for identifier in node['actors']['items']:
                    actor = node['actors']['items'][identifier]
                    if actor['state'] != 'valid':
                        continue

                    for form_ix, form in enumerate(actor['forms']):
                        if form['state'] != 'valid':
                            continue

                        if form['ref'] != form_ref:
                            continue

                        if input_name not in form['inputs']['items']:
                            continue

                        inpt = form['inputs']['items'][input_name]

                        state_ref = [
                            node['id'],
                            identifier,
                            str(form_ix),
                        ]
                        state_ref = '.'.join(state_ref)
                        state_ref = state_ref + ':' + dep

                        field = {
                            'ref': state_ref,
                            **inpt,
                        }
                        del field['state']

                        fields.append(field)

        json_data['fields'] = fields

    return flask.jsonify({
        'data': json_data,
    })
