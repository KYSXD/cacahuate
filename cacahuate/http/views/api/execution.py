import cacahuate.http.middleware
from cacahuate.database import db_session
from cacahuate.errors import (
    MalformedProcess,
    ProcessNotFound,
    RequiredInputError,
    RequiredListError,
)
from cacahuate.http.errors import (
    BadRequest,
    NotFound,
    UnprocessableEntity,
)
from cacahuate.http.mongo import mongo
from cacahuate.http.validation import (
    validate_auth,
    validate_json,
    validate_patch_inputs,
)
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import make_node

import flask

import sqlalchemy.orm
import sqlalchemy.orm.exc


bp = flask.Blueprint('cacahuate_execution_legacy', __name__)


@bp.route('/execution/<execution_id>', methods=['GET'])
@cacahuate.http.middleware.requires_auth
def process_status(execution_id):
    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]

    try:
        exc = next(collection.find({'id': execution_id}, {'_id': False}))
    except StopIteration:
        raise sqlalchemy.orm.exc.NoResultFound(
            'Specified execution never existed, and never will',
        )

    return flask.jsonify({
        'data': exc,
    })


@bp.route('/execution/<pk>', methods=['PATCH'])
@cacahuate.http.middleware.requires_auth
def execution_patch(pk):
    execution = db_session.query(Execution).filter(
        Execution.id == pk,
        Execution.status == Execution.Status.ONGOING,
    ).first()

    if execution is None:
        flask.abort(404)

    validate_json(flask.request.json, ['comment', 'inputs'])

    if type(flask.request.json['inputs']) is not list:
        raise RequiredListError('inputs', 'request.body.inputs')

    if not flask.request.json['comment']:
        raise RequiredInputError('comment', 'request.body.inputs')

    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]
    execution_state = next(collection.find({'id': pk}))

    xml = Xml.load(
        flask.current_app.config,
        execution.process_name,
        direct=True,
    )

    processed_inputs = validate_patch_inputs(
        flask.request.json['inputs'],
        execution_state,
        xml,
    )

    handle.delay(flask.json.dumps({
        'command': 'patch',
        'execution_id': execution.id,
        'comment': flask.request.json['comment'],
        'inputs': processed_inputs,
        'user_identifier': flask.g.user.identifier,
    }))

    return flask.jsonify({
        'data': 'accepted',
    }), 202


@bp.route('/execution/<exe_id>/user', methods=['POST'])
@cacahuate.http.middleware.requires_auth
def execution_add_user(exe_id):
    ''' adds the user as a candidate for solving the given node, only if the
    node has an active pointer. '''
    # TODO: possible race condition introduced here.
    # How does this code work in case the handler is moving the pointer?

    execution = db_session.query(Execution).filter(
        Execution.id == exe_id,
        Execution.status == Execution.Status.ONGOING,
    ).first()

    if execution is None:
        flask.abort(404)

    # validate the members needed
    validate_json(flask.request.json, ['identifiers', 'node_id'])

    identifiers = flask.request.json['identifiers']
    node_id = flask.request.json['node_id']

    # get actual pointer
    try:
        next(
            p_ for p_ in execution.pointers
            if p_.node_id == node_id and p_.status == Pointer.Status.ONGOING
        )
    except StopIteration:
        raise BadRequest([{
            'detail': f'{node_id} does not have a live pointer',
            'code': 'validation.no_live_pointer',
            'where': 'request.body.node_id',
        }])

    # trigger rabbit
    handle.delay(flask.json.dumps({
        'command': 'add_candidates',
        'identifiers': identifiers,
        'execution_id': execution.id,
        'node_id': node_id,
    }))

    return flask.jsonify({
        'data': 'accepted',
    }), 202


@bp.route('/execution/<pk>', methods=['DELETE'])
@cacahuate.http.middleware.requires_auth
def delete_process(pk):
    execution = db_session.query(Execution).filter(
        Execution.id == pk,
        Execution.status == Execution.Status.ONGOING,
    ).first()

    if execution is None:
        flask.abort(404)

    handle.delay(flask.json.dumps({
        'command': 'cancel',
        'execution_id': execution.id,
        'user_identifier': flask.g.user.identifier,
    }))

    return flask.jsonify({
        'data': 'accepted',
    }), 202


@bp.route('/execution', methods=['POST'])
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
def start_process():
    validate_json(flask.request.json, ['process_name'])

    try:
        xml = Xml.load(
            flask.current_app.config,
            flask.request.json['process_name'],
        )
    except ProcessNotFound:
        raise NotFound([{
            'detail': '{} process does not exist'
                      .format(flask.request.json['process_name']),
            'where': 'request.body.process_name',
        }])
    except MalformedProcess as e:
        raise UnprocessableEntity([{
            'detail': str(e),
            'where': 'request.body.process_name',
        }])

    xmliter = iter(xml)
    node = make_node(next(xmliter), xmliter)

    # Check for authorization
    validate_auth(node, flask.g.user)

    # check if there are any forms present
    collected_input = node.validate_input(flask.request.json)

    # get rabbit channel for process queue
    execution = xml.start(
        node,
        collected_input,
        mongo.db,
        flask.g.user.identifier,
    )

    # trigger rabbit
    handle.delay(flask.json.dumps({
        'command': 'step',
        'pointer_id': next(
            p_ for p_ in execution.pointers
            if p_.status == Pointer.Status.ONGOING
        ).id,
        'user_identifier': flask.g.user.identifier,
        'input': collected_input,
    }))

    return {
        'data': execution.as_json(),
    }, 201
