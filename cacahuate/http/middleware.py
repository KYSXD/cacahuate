import functools
from functools import wraps

import cacahuate.errors
from cacahuate.database import db_session
from cacahuate.http.errors import (
    BadRequest,
    Unauthorized,
)
from cacahuate.models import (
    User,
)

import flask
from flask import g
from flask import jsonify
from flask import request

import jwt
import jwt.exceptions

from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.exc import NoResultFound

from werkzeug.exceptions import BadRequest as WBadRequest


def requires_json(view):
    @wraps(view)
    def wrapper(*args, **kwargs):
        if request.method in ['POST', 'PUT']:
            if request.headers.get('Content-Type') != 'application/json':
                raise BadRequest([{
                    'detail': 'Content-Type must be application/json',
                    'where': 'request.headers.content_type',
                }])

            try:
                request.get_json()
            except WBadRequest:
                raise BadRequest([{
                    'detail': 'request body is not valid json',
                    'where': 'request.body',
                }])

        res = view(*args, **kwargs)
        if type(res) is flask.wrappers.Response and hasattr(res, 'json'):
            return res
        elif type(res) is tuple:
            return tuple([jsonify(res[0])] + list(res[1:]))
        else:
            return jsonify(res)
    return wrapper


def requires_auth(view):
    @wraps(view)
    def wrapper(*args, **kwargs):
        if request.authorization is None:
            raise Unauthorized([{
                'detail': 'You must provide basic authorization headers',
                'where': 'request.authorization',
            }])

        identifier = request.authorization['username']
        token_str = request.authorization['password']

        try:
            decoded = jwt.decode(
                token_str,
                flask.current_app.config['SECRET_KEY'],
                algorithms=['HS256'],
                options={
                    'require': [
                        'sub',
                        'exp',
                        'iat',
                    ],
                    'verify_exp': True,
                    'verify_iat': True,
                },
            )
        except (
            jwt.exceptions.ExpiredSignatureError,
            jwt.exceptions.DecodeError,
            jwt.exceptions.InvalidTokenError,
        ):
            raise Unauthorized([{
                'detail': 'Your credentials are invalid, sorry',
                'where': 'request.authorization',
            }])

        try:
            user = db_session.query(User).filter(
                User.identifier == identifier,
                User.is_active.__eq__(True),
            ).one()
            db_session.commit()
        except (
            NoResultFound,
            MultipleResultsFound,
        ):
            raise Unauthorized([{
                'detail': 'Your credentials are invalid, sorry',
                'where': 'request.authorization',
            }])

        if decoded['sub'] != user.identifier:
            raise Unauthorized([{
                'detail': 'Your credentials are invalid, sorry',
                'where': 'request.authorization',
            }])

        g.user = user

        return view(*args, **kwargs)
    return wrapper


def pagination(view):
    @wraps(view)
    def wrapper(*args, **kwargs):
        if request.method == 'POST':
            try:
                limit = request.get_json().get(
                    'limit',
                    flask.current_app.config['PAGINATION_LIMIT'],
                )
            except Exception:
                limit = flask.current_app.config['PAGINATION_LIMIT']
            try:
                offset = request.get_json().get(
                    'offset',
                    flask.current_app.config['PAGINATION_OFFSET'],
                )
            except Exception:
                offset = flask.current_app.config['PAGINATION_OFFSET']

        elif request.method == 'GET':
            limit = request.args.get(
                'limit',
                flask.current_app.config['PAGINATION_LIMIT'],
            )
            offset = request.args.get(
                'offset',
                flask.current_app.config['PAGINATION_OFFSET'],
            )

        if not type(limit) is int and not limit.isdigit():
            limit = flask.current_app.config['PAGINATION_LIMIT']

        if not type(offset) is int and not offset.isdigit():
            offset = flask.current_app.config['PAGINATION_OFFSET']

        g.offset = int(offset)
        g.limit = int(limit)

        return view(*args, **kwargs)
    return wrapper


def user_passes_test(test_func):
    def decorator(view_func):
        @functools.wraps(view_func)
        def _wrapped_view(*args, **kwargs):
            if test_func(db_session.query(User).filter(
                User.id == flask.g.user.id,
            ).one()):
                return view_func(*args, **kwargs)
        return _wrapped_view
    return decorator


def permission_required(perm, raise_exception=False):
    def check_perms(user):
        if isinstance(perm, str):
            perms = (perm,)
        else:
            perms = perm
        # First check if the user has the permission
        if user.has_perms(perms):
            return True
        # In case the 403 handler should be called raise the exception
        raise cacahuate.errors.PermissionDenied

    return user_passes_test(check_perms)
