import graphlib


def get_processable_nodes(graph, processed_nodes):
    ts = graphlib.TopologicalSorter(graph)
    ts.prepare()

    processable_nodes = set()
    while ts.is_active():
        node_group = ts.get_ready()

        if not node_group:
            break

        for node in node_group:
            if node in processed_nodes:
                ts.done(node)

            else:
                processable_nodes.add(node)
    return processable_nodes
