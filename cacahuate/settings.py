import os

import passlib.hash

base_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')

SECRET_KEY = os.getenv(
    'CACAHUATE_SECRET_KEY',
    'some_secret_key',
)

JWT_ACCESS_TOKEN_EXPIRES = int(os.getenv(
    'JWT_ACCESS_TOKEN_EXPIRES',
    str(60 * 60),
))

# Rabbitmq settings
RABBIT_HOST = os.getenv('RABBITMQ_HOST', 'localhost')
RABBIT_PORT = os.getenv('RABBITMQ_PORT', 5672)
RABBIT_USER = os.getenv('RABBITMQ_USER', 'guest')
RABBIT_PASS = os.getenv('RABBITMQ_PASS', 'guest')
RABBIT_HEARTBEAT = os.getenv('RABBITMQ_HEARTBEAT', 30)
RABBIT_QUEUE = os.getenv(
    'RABBITMQ_QUEUE',
    'cacahuate_process',
)
RABBIT_NOTIFY_EXCHANGE = os.getenv(
    'RABBITMQ_NOTIFY_EXCHANGE',
    'charpe_notify',
)
RABBIT_CONSUMER_TAG = os.getenv(
    'RABBITMQ_CONSUMER_TAG',
    'cacahuate_consumer_1',
)
CELERY_BROKER_URL = 'amqp://{credentials}{host}:{port}//'.format(
    credentials='{username}:{password}@'.format(
        username=RABBIT_USER,
        password=RABBIT_PASS,
    ) if RABBIT_USER or RABBIT_PASS else '',
    host=RABBIT_HOST,
    port=RABBIT_PORT,
)

# Default logging config
LOGGING_FORMAT = os.getenv(
    'CACAHUATE_LOGGING_FORMAT',
    '[%(asctime)s: %(levelname)s] %(message)s - %(name)s:%(lineno)s',
)
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': LOGGING_FORMAT,
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default',
        },
        # 'charpe': {
        #     'class': 'charpe.CharpeHandler',
        #     'level': 'ERROR',
        #     'host': RABBIT_HOST,
        #     'medium': 'email',
        #     'exchange': RABBIT_NOTIFY_EXCHANGE,
        #     'service_name': 'cacahuate',
        #     'params': {
        #         'recipient': 'support@example.com',
        #         'subject': '[cacahuate] Server Error',
        #         'template': 'server-error',
        #     },
        # },
    },
    'loggers': {
        'cacahuate': {
            'handlers': ['console'],
            'level': 'INFO',
            'filters': [],
        },
    },
}

# Where to store xml files
XML_PATH = os.getenv(
    'CACAHUATE_XML_PATH',
    os.path.join(base_dir, 'xml'),
)

# Custom path to templates
TEMPLATE_PATH = os.getenv(
    'CACAHUATE_TEMPLATE_PATH',
    os.path.join(base_dir, 'template'),
)

# Notification related
NOTIFICATION_ASSIGNED_TASK_SUBJECT = os.getenv(
    'CACAHUATE_NOTIFICATION_ASSIGNED_TASK_SUBJECT',
    '[procesos] Tarea asignada',
)

NOTIFICATION_ASSIGNED_TASK_TEMPLATE = os.getenv(
    'CACAHUATE_NOTIFICATION_ASSIGNED_TASK_TEMPLATE',
    'assigned-task.html',
)

NOTIFICATION_CANCELLED_ACTIVITY_SUBJECT = os.getenv(
    'CACAHUATE_CANCELLED_ACTIVITY_SUBJECT',
    '[procesos] Ejecución cancelada',
)

NOTIFICATION_CANCELLED_ACTIVITY_TEMPLATE = os.getenv(
    'CACAHUATE_CANCELLED_ACTIVITY_TEMPLATE',
    'cancelled-activity.html',
)

USERLESS_TASK_RAISES_ERROR = os.getenv(
    'CACAHUATE_USERLESS_TASK_RAISES_ERROR',
    'True',
) == 'True'

# DB related
DB_ID_FUNCTION = os.getenv(
    'CACAHUATE_ID_FUNCTION',
    'cacahuate.models.id_function',
)

# Mongodb
MONGO_USERNAME = os.getenv('MONGO_USERNAME', '')
MONGO_PASSWORD = os.getenv('MONGO_PASSWORD', '')

MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
MONGO_PORT = os.getenv('MONGO_PORT', '27017')
MONGO_DBNAME = os.getenv('MONGO_DBNAME', 'cacahuate')
MONGO_URI = 'mongodb://{credentials}{host}:{port}/{database}'.format(
    credentials='{username}:{password}@'.format(
        username=MONGO_USERNAME,
        password=MONGO_PASSWORD,
    ) if MONGO_USERNAME or MONGO_PASSWORD else '',
    host=MONGO_HOST,
    port=MONGO_PORT,
    database=MONGO_DBNAME,
)

POINTER_COLLECTION = 'pointer'
EXECUTION_COLLECTION = 'execution'

# API related
URL_PREFIX = os.getenv(
    'CACAHUATE_URL_PREFIX',
    '/v1/',
)

# Defaults for pagination
PAGINATION_LIMIT = int(os.environ.get(
    'CACAHUATE_PAGINATION_LIMIT',
    '20',
))
PAGINATION_OFFSET = int(os.environ.get(
    'CACAHUATE_PAGINATION_OFFSET',
    '0',
))

# Time stuff
TIMEZONE = 'UTC'

# Redis settings
REDIS_USERNAME = os.getenv('REDIS_USERNAME', '')
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', '')

REDIS_HOST = os.getenv('REDIS_HOST', 'localhost')
REDIS_PORT = os.getenv('REDIS_PORT', 6379)
REDIS_DB = os.getenv('REDIS_DB', 0)
REDIS_URL = 'redis://{credentials}{host}'.format(
    credentials='{username}:{password}@'.format(
        username=REDIS_USERNAME,
        password=REDIS_PASSWORD,
    ) if REDIS_USERNAME or REDIS_PASSWORD else '',
    host=REDIS_HOST,
)

# LDAP settings
AUTH_LDAP_SERVER_URI = os.getenv(
    'AUTH_LDAP_SERVER_URI',
    'ldap://ldap.example.com',
)
AUTH_LDAP_USE_SSL = os.getenv(
    'AUTH_LDAP_USE_SSL',
    True,
)
AUTH_LDAP_DOMAIN = os.getenv(
    'AUTH_LDAP_DOMAIN',
    'CACAHUATE',
)
AUTH_LDAP_SEARCH_BASE = os.getenv(
    'AUTH_LDAP_SEARCH_BASE',
    '',
)
AUTH_LDAP_SEARCH_FILTER = os.getenv(
    'AUTH_LDAP_SEARCH_FILTER',
    '',
)
AUTH_LDAP_USER_ATTR_MAP = {
    'email': 'mail',
    'fullname': 'displayName',
}

# The different providers that can be used for log in
ENABLED_LOGIN_PROVIDERS = []

if os.getenv(
    'CACAHUATE_ENABLE_LDAP_LOGIN',
    'False',
) == 'True':
    ENABLED_LOGIN_PROVIDERS.append('ldap')

if os.getenv(
    'CACAHUATE_ENABLE_IMPERSONATE_LOGIN',
    'False',
) == 'True':
    ENABLED_LOGIN_PROVIDERS.append('impersonate')

IMPERSONATE_PASSWORD = passlib.hash.pbkdf2_sha256.hash(os.getenv(
    'CACAHUATE_IMPERSONATE_PASSWORD',
    'something.',
))


# Providers enabled for locating people in the system
ENABLED_HIERARCHY_PROVIDERS = [
    'anyone',
    'backref',
    'admin',
]

# custom login providers
CUSTOM_LOGIN_PROVIDERS = {
    # 'name': 'importable.path',
}

# custom hierarchy providers
CUSTOM_HIERARCHY_PROVIDERS = {
    # 'name': 'importable.path',
}

# will be sent to charpe for rendering of emails
GUI_URL = os.getenv(
    'CACAHUATE_GUI_URL',
    'http://localhost:8080',
)

# Invalid filters for query string
INVALID_FILTERS = (
    'limit',
    'offset',
)

PROCESS_ENV = {
    # 'CUSTOM_VAR': 'its value',
}

JINJA_FILTERS = {
    # 'ismybirthday': ismybirthday,
}

SOLVERS = {
    # 'check_truthy': lambda x: (True, '') if x else (False, f'{x} fails'),
}

SQLALCHEMY_DATABASE_URI = (
    'postgresql+psycopg2://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
).format(
    DB_USER=os.environ.get('POSTGRES_USER', 'cacahuate_user'),
    DB_PASS=os.environ.get('POSTGRES_PASSWORD', 'cacahuate_pass'),
    DB_HOST=os.environ.get('POSTGRES_HOST', 'localhost'),
    DB_PORT=os.environ.get('POSTGRES_PORT', '5432'),
    DB_NAME=os.environ.get('POSTGRES_DB', 'cacahuate_db'),
)
SQLALCHEMY_CONNECT_TIMEOUT = int(os.environ.get(
    'SQLALCHEMY_CONNECT_TIMEOUT',
    '10',
))
SQLALCHEMY_TRACK_MODIFICATIONS = (
    False
)

ENABLE_GRAPHIQL = os.getenv(
    'CACAHUATE_ENABLE_GRAPHIQL',
    'False',
) == 'True'
