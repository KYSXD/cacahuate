import collections
import datetime
import importlib
import logging

import cacahuate.rabbit
from cacahuate.cascade import cascade_invalidate
from cacahuate.cascade import track_next_node
from cacahuate.database import db_session
from cacahuate.errors import (
    EndOfProcess,
    InconsistentState,
    MisconfiguredProvider,
)
from cacahuate.jsontypes import Map
from cacahuate.models import (
    Execution,
    Pointer,
    User,
)
from cacahuate.mongo import make_context
from cacahuate.mongo import pointer_entry
from cacahuate.templates import render_or
from cacahuate.xml import Xml
from cacahuate.xml.node import UserAttachedNode

import pymongo

import simplejson as json

import sqlalchemy.orm.exc


LOGGER = logging.getLogger(__name__)


def get_user_or_inconsistent_state(identifier):
    if identifier == '__system__':
        return cacahuate.models.get_or_create_user(
            '__system__',
            {
                'identifier': '__system__',
                'fullname': 'System',
                'email': None,
            },
        )

    try:
        return db_session.query(User).filter(
            User.identifier == identifier,
            User.is_active.__eq__(True),
        ).one()
    except sqlalchemy.orm.exc.NoResultFound:
        raise InconsistentState(
            f'sent identifier of unexisten user: {identifier}',
        )


class FieldReference:
    __slots__ = (
        'node_id',
        'user_identifier',
        'task_form_index',
        'form_id',
        'field_id',
    )

    def __init__(
        self,
        *,
        node_id=None,
        user_identifier=None,
        task_form_index=None,
        form_id=None,
        field_id=None,
        ref=None,
    ):
        if ref is not None:
            node_id, user_identifier, form_path, field_id = ref.split('.')
            task_form_index, form_id = form_path.split(':')

        tokens = (
            node_id,
            user_identifier,
            task_form_index,
            form_id,
            field_id,
        )
        if None in tokens or '' in tokens:
            raise Exception('Malformed field reference')

        self.node_id = node_id
        self.user_identifier = user_identifier
        self.task_form_index = int(task_form_index)
        self.form_id = form_id
        self.field_id = field_id

    def __str__(self):
        return '.'.join([
            self.node_id,
            self.user_identifier,
            f'{self.task_form_index}:{self.form_id}',
            self.field_id,
        ])


def build_pointer_inputs(
    *,
    collected_forms,
    node_id,
    author_identifier,
):
    values = []

    form_count = collections.defaultdict(lambda: 0)
    for task_form_index, form in enumerate(collected_forms):
        form_ref = form['ref']
        form_count[form_ref] += 1
        for field_id, field_value in form['inputs']['items'].items():
            reference = FieldReference(
                node_id=node_id,
                user_identifier=author_identifier,
                task_form_index=task_form_index,
                form_id=form['ref'],
                field_id=field_id,
            )
            values.append({
                'ref': str(reference),
                'value': field_value['value'],
                'value_caption': field_value['value_caption'],
                'action': 'create',
                'author': reference.user_identifier,
                'task_form_index': reference.task_form_index,
                'form_index': form_count[form_ref] - 1,
                'form_id': reference.form_id,
                'field_id': reference.field_id,
            })
    return values


def build_validation_inputs(
    *,
    collected_forms,
    node_id,
    author_identifier,
):
    form = collected_forms[0]
    if form['inputs']['items']['response']['value'] == 'accept':
        return []

    invalidations = form['inputs']['items']['inputs']['value']
    if invalidations is None:
        return []

    values = []
    for item in invalidations:
        reference = FieldReference(ref=item['ref'])
        values.append({
            'ref': str(reference),
            'value': None,
            'value_caption': '',
            'action': 'delete',
            'author': author_identifier,
            'task_form_index': reference.task_form_index,
            'form_index': None,
            'form_id': reference.form_id,
            'field_id': reference.field_id,
        })
    return values


def build_pointer_inputs_util(
    *,
    collected_forms,
    node_id,
    node_type,
    author_identifier,
):
    if node_type in [
        Pointer.NodeType.ACTION,
        Pointer.NodeType.CALL,
        Pointer.NodeType.CONNECTION,
        Pointer.NodeType.ELIF,
        Pointer.NodeType.ELSE,
        Pointer.NodeType.EXIT,
        Pointer.NodeType.IF,
        Pointer.NodeType.REQUEST,
    ]:
        return build_pointer_inputs(
            collected_forms=collected_forms,
            node_id=node_id,
            author_identifier=author_identifier,
        )

    if node_type == Pointer.NodeType.VALIDATION:
        return build_validation_inputs(
            collected_forms=collected_forms,
            node_id=node_id,
            author_identifier=author_identifier,
        )

    return []


def build_patch_inputs(collected_inputs, author_identifier):
    values = []
    for item in collected_inputs:
        reference = FieldReference(ref=item['ref'])
        if 'value' in item:
            values.append({
                'ref': str(reference),
                'value': item['value'],
                'value_caption': item['value_caption'],
                'action': 'update',
                'author': author_identifier,
                'task_form_index': reference.task_form_index,
                'form_index': None,
                'form_id': reference.form_id,
                'field_id': reference.field_id,
            })
        else:
            values.append({
                'ref': str(reference),
                'value': None,
                'value_caption': None,
                'action': 'delete',
                'author': author_identifier,
                'task_form_index': reference.task_form_index,
                'form_index': None,
                'form_id': reference.form_id,
                'field_id': reference.field_id,
            })

    return values


def unpack_pointer_values(pointer):
    is_finished = pointer['state'] == Pointer.Status.FINISHED
    node_type = pointer['node']['type']
    if is_finished and node_type in (
        Pointer.NodeType.ACTION,
        Pointer.NodeType.CALL,
        Pointer.NodeType.CONNECTION,
        Pointer.NodeType.ELIF,
        Pointer.NodeType.ELSE,
        Pointer.NodeType.EXIT,
        Pointer.NodeType.IF,
        Pointer.NodeType.REQUEST,
        Pointer.NodeType.VALIDATION,
    ):
        (actor_json,) = pointer['actors']['items'].values()
        author_identifier = actor_json['user']['identifier']
        collected_forms = actor_json['forms']

        return build_pointer_inputs_util(
            collected_forms=collected_forms,
            node_id=pointer['node']['id'],
            node_type=node_type,
            author_identifier=author_identifier,
        )

    if 'patch' in pointer:
        collected_inputs = pointer['patch']['inputs']
        author_identifier = pointer['patch']['actor']['identifier']

        return build_patch_inputs(collected_inputs, author_identifier)

    return []


def compact_values(values):
    compacted = collections.defaultdict(list)
    for value in values:
        form_id = value['form_id']
        form_index = value['form_index']
        field_id = value['field_id']
        if value['action'] == 'create':
            # extend form_array if needed
            array_len = len(compacted[form_id])
            if array_len <= form_index:
                compacted[form_id] += [None] * (form_index - array_len)
                compacted[form_id].append(collections.defaultdict(
                    lambda: {
                        'authors': set(),
                        'refs': set(),
                    },
                ))
            current_form = compacted[form_id][form_index]
            current_form[field_id]['authors'].add(value['author'])
            current_form[field_id]['refs'].add(value['ref'])
            current_form[field_id]['value'] = value['value']
            current_form[field_id]['value_caption'] = value['value_caption']

        if value['action'] == 'update':
            # find actual form_index
            for index, form in enumerate(compacted[form_id]):
                for field in form.values():
                    for ref in field['refs']:
                        if value['ref'].startswith(ref.rsplit(',', 1)[0]):
                            form_index = index
            current_form = compacted[form_id][form_index]
            current_form[field_id]['authors'].add(value['author'])
            current_form[field_id]['value'] = value['value']
            current_form[field_id]['value_caption'] = value['value_caption']

        if value['action'] == 'delete':
            # find actual form_index
            for index, form in enumerate(compacted[form_id]):
                for field in form.values():
                    for ref in field['refs']:
                        if value['ref'].startswith(ref.rsplit(',', 1)[0]):
                            form_index = index
            current_form = compacted[form_id][form_index]
            current_form[field_id]['authors'].add(value['author'])
            current_form[field_id]['value'] = value['value']
            current_form[field_id]['value_caption'] = value['value_caption']

    return {
        k: list(map(dict, v))
        for (k, v) in compacted.items()
    }


class Handler:
    ''' The actual process machine, it is in charge of moving the pointers
    through the graph of nodes '''

    def __init__(self, config):
        self.config = config
        self.mongo = None

    def __call__(self, body: bytes):
        ''' the main callback of cacahuate, gets called when a new message
        arrives from rabbitmq. '''
        message = json.loads(body)

        if message['command'] == 'cancel':
            self.cancel_execution(message)
        elif message['command'] == 'step':
            self.step(message)
        elif message['command'] == 'patch':
            self.patch(message)
        elif message['command'] == 'reload':
            self.reload(message)
        elif message['command'] == 'task_notify':
            self.task_notify(message)
        elif message['command'] == 'add_candidates':
            self.add_candidates(message)
        else:
            LOGGER.warning(
                'Unrecognized command {}'.format(message['command']),
            )

    def step(self, message: dict) -> None:
        ''' Handles deleting a pointer from the current node and creating a new
        one on the next '''
        pointer = db_session.query(Pointer).filter(
            Pointer.id == message['pointer_id'],
        ).one()

        user = get_user_or_inconsistent_state(message['user_identifier'])

        execution = pointer.execution

        if pointer.status != Pointer.Status.ONGOING:
            LOGGER.info('Queued dead pointer: {pointer_id}'.format(
                pointer_id=message.get('pointer_id'),
            ))
            return None

        process_name = execution.process_name
        xml = Xml.load(
            self.config,
            process_name,
            direct=True,
        )
        node = xml.get_node(pointer.node_id)

        # node's lifetime ends here
        self.teardown(node, pointer, user, message['input'])

        # compute the next node in the sequence
        try:
            next_node = node
            state = self.execution_collection().find_one({'id': execution.id})

            while True:
                next_node = next_node.get_next_node(
                    xml,
                    state,
                    self.get_mongo(),
                    self.config,
                )

                state = (
                    self.execution_collection().find_one({'id': execution.id})
                )

                next_id = next_node.id
                state_items = state['state']['items']
                if next_id in state_items:
                    if state_items[next_id]['state'] == 'valid':
                        continue
                break
        except (
            EndOfProcess,  # Exit node case
            StopIteration,  # No node case
        ):
            # finish the execution
            return self.finish_execution(execution)

        # node's begining of life
        new_pointer = self.create_pointer(next_node, execution, state)
        new_input = self.wakeup_pointer(next_node, new_pointer, state)

        # Sync nodes are queued immediatly
        if not next_node.is_async():
            from cacahuate.tasks import handle
            handle.delay(json.dumps({
                'command': 'step',
                'pointer_id': new_pointer.id,
                'user_identifier': '__system__',
                'input': new_input,
            }))

    def reload(self, message: dict):
        pointer = db_session.query(Pointer).filter(
            Pointer.id == message['pointer_id'],
        ).one()

        execution = pointer.execution

        if pointer.status != Pointer.Status.ONGOING:
            LOGGER.info('Queued dead pointer: {pointer_id}'.format(
                pointer_id=message.get('pointer_id'),
            ))
            return

        process_name = execution.process_name
        xml = Xml.load(
            self.config,
            process_name,
            direct=True,
        )
        node = xml.get_node(pointer.node_id)
        state = next(self.execution_collection().find({'id': execution.id}))

        new_input = self.wakeup_pointer(node, pointer, state)

        # Sync nodes are queued immediatly
        if not node.is_async():
            from cacahuate.tasks import handle
            handle.delay(json.dumps({
                'command': 'step',
                'pointer_id': pointer.id,
                'user_identifier': '__system__',
                'input': new_input,
            }))

    def create_pointer(self, node, execution, state) -> Pointer:
        # get currect execution context
        exc_doc = next(self.execution_collection().find({'id': execution.id}))
        context = make_context(exc_doc['values'], self.config)

        # create a pointer in this node
        pointer = Pointer(
            node_id=node.id,
            node_type=type(node).__name__.lower(),
            name=node.get_name(context),
            description=node.get_description(context),
            execution=execution,
            status=Pointer.Status.ONGOING,
            id=self.generate_pointer_id(),
        )

        db_session.add(pointer)
        db_session.commit()

        LOGGER.debug('Created pointer p:{} n:{} e:{}'.format(
            pointer.id,
            node.id,
            execution.id,
        ))

        # mark this node as ongoing
        self.execution_collection().update_one({
            'id': execution.id,
        }, {
            '$set': {
                f'state.items.{node.id}.state': pointer.status,
                f'state.items.{node.id}.name': pointer.name,
                f'state.items.{node.id}.description': pointer.description,
            },
        })

        # update registry about this pointer
        self.pointer_collection().insert_one(pointer_entry(
            pointer,
        ))

        return pointer

    def add_candidates(self, message):
        current_time = datetime.datetime.now()

        identifiers = message['identifiers']
        execution_id = message['execution_id']
        node_id = message['node_id']

        execution = db_session.query(Execution).filter(
            Execution.id == execution_id,
        ).one()

        if execution.status != Execution.Status.ONGOING:
            LOGGER.info('Queued dead execution: {execution_id}'.format(
                execution_id=message.get('execution_id'),
            ))
            return

        pointer = next(
            p_ for p_ in execution.pointers
            if p_.node_id == node_id and p_.status == Pointer.Status.ONGOING
        )

        users = db_session.query(User).filter(
            User.identifier.in_(identifiers),
            User.is_active.__eq__(True),
        ).all()

        for user in users:
            if not user.is_active:
                continue

            if user in pointer.candidates:
                continue

            pointer.candidates.append(user)

            if not user.email:
                continue

            mediums = self.get_contact_channels(
                user,
                self.config['NOTIFICATION_ASSIGNED_TASK_SUBJECT'],
                self.config['NOTIFICATION_ASSIGNED_TASK_TEMPLATE'],
            )

            for medium, params in mediums:
                body = json.dumps({
                    **{
                        'data': {
                            'pointer': {
                                **pointer.as_json(),
                                'execution': pointer.execution.as_json(),
                            },
                            'cacahuate_url': self.config['GUI_URL'],
                        },
                    },
                    **params,
                })

                cacahuate.rabbit.send_notify(
                    hostname=self.config['RABBIT_HOST'],
                    port=self.config['RABBIT_PORT'],
                    username=self.config['RABBIT_USER'],
                    password=self.config['RABBIT_PASS'],
                    heartbeat=self.config['RABBIT_HEARTBEAT'],
                    exchange=self.config['RABBIT_NOTIFY_EXCHANGE'],
                    medium=medium,
                    body=body,
                )

        execution.updated_at = current_time
        pointer.updated_at = current_time
        db_session.commit()

    def wakeup_pointer(self, node, pointer, state):
        # notify someone (can raise an exception)
        if isinstance(node, UserAttachedNode):
            self.set_task_candidates(node, pointer, state)
            self.notify_task_candidates(pointer)

        # do some work (can raise an exception)
        if not node.is_async():
            return node.work(self.config, state, self.get_mongo())
        return []

    def task_notify(self, message):
        pointer = db_session.query(Pointer).filter(
            Pointer.id == message['pointer_id'],
        ).one()

        if pointer.status != Pointer.Status.ONGOING:
            LOGGER.info('Queued dead pointer: {pointer_id}'.format(
                pointer_id=message.get('pointer_id'),
            ))
            return

        self.notify_task_candidates(pointer)

    def teardown(self, node, pointer, user, forms):
        ''' finishes the node's lifecycle '''
        current_time = datetime.datetime.now()

        execution = pointer.execution

        if user not in pointer.actors:
            pointer.actors.append(user)

        actor_json = {
            '_type': 'actor',
            'state': 'valid',
            'user': user.as_json(include=[
                '_type',
                'fullname',
                'identifier',
                'email',
            ]),
            'forms': forms,
        }

        values = self.compact_form_values(forms)

        # update state
        mongo_exe = self.execution_collection().find_one_and_update(
            {'id': execution.id},
            {
                '$set': {**{
                    'state.items.{node_id}.state'.format(
                        node_id=pointer.node_id,
                    ): 'valid',
                    'state.items.{node_id}.actors.items.{identifier}'.format(
                        node_id=pointer.node_id,
                        identifier=user.identifier,
                    ): actor_json,
                }, **values},
            },
            return_document=pymongo.collection.ReturnDocument.AFTER,
        )

        context = make_context(mongo_exe['values'], self.config)

        # update execution's name and description
        execution.name = render_or(
            execution.name_template,
            execution.name,
            context,
        )
        execution.description = render_or(
            execution.description_template,
            execution.description,
            context,
        )

        if self.execution_collection().update_one(
            {'id': execution.id},
            {'$set': {
                'name': execution.name,
                'description': execution.description,
                'values._execution.0.name': execution.name,
                'values._execution.0.description': execution.description,
            }},
        ).matched_count != 1:
            raise InconsistentState(
                'Error in pointer teardown: execution {execution_id}'.format(
                    execution_id=execution.id,
                ),
            )

        # update pointer
        if self.pointer_collection().update_one({
            'id': pointer.id,
        }, {
            '$set': {
                'state': 'finished',
                'finished_at': current_time,
                'actors': Map(
                    [actor_json],
                    key=lambda a: a['user']['identifier'],
                ).to_json(),
                'values': build_pointer_inputs_util(
                    collected_forms=forms,
                    node_id=pointer.node_id,
                    node_type=pointer.node_type,
                    author_identifier=user.identifier,
                ),
            },
        }).matched_count != 1:
            raise InconsistentState(
                'Error in pointer teardown: pointer {pointer_id}'.format(
                    pointer_id=pointer.id,
                ),
            )

        execution.updated_at = current_time
        pointer.status = 'finished'
        pointer.updated_at = current_time
        pointer.finished_at = current_time
        db_session.commit()

        LOGGER.debug('Deleted pointer p:{} n:{} e:{}'.format(
            pointer.id,
            pointer.node_id,
            execution.id,
        ))

    def finish_execution(self, execution) -> None:
        """ shuts down this execution and every related object """
        current_time = datetime.datetime.now()

        execution.status = 'finished'
        execution.updated_at = current_time
        execution.finished_at = current_time

        for pointer in execution.pointers:
            if pointer.status != Pointer.Status.ONGOING:
                continue
            pointer.status = Pointer.Status.CANCELLED
            pointer.updated_at = current_time
            pointer.finished_at = current_time

        self.execution_collection().update_one({
            'id': execution.id,
        }, {
            '$set': {
                'status': execution.status,
                'finished_at': execution.finished_at,
            },
        })

        db_session.commit()

        LOGGER.debug('Finished e:{}'.format(execution.id))

    def compact_form_values(self, collected_forms):
        ''' Given an imput from a node create a representation that will be
        used to store the data in the 'values' key of the execution collection
        in mongodb. '''
        compact = {}

        for form in collected_forms:
            key = 'values.{}'.format(form['ref'])

            if key in compact:
                compact[key].append({
                    k: v['value'] for k, v in form['inputs']['items'].items()
                })
            else:
                compact[key] = [{
                    k: v['value'] for k, v in form['inputs']['items'].items()
                }]

        return compact

    def get_invalid_users(self, node_state):
        users = [
            identifier
            for identifier, actor in node_state['actors']['items'].items()
            if actor['state'] == 'invalid'
        ]

        return db_session.query(User).filter(
            User.identifier.in_(users),
            User.is_active.__eq__(True),
        ).all()

    def set_task_candidates(self, node, pointer, state):
        node_state = state['state']['items'][node.id]

        if node_state['state'] == 'invalid':
            users = self.get_invalid_users(node_state)
        else:
            users = node.get_actors(self.config, state)

        if type(users) is not list:
            raise MisconfiguredProvider('Provider returned non list')

        if len(users) == 0 and self.config['USERLESS_TASK_RAISES_ERROR']:
            raise InconsistentState(
                'No user assigned, dead execution {execution_id}'.format(
                    execution_id=pointer.execution.id,
                ),
            )

        for user in users:
            if not user.is_active:
                continue

            if user not in pointer.candidates:
                pointer.candidates.append(user)
        db_session.commit()

        LOGGER.debug('Waking up n:{} found users: {} e:{}'.format(
            node.id,
            ', '.join(u.identifier for u in users),
            pointer.execution.id,
        ))

    def notify_task_candidates(self, pointer):
        for user in pointer.candidates:
            if not user.is_active:
                continue

            if not user.email:
                continue

            mediums = self.get_contact_channels(
                user,
                self.config['NOTIFICATION_ASSIGNED_TASK_SUBJECT'],
                self.config['NOTIFICATION_ASSIGNED_TASK_TEMPLATE'],
            )

            for medium, params in mediums:
                body = json.dumps({
                    **{'data': {
                        'pointer': {
                            **pointer.as_json(),
                            'execution': pointer.execution.as_json(),
                        },
                        'cacahuate_url': self.config['GUI_URL'],
                    }},
                    **params,
                })

                cacahuate.rabbit.send_notify(
                    hostname=self.config['RABBIT_HOST'],
                    port=self.config['RABBIT_PORT'],
                    username=self.config['RABBIT_USER'],
                    password=self.config['RABBIT_PASS'],
                    heartbeat=self.config['RABBIT_HEARTBEAT'],
                    exchange=self.config['RABBIT_NOTIFY_EXCHANGE'],
                    medium=medium,
                    body=body,
                )

    def get_mongo(self):
        if self.mongo is None:
            uri = self.config['MONGO_URI']

            with pymongo.MongoClient(uri) as client:
                database_name = pymongo.uri_parser.parse_uri(uri)['database']
                self.mongo = client[database_name]

        return self.mongo

    def execution_collection(self):
        return self.get_mongo()[self.config['EXECUTION_COLLECTION']]

    def pointer_collection(self):
        return self.get_mongo()[self.config['POINTER_COLLECTION']]

    def get_contact_channels(
        self,
        user: User,
        subject,
        template,
    ):
        return [('email', {
            'recipient': user.email,
            'subject': subject,
            'template': template,
        })]

    def patch(self, message):
        current_time = datetime.datetime.now()

        execution = db_session.query(Execution).filter(
            Execution.id == message['execution_id'],
            Execution.status == Execution.Status.ONGOING,
        ).one()

        process_name = execution.process_name
        xml = Xml.load(
            self.config,
            process_name,
            direct=True,
        )

        # set nodes with pointers as unfilled, delete pointers
        updates = {}

        user = get_user_or_inconsistent_state(message['user_identifier'])

        execution.updated_at = current_time
        for pointer in execution.pointers:
            if pointer.status != Pointer.Status.ONGOING:
                continue
            pointer.status = Pointer.Status.CANCELLED
            pointer.updated_at = current_time
            pointer.finished_at = current_time

            if user not in pointer.actors:
                pointer.actors.append(user)

            updates['state.items.{node}.state'.format(
                node=pointer.node_id,
            )] = 'unfilled'

            self.pointer_collection().update_one({
                'id': pointer.id,
            }, {
                '$set': {
                    'state': 'cancelled',
                    'finished_at': current_time,
                    'patch': {
                        'comment': message['comment'],
                        'inputs': message['inputs'],
                        'actor': user.as_json(include=[
                            '_type',
                            'fullname',
                            'identifier',
                            'email',
                        ]),
                    },
                    'values': build_patch_inputs(
                        message['inputs'],
                        user.identifier,
                    ),
                },
            })
        db_session.commit()

        self.execution_collection().update_one({
            'id': execution.id,
        }, {
            '$set': updates,
        })

        # retrieve updated state
        state = next(self.execution_collection().find({'id': execution.id}))

        state_updates = cascade_invalidate(
            xml,
            state,
            message['inputs'],
            message['comment'],
        )

        # update state
        if state_updates:
            self.execution_collection().update_one({
                'id': state['id'],
            }, {
                '$set': state_updates,
            })

        # retrieve updated state
        state = next(self.execution_collection().find({'id': execution.id}))

        first_invalid_node = track_next_node(
            xml,
            state,
            self.get_mongo(),
            self.config,
        )

        # node's begining of life
        new_pointer = self.create_pointer(first_invalid_node, execution, state)
        new_input = self.wakeup_pointer(first_invalid_node, new_pointer, state)

        # Sync nodes are queued immediatly
        if not first_invalid_node.is_async():
            from cacahuate.tasks import handle
            handle.delay(json.dumps({
                'command': 'step',
                'pointer_id': new_pointer.id,
                'user_identifier': '__system__',
                'input': new_input,
            }))

    def cancel_execution(self, message):
        current_time = datetime.datetime.now()

        execution_id = message['execution_id']

        execution = db_session.query(Execution).filter(
            Execution.id == execution_id,
        ).one()

        if execution.status != Execution.Status.ONGOING:
            LOGGER.info('Queued dead execution: {execution_id}'.format(
                execution_id=message.get('execution_id'),
            ))
            return

        user = get_user_or_inconsistent_state(message['user_identifier'])

        execution.status = 'cancelled'
        execution.finished_at = current_time

        for pointer in execution.pointers:
            if pointer.status != Pointer.Status.ONGOING:
                continue
            pointer.status = Pointer.Status.CANCELLED
            pointer.updated_at = current_time
            pointer.finished_at = current_time

            if user not in pointer.actors:
                pointer.actors.append(user)

        self.execution_collection().update_one({
            'id': execution.id,
        }, {
            '$set': {
                'status': execution.status,
                'finished_at': execution.finished_at,
            },
        })

        self.pointer_collection().update_many({
            'id': {'$in': [item.id for item in execution.pointers]},
            'state': Pointer.Status.ONGOING,
        }, {
            '$set': {
                'state': 'cancelled',
                'finished_at': execution.finished_at,
            },
        })

        users = {}
        for pointer in execution.pointers:
            for actor in pointer.actors:
                users[actor.identifier] = actor

        for user in users.values():
            if not user.is_active:
                continue

            if not user.email:
                continue

            mediums = self.get_contact_channels(
                user,
                self.config['NOTIFICATION_CANCELLED_ACTIVITY_SUBJECT'],
                self.config['NOTIFICATION_CANCELLED_ACTIVITY_TEMPLATE'],
            )

            for medium, params in mediums:
                body = json.dumps({
                    **{'data': {
                        'execution': {
                            **execution.as_json(),
                        },
                        'cacahuate_url': self.config['GUI_URL'],
                    }},
                    **params,
                })

                cacahuate.rabbit.send_notify(
                    hostname=self.config['RABBIT_HOST'],
                    port=self.config['RABBIT_PORT'],
                    username=self.config['RABBIT_USER'],
                    password=self.config['RABBIT_PASS'],
                    heartbeat=self.config['RABBIT_HEARTBEAT'],
                    exchange=self.config['RABBIT_NOTIFY_EXCHANGE'],
                    medium=medium,
                    body=body,
                )

        db_session.commit()

    def _generate_id(self):
        func = getattr(
            importlib.import_module(
                self.config['DB_ID_FUNCTION'].rsplit('.', 1)[0],
            ),
            self.config['DB_ID_FUNCTION'].rsplit('.', 1)[1],
        )

        return func()

    def generate_pointer_id(self):
        new_id = self._generate_id()

        while db_session.query(Pointer).filter(
            Pointer.id == new_id,
        ).count() != 0:
            new_id = self._generate_id()

        return new_id
