#!/usr/bin/env python3
import logging
import logging.config
import os
import time

import cacahuate.database
import cacahuate.database.permissions
from cacahuate.indexes import create_indexes
from cacahuate.tasks import app as celery

from itacate import Config


def main():
    # Load the config
    config = Config(os.path.dirname(os.path.realpath(__file__)))
    config.from_object('cacahuate.settings')

    if os.getenv('CACAHUATE_SETTINGS'):
        config.from_envvar('CACAHUATE_SETTINGS', silent=False)

    # Set the timezone
    os.environ['TZ'] = config['TIMEZONE']
    time.tzset()

    # Setup logging
    logging.config.dictConfig(config['LOGGING'])

    # Load the models
    cacahuate.database.init_engine(
        config['SQLALCHEMY_DATABASE_URI'],
        connect_args={
            'connect_timeout': config['SQLALCHEMY_CONNECT_TIMEOUT'],
        },
    )
    cacahuate.database.permissions.create_initial_permissions()

    # Create mongo indexes
    create_indexes(config)

    # Update celery config
    celery.conf.update(
        broker_url=config['CELERY_BROKER_URL'],
        worker_concurrency=1,
        worker_hijack_root_logger=False,
        task_default_queue=config['RABBIT_QUEUE'],
    )
    worker = celery.Worker()
    worker.start()


def rng_path():
    print(os.path.abspath(os.path.join(
        os.path.dirname(__file__),
        'xml/process-spec.rng',
    )))


if __name__ == '__main__':
    main()
