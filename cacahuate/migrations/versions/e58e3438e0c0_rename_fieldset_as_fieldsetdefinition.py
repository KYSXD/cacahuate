"""Rename fieldset as fieldsetdefinition

Revision ID: e58e3438e0c0
Revises: 0e69a19e86f7
Create Date: 2023-02-21 20:37:53.286758

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e58e3438e0c0'
down_revision = '0e69a19e86f7'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.drop_index('ix_fieldset_id', table_name='fieldset')
    op.drop_index('ix_fieldset_identifier', table_name='fieldset')
    op.drop_index('ix_fieldset_workflow_id', table_name='fieldset')

    op.drop_constraint('field_fieldset_id_fkey', 'field', type_='foreignkey')
    op.rename_table('fieldset', 'fieldsetdefinition')
    op.create_foreign_key('field_fieldsetdefinition_id_fkey', 'field', 'fieldsetdefinition', ['fieldset_id'], ['id'])

    op.create_index(op.f('ix_fieldsetdefinition_id'), 'fieldsetdefinition', ['id'], unique=False)
    op.create_index(op.f('ix_fieldsetdefinition_identifier'), 'fieldsetdefinition', ['identifier'], unique=False)
    op.create_index(op.f('ix_fieldsetdefinition_workflow_id'), 'fieldsetdefinition', ['workflow_id'], unique=False)


def downgrade() -> None:
    op.drop_index(op.f('ix_fieldsetdefinition_workflow_id'), table_name='fieldsetdefinition')
    op.drop_index(op.f('ix_fieldsetdefinition_identifier'), table_name='fieldsetdefinition')
    op.drop_index(op.f('ix_fieldsetdefinition_id'), table_name='fieldsetdefinition')

    op.drop_constraint('field_fieldsetdefinition_id_fkey', 'field', type_='foreignkey')
    op.rename_table('fieldsetdefinition', 'fieldset')
    op.create_foreign_key('field_fieldset_id_fkey', 'field', 'fieldset', ['fieldset_id'], ['id'])

    op.create_index('ix_fieldset_workflow_id', 'fieldset', ['workflow_id'], unique=False)
    op.create_index('ix_fieldset_identifier', 'fieldset', ['identifier'], unique=False)
    op.create_index('ix_fieldset_id', 'fieldset', ['id'], unique=False)
