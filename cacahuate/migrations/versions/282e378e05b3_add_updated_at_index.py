"""Add updated_at index

Revision ID: 282e378e05b3
Revises: 57c580f2a1ec
Create Date: 2023-06-28 11:34:33.371322

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '282e378e05b3'
down_revision = '57c580f2a1ec'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_index(op.f('ix_workflowdefinition_updated_at'), 'workflowdefinition', ['updated_at'], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_workflowdefinition_updated_at'), table_name='workflowdefinition')
    # ### end Alembic commands ###
