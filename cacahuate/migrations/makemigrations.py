import datetime
import alembic.config
import os

here = os.path.dirname(os.path.abspath(__file__))

alembic_args = [
    '-c', os.path.join(here, '../alembic.ini'),
    'revision', '--autogenerate',
    '-m', datetime.datetime.now().strftime("auto_%Y%m%d_%H%M"),
]


def main():
    alembic.config.main(argv=alembic_args)


if __name__ == '__main__':
    main()
