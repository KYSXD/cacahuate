import logging
import traceback

import flask

from jinja2 import Environment, TemplateError

LOGGER = logging.getLogger(__name__)


def datetimeformat(
    value,
    **kwargs,
):
    return value.strftime(
        kwargs.get('format', '%Y-%m-%d %H:%M:%S%z'),
    )


def render_or(template, default, context={}):
    ''' Renders the given template in case it is a valid jinja template or
    returns the default value '''
    env = Environment()

    env.filters['datetimeformat'] = datetimeformat

    if flask.has_app_context():
        for name, fn in flask.current_app.config['JINJA_FILTERS'].items():
            env.filters[name] = fn

    try:
        return env.from_string(template).render(**context)
    except TemplateError:
        LOGGER.error(traceback.format_exc())
        return default
