import pika


def send_notify(
    hostname,
    port,
    username,
    password,
    heartbeat,
    exchange,
    medium,
    body: str,
):
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=hostname,
        port=port,
        credentials=pika.PlainCredentials(
            username=username,
            password=password,
        ),
        heartbeat=heartbeat,
    ))

    channel = connection.channel()

    channel.exchange_declare(
        exchange=exchange,
        exchange_type='direct',
    )

    channel.basic_publish(
        exchange=exchange,
        routing_key=medium,
        body=body,
        properties=pika.BasicProperties(
            delivery_mode=2,
        ),
    )

    connection.close()
