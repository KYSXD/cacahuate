import datetime
from dataclasses import dataclass
from typing import (
    Dict,
    List,
    Optional,
    TypeVar,
)


@dataclass
class UserEntry:
    fullname: str
    identifier: str
    email: Optional[str] = None
    _type: str = 'user'


TActorMapItemEntry = TypeVar('TActorMapItemEntry', bound='ActorMapItemEntry')


@dataclass
class ActorMapItemEntry:
    state: str
    user: UserEntry
    forms: dict
    _type: str = 'actor'

    def __post_init__(self: TActorMapItemEntry) -> None:
        if not isinstance(self.user, UserEntry):
            self.user = UserEntry(**self.user)


TActorMapEntry = TypeVar('TActorMapEntry', bound='ActorMapEntry')


@dataclass
class ActorMapEntry:
    items: Dict[str, ActorMapItemEntry]
    _type: str = 'actor'

    def __post_init__(self: TActorMapEntry) -> None:
        for key, value in self.items.items():
            if not isinstance(value, ActorMapItemEntry):
                self.items[key] = ActorMapItemEntry(**value)


@dataclass
class NodeEntry:
    id: str  # noqa: A003
    name: str
    description: str
    type: str  # noqa: A003


@dataclass
class PatchInputEntry:
    ref: str
    value: Optional[str] = None
    value_caption: Optional[str] = None


TPatchEntry = TypeVar('TPointerEntry', bound='PointerEntry')


@dataclass
class PatchEntry:
    comment: str
    inputs: List[PatchInputEntry]
    actor: UserEntry

    def __post_init__(self: TPatchEntry) -> None:
        for index, value in enumerate(self.inputs):
            if not isinstance(value, PatchInputEntry):
                self.inputs[index] = PatchInputEntry(**value)

        if not isinstance(self.actor, UserEntry):
            self.actor = UserEntry(**self.actor)


TPointerEntry = TypeVar('TPointerEntry', bound='PointerEntry')


@dataclass
class PointerEntry:
    id: str  # noqa: A003
    started_at: datetime.datetime
    finished_at: datetime.datetime
    execution: dict
    node: NodeEntry
    actors: Dict[str, ActorMapEntry]
    process_id: str
    state: str
    patch: Optional[List[PatchEntry]] = None
    _type: str = 'pointer'

    def __post_init__(self: TPointerEntry) -> None:
        if not isinstance(self.actors, ActorMapEntry):
            self.actors = ActorMapEntry(**self.actors)

        if self.patch and not isinstance(self.patch, PatchEntry):
            self.patch = PatchEntry(**self.patch)
