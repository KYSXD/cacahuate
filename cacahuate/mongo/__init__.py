import logging
from datetime import datetime
from datetime import timezone

from cacahuate.jsontypes import Map
from cacahuate.jsontypes import MultiFormDict

LOGGER = logging.getLogger(__name__)

DATE_FIELDS = [
    'started_at',
    'finished_at',
]


def make_context(values, config) -> list[MultiFormDict]:
    ''' the proper and only way to get the ``'values'`` key out of
    an execution document from mongo. It takes care of the transformations
    needed for it to work in jinja templates and other contexts where the
    multiplicity of answers (multiforms) is relevant. '''
    context = {}

    try:
        for key, value in values.items():
            context[key] = MultiFormDict(value)
    except KeyError:
        pass

    context['_env'] = MultiFormDict([config.get('PROCESS_ENV') or {}])

    return context


def execution_entry(execution, state):
    return {
        '_type': 'execution',
        'id': execution.id,
        'name': execution.name,
        'process_name': execution.process_name,
        'description': execution.description,
        'status': execution.status,
        'started_at': execution.started_at,
        'finished_at': None,
        'state': state,
        'values': {
            '_execution': [{
                'id': execution.id,
                'name': execution.name,
                'process_name': execution.process_name,
                'description': execution.description,
                'started_at': datetime.now(timezone.utc).isoformat(),
            }],
        },
    }


def pointer_entry(pointer):
    return {
        'id': pointer.id,
        'started_at': pointer.started_at,
        'finished_at': None,
        'node': {
            'id': pointer.node_id,
            'name': pointer.name,
            'description': pointer.description,
            'type': pointer.node_type.lower(),
        },
        'state': pointer.status,
        'process_id': pointer.execution.process_name,
        'actors': Map([], key='identifier').to_json(),
        'values': [],
    }
