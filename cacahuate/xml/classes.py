from dataclasses import dataclass
from typing import (
    Dict,
    List,
    Optional,
    Set,
    TypeVar,
)


@dataclass
class Modifier:
    key: str
    value: str


@dataclass
class Choice:
    label: str
    value: str


TField = TypeVar('TField', bound='Field')


@dataclass
class Field:
    id: str  # noqa: A003
    label: str
    type: str  # noqa: A003
    widget: str
    dependencies: List[str]
    modifiers: List[Modifier]
    choices: Optional[List[Choice]] = None

    def __post_init__(self: TField) -> None:
        for index, value in enumerate(self.choices or []):
            if not isinstance(value, Choice):
                self.choices[index] = Choice(**value)


TFieldset = TypeVar('TFieldset', bound='Fieldset')


@dataclass
class Fieldset:
    fields: Dict[str, Field]
    id: str  # noqa: A003
    max_amount: int
    min_amount: int

    def __post_init__(self: TFieldset) -> None:
        for key, value in self.fields.items():
            if not isinstance(value, Field):
                self.fields[key] = Field(**value)


@dataclass
class TaskField:
    reference: str
    permission: str


@dataclass
class Task:
    condition: str
    description: str
    fields: List[str]
    id: str  # noqa: A003
    name: str
    predecessors: Set[str]
    requirements: Set[str]
    dependencies: List[str]
    type: str  # noqa: A003


TProcess = TypeVar('TProcess', bound='Process')


@dataclass
class Process:
    identifier: str
    version: str
    name: str
    description: str
    author: str
    public: bool
    tasks: Dict[str, Task]
    fieldsets: Dict[str, Fieldset]

    def __post_init__(self: TProcess) -> None:
        for key, value in self.tasks.items():
            if not isinstance(value, Task):
                self.tasks[key] = Task(**value)

        for key, value in self.fieldsets.items():
            if not isinstance(value, Fieldset):
                self.fieldsets[key] = Fieldset(**value)
