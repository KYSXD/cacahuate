import collections
from typing import (
    Dict,
    Set,
    TypedDict,
)

from cacahuate.xml.classes import (
    Field,
    Fieldset,
    Process,
    Task,
    TaskField,
)


INPUT_TYPE_MAP = {
    'text': 'text',
    'datetime': 'datetime',
    'password': 'text',
    'checkbox': 'text',
    'radio': 'text',
    'select': 'text',
    'file': 'file',
    'date': 'date',
    'int': 'integer',
    'float': 'float',
    'link': 'text',
    'currency': 'float',
    'model': 'foreignkey',
}

INPUT_WIDGET_MAP = {
    'text': 'text',
    'datetime': 'datetime',
    'password': 'password',
    'checkbox': 'checkboxselectmultiple',
    'radio': 'radioselect',
    'select': 'select',
    'file': 'file',
    'date': 'date',
    'int': 'number',
    'float': 'number',
    'link': 'url',
    'currency': 'number',
    'model': 'modelselect',
}


def translate_field(field: dict) -> Field:
    return Field(
        id=field['id'],
        label=field['label'],
        type=INPUT_TYPE_MAP.get(field['type']),
        widget=INPUT_WIDGET_MAP.get(field['type']),
        dependencies=field['dependencies'],
        modifiers=field.get('modifiers') or [],
        choices=field.get('options'),
    )


def translate_form(form: dict) -> Fieldset:
    return Fieldset(
        id=form['id'],
        min_amount=form['min_amount'],
        max_amount=form['max_amount'],
        fields=collections.OrderedDict(
            (field['id'], translate_field(field))
            for field in form['inputs']
        ),
    )


class TranslateTaskResponse(TypedDict):
    tasks: Dict[str, Task]
    fieldsets: Dict[str, Fieldset]
    leaves: Set[str]


def translate_action(
    task: dict,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    fieldsets = collections.OrderedDict(
        (form['id'], translate_form(form))
        for form in task['forms']
    )

    fields = []
    for fieldset_id, fieldset in fieldsets.items():
        for field_id in fieldset.fields.keys():
            fields.append(TaskField(
                permission='editable',
                reference=f'{fieldset_id}.{field_id}',
            ))

        fieldset.fields = dict(fieldset.fields)
    fieldsets = dict(fieldsets)

    return TranslateTaskResponse(
        tasks={
            task['id']: Task(
                id=task['id'],
                type=task['type'],
                name=task['name'],
                description=task['description'],
                predecessors=predecessors.copy(),
                requirements=requirements.copy(),
                dependencies=task.get('dependencies') or [],
                fields=fields,
                condition=None,
            ),
        },
        fieldsets=fieldsets,
        leaves=set(),
    )


def translate_validation(
    task: dict,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    return TranslateTaskResponse(
        tasks={
            task['id']: Task(
                id=task['id'],
                type=task['type'],
                name=task['name'],
                description=task['description'],
                predecessors=predecessors.copy(),
                requirements=requirements.copy(),
                dependencies=task.get('dependencies') or [],
                fields=[
                    TaskField(
                        reference=reference,
                        permission='read_only',
                    ) for reference in task['dependencies']
                ],
                condition=None,
            ),
        },
        fieldsets={},
        leaves=set(),
    )


def translate_exit(
    task: dict,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    return TranslateTaskResponse(
        tasks={
            task['id']: Task(
                id=task['id'],
                type=task['type'],
                name='',
                description='',
                predecessors=predecessors.copy(),
                requirements=requirements.copy(),
                dependencies=task.get('dependencies') or [],
                fields=[
                    TaskField(
                        reference=reference,
                        permission='read_only',
                    ) for reference in task['dependencies']
                ],
                condition=None,
            ),
        },
        fieldsets={},
        leaves=set(),
    )


def translate_ifelifelse(
    task: dict,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    local_requirements = requirements.copy()
    if task.get('condition') is not None:
        local_requirements.add(task.get('condition'))

    data = translate_tasks(
        task['children'],
        local_requirements.copy(),
        {task['id']},
    )

    return TranslateTaskResponse(
        tasks={
            **data['tasks'],
            task['id']: Task(
                id=task['id'],
                type=task['type'],
                name='',
                description='',
                predecessors=predecessors.copy(),
                requirements=requirements.copy(),
                dependencies=task.get('dependencies') or [],
                condition=task.get('condition'),
                fields=[],
            ),
        },
        fieldsets=data['fieldsets'],
        leaves=data['leaves'].copy(),
    )


def translate_request(
    task: dict,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    return TranslateTaskResponse(
        tasks={
            task['id']: Task(
                id=task['id'],
                type=task['type'],
                name='',
                description='',
                predecessors=predecessors.copy(),
                requirements=requirements.copy(),
                dependencies=task.get('dependencies') or [],
                fields=[],
                condition=None,
            ),
        },
        fieldsets={},
        leaves=set(),
    )


def translate_call(
    task: dict,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    return TranslateTaskResponse(
        tasks={
            task['id']: Task(
                id=task['id'],
                type=task['type'],
                name='',
                description='',
                predecessors=predecessors.copy(),
                requirements=requirements.copy(),
                dependencies=task.get('dependencies') or [],
                fields=[],
                condition=None,
            ),
        },
        fieldsets={},
        leaves=set(),
    )


def translate_connection(
    task: dict,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    return TranslateTaskResponse(
        tasks={
            task['id']: Task(
                id=task['id'],
                type=task['type'],
                name='',
                description='',
                predecessors=predecessors.copy(),
                requirements=requirements.copy(),
                dependencies=task.get('dependencies') or [],
                fields=[],
                condition=None,
            ),
        },
        fieldsets={},
        leaves=set(),
    )


def translate_task(
    task: dict,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    fn_mapping = {
        'action': translate_action,
        'validation': translate_validation,
        'exit': translate_exit,
        'if': translate_ifelifelse,
        'elif': translate_ifelifelse,
        'else': translate_ifelifelse,
        'request': translate_request,
        'call': translate_call,
        'connection': translate_connection,
    }

    fn = fn_mapping[task['type']]

    return fn(
        task,
        requirements.copy(),
        predecessors.copy(),
    )


def translate_tasks(
    tasks: list,
    requirements: set,
    predecessors: set,
) -> TranslateTaskResponse:
    translated = {
        'tasks': {},
        'fieldsets': {},
        'leaves': predecessors.copy(),
    }

    for index, task in enumerate(tasks):
        local_requirements = requirements.copy()
        if task['type'] in ['elif', 'else']:
            previous_task = tasks[index-1]
            previous_condition = previous_task['condition']
            previous_requirements = (
                translated['tasks'][previous_task['id']].requirements
            )
            local_requirements.add(f'!({previous_condition})')
            local_requirements.update(previous_requirements)

        local_predecessors = translated['leaves'].copy()
        if task['type'] in ['elif', 'else']:
            previous_task = tasks[index-1]
            local_predecessors = {tasks[index-1]['id']}

        data = translate_task(
            task,
            local_requirements,
            local_predecessors,
        )
        translated['tasks'].update(
            data['tasks'],
        )

        for fieldset in data['fieldsets'].values():
            if fieldset.id in translated['fieldsets']:
                translated['fieldsets'][fieldset.id].fields.update(
                    fieldset.fields,
                )
                continue

            translated['fieldsets'][fieldset.id] = fieldset

        if task['type'] == 'if':
            translated['leaves'] = data['leaves']

        if task['type'] in ['elif', 'else']:
            translated['leaves'].update(data['leaves'])

        if task['type'] not in ['if', 'elif', 'else']:
            translated['leaves'] = {task['id']}

        if task['type'] in ['if', 'elif']:
            if index + 1 < len(tasks):
                if tasks[index+1]['type'] not in ['elif', 'else']:
                    translated['leaves'].add(task['id'])
            else:
                translated['leaves'].add(task['id'])

    return TranslateTaskResponse(
        tasks=translated['tasks'],
        fieldsets=translated['fieldsets'],
        leaves=translated['leaves'],
    )


def translate_process(
    parsed_process: dict,
    identifier: str,
    version: str,
) -> Process:
    data = translate_tasks(
        parsed_process['process'],
        set(),
        set(),
    )

    return Process(
        name=parsed_process['process-info']['name'],
        description=parsed_process['process-info']['description'],
        author=parsed_process['process-info']['author'],
        identifier=identifier,
        version=version,
        public=parsed_process['process-info']['public'],
        tasks=data['tasks'],
        fieldsets=data['fieldsets'],
    )
