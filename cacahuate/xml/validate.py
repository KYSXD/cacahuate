import argparse
import glob
import re
import sys
from xml.dom import pulldom
from xml.sax._exceptions import SAXParseException

from cacahuate.errors import MalformedProcess
from cacahuate.grammar import Condition
from cacahuate.xml import NODES
from cacahuate.xml import get_text

from lark.exceptions import GrammarError
from lark.exceptions import LexError
from lark.exceptions import ParseError


def _validate_file(filename):
    ids = []
    data_form = {}
    passed_nodes = []
    variable_re = re.compile(r'^[a-zA-Z_][a-zA-Z0-9_]*$')

    class StreamWrapper:

        def __init__(self, filename):
            self.f = open(filename)
            self.lineno = 0

        def read(self, b):
            self.lineno += 1
            return self.f.readline()

    sw = StreamWrapper(filename)

    def check_id(node):
        id_element = node.getAttribute('id')

        if not id_element:
            raise MalformedProcess(
                '{}:{} All nodes must have an id'.format(filename, sw.lineno),
            )

        if not variable_re.match(id_element):
            raise MalformedProcess(
                '{}:{} Id must be a valid variable name'.format(
                    filename,
                    sw.lineno,
                ),
            )

        if id_element not in ids:
            ids.append(id_element)
        else:
            raise MalformedProcess("{}:{} Duplicated id: '{}'".format(
                filename, sw.lineno,
                id_element,
            ))

    doc = pulldom.parse(sw)

    for event, node in doc:
        if event != pulldom.START_ELEMENT:
            continue

        if node.tagName == 'condition':
            doc.expandNode(node)  # noqa: B038

            try:
                tree = Condition().parse(get_text(node))
            except GrammarError:
                raise MalformedProcess(
                    '{}:{} Grammar error in condition'.format(
                        filename,
                        sw.lineno,
                    ),
                )
            except ParseError:
                raise MalformedProcess(
                    '{}:{} Parse error in condition'.format(
                        filename,
                        sw.lineno,
                    ),
                )
            except LexError:
                raise MalformedProcess(
                    '{}:{} Lex error in condition'.format(filename, sw.lineno),
                )
            except KeyError as e:
                raise MalformedProcess(
                    '{}:{} variable does not exist: {}'.format(
                        filename, sw.lineno,
                        str(e),
                    ),
                )

            # validate variables used in condition
            for tree in tree.find_data('ref'):
                reference_form, field_form = [
                    x.children[0][:]
                    for x in tree.children
                ]

                try:
                    data_form[reference_form][field_form]
                except KeyError:
                    raise MalformedProcess(
                        "{}:{} variable used in condition "
                        "is not defined '{}'".format(
                            filename,
                            sw.lineno,
                            reference_form + '.' + field_form,
                        ),
                    )

            continue

        if node.tagName not in NODES:
            continue

        # Duplicate ids
        check_id(node)

        if node.tagName == 'if':
            continue

        # Expand and check this node. <if> nodes are not expanded
        doc.expandNode(node)  # noqa: B038

        # Check auth-filter params
        params = node.getElementsByTagName("param")

        for param in params:
            if param.getAttribute('type') != 'ref':
                continue

            ref_type, ref = get_text(param).split('#')

            if ref_type == 'form':
                reference_form, field_form = ref.split('.')

                try:
                    data_form[reference_form][field_form]
                except KeyError:
                    raise MalformedProcess(
                        "{}:{} Referenced param does not exist '{}'".format(
                            filename,
                            sw.lineno,
                            reference_form + '.' + field_form,
                        ),
                    )
            elif ref_type in [
                'user.email',
                'user.identifier',
                'user.fullname',
            ]:
                if ref not in passed_nodes:
                    raise MalformedProcess(
                        '{}:{} Referenced user is never created: {}'.format(
                            filename,
                            sw.lineno,
                            ref,
                        ),
                    )
            else:
                raise MalformedProcess(
                    '{}:{} Reference type unknown: {}'.format(
                        filename,
                        sw.lineno,
                        ref_type,
                    ),
                )

        # Check dependencies
        deps = node.getElementsByTagName("dep")

        for dep in deps:
            form, field = get_text(dep).split('.')

            try:
                data_form[form][field]
            except KeyError:
                raise MalformedProcess(
                    "{}:{} Referenced dependency does not exist '{}'".format(
                        filename,
                        sw.lineno,
                        form + '.' + field,
                    ),
                )

        # fill forms for later usage
        forms = node.getElementsByTagName('form')

        for form in forms:
            form_id = form.getAttribute('id')

            if form_id and not variable_re.match(form_id):
                raise MalformedProcess(
                    '{}:{} Form ids must be valid variable names'.format(
                        filename,
                        sw.lineno,
                    ),
                )

            form_ref = form.getAttribute('ref')

            if form_ref and not variable_re.match(form_ref):
                raise MalformedProcess(
                    '{}:{} Form refs must be valid variable names'.format(
                        filename,
                        sw.lineno,
                    ),
                )

            inputs = form.getElementsByTagName("input")
            array_input = {}

            for inpt in inputs:
                inpt_name = inpt.getAttribute('name')

                if not variable_re.match(inpt_name):
                    raise MalformedProcess(
                        '{}:{} Field names must match [a-zA-Z0-9_]+'.format(
                            filename,
                            sw.lineno,
                        ),
                    )

                array_input[inpt_name] = inpt.getAttribute('default')

            data_form[form_id] = array_input

        # In case of a request node, add its captured values to `data_form`
        # so they can be recognized as valid values
        captures = node.getElementsByTagName('capture')

        for capture in captures:
            capture_id = capture.getAttribute('id')

            if capture_id and not variable_re.match(capture_id):
                raise MalformedProcess(
                    '{}:{} Capture ids must be valid variable names'.format(
                        filename,
                        sw.lineno,
                    ),
                )

            capture_inputs = capture.getElementsByTagName("value")
            array_input = {}

            for inpt in capture_inputs:
                inpt_name = inpt.getAttribute('name')

                if not variable_re.match(inpt_name):
                    raise MalformedProcess(
                        '{}:{} names must be valid variable names'.format(
                            filename,
                            sw.lineno,
                        ),
                    )

                array_input[inpt_name] = '1'

            data_form[capture_id] = array_input

        # add this node to the list of revised nodes
        has_auth_filter = len(node.getElementsByTagName('auth-filter')) > 0

        if has_auth_filter:
            passed_nodes.append(node.getAttribute('id'))


def xmlvalidate(filenames=None, verbose=False):
    if not filenames:
        parser = argparse.ArgumentParser(description='Validate xmls')

        parser.add_argument('files', metavar='FILE', nargs='+',
                            help='the files to be validated')
        parser.add_argument('--verbose', '-v', action='store_true')

        args = parser.parse_args()

        filenames = []
        for arg in args.files:
            filenames += glob.glob(arg)
        verbose = args.verbose

    found_errors = False

    for filename in filenames:
        try:
            try:
                _validate_file(filename)
                if verbose:
                    print('{} seems correct'.format(filename))
            except SAXParseException:
                raise MalformedProcess('{}:{} Is not valid xml'.format(
                    filename,
                    0,
                ))
            except FileNotFoundError:
                raise MalformedProcess('{} not found'.format(filename))
        except MalformedProcess as e:
            if verbose:
                print(e)
            found_errors = True

    if found_errors:
        sys.exit('** Validation errors found **')


if __name__ == '__main__':
    xmlvalidate()
