import re
import xml.dom.pulldom

from cacahuate.xml.utils import get_text


def clean_multiple_attr(attr):
    rng = (1, 1)

    if attr:
        nums = re.compile(r'\d+').findall(attr)
        nums = [int(x) for x in nums]
        if len(nums) == 1:
            rng = (nums[0], nums[0])
        elif len(nums) == 2:
            rng = (nums[0], nums[1])
        else:
            rng = (0, 1_000)

    return rng


def parse_dependencies(node, parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'dependencies':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'dep':
            parser.expandNode(node)  # noqa: B038
            items.append(get_text(node))

    return items


def parse_option(node, parser):
    ref = node.getAttribute('ref')
    if ref:
        data = {
            'ref': ref,
            'value': node.getAttribute('value'),
            'label': node.getAttribute('label'),
            'filters': [],
        }

        for subnode in node.getElementsByTagName('filter'):
            data['filters'].append({
                'name': subnode.getAttribute('name'),
                'value': get_text(subnode),
            })

        return data

    else:
        data = {
            'value': node.getAttribute('value'),
            'label': node.getAttribute('label'),
        }

        if not data['label']:
            parser.expandNode(node)  # noqa: B038
            data['label'] = get_text(node)

        return data


def parse_options(node, parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'options':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'option':
            items.append(
                parse_option(node, parser),
            )

    return items


def parse_input(node, parser):
    data = {
        'id': node.getAttribute('name'),
        'type': node.getAttribute('type'),
        'label': node.getAttribute('label'),
        'dependencies': [],
    }

    if node.getAttribute('type') == 'formfieldselect':
        data['type'] = 'model'
        data['modifiers'] = [
            {
                'key': 'model',
                'value': 'value',
            },
            {
                'key': 'filter:execution__id',
                'value': '{{ _execution.id }}',
            },
            {
                'key': 'filter:fieldset_ref',
                'value': node.getAttribute('formid'),
            },
            {
                'key': 'value_path',
                'value': node.getAttribute('optionvaluepath'),
            },
            {
                'key': 'label_path',
                'value': node.getAttribute('optionlabelpath'),
            },
        ]

    if node.getAttribute('type') == 'categoryquirkselect':
        data['type'] = 'model'
        data['modifiers'] = [
            {
                'key': 'model',
                'value': 'quirk',
            },
            {
                'key': 'filter:category__codename',
                'value': node.getAttribute('categorycodename'),
            },
            {
                'key': 'value_path',
                'value': node.getAttribute('optionvaluepath'),
            },
            {
                'key': 'label_path',
                'value': node.getAttribute('optionlabelpath'),
            },
        ]

    if node.getAttribute('type') == 'userselect':
        data['type'] = 'model'
        data['modifiers'] = [
            {
                'key': 'model',
                'value': 'user',
            },
            {
                'key': 'value_path',
                'value': node.getAttribute('optionvaluepath'),
            },
            {
                'key': 'label_path',
                'value': node.getAttribute('optionlabelpath'),
            },
        ]

    if node.getAttribute('required') == 'required':
        data['required'] = 'TRUE'
    else:
        data['required'] = 'FALSE'

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'input':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'dependencies':
            data['dependencies'] = parse_dependencies(node, parser)

        if node_tag == 'options':
            data['options'] = parse_options(node, parser)

        if node_tag == 'required':
            parser.expandNode(node)  # noqa: B038
            data['required'] = get_text(node)

    return data


def parse_form(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'inputs': [],
    }

    (
        data['min_amount'],
        data['max_amount'],
    ) = clean_multiple_attr(node.getAttribute('multiple'))

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'form':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'input':
            data['inputs'].append(
                parse_input(node, parser),
            )

    return data


def parse_form_array(parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'form-array':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'form':
            items.append(
                parse_form(node, parser),
            )

    return items


def parse_action(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'action',
        'milestone': node.getAttribute('milestone') == 'true',
        'forms': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'action':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'name':
            parser.expandNode(node)  # noqa: B038
            data['name'] = get_text(node)

        if node_tag == 'description':
            parser.expandNode(node)  # noqa: B038
            data['description'] = get_text(node)

        if node_tag == 'form-array':
            data['forms'] = parse_form_array(parser)

    return data


def parse_validation(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'validation',
        'milestone': node.getAttribute('milestone') == 'true',
        'dependencies': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'validation':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'name':
            parser.expandNode(node)  # noqa: B038
            data['name'] = get_text(node)

        if node_tag == 'description':
            parser.expandNode(node)  # noqa: B038
            data['description'] = get_text(node)

        if node_tag == 'dependencies':
            data['dependencies'] = parse_dependencies(node, parser)

    return data


def parse_exit(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'exit',
        'milestone': node.getAttribute('milestone') == 'true',
        'dependencies': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'exit':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

    return data


def parse_if(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'if',
        'milestone': node.getAttribute('milestone') == 'true',
        'condition': None,
        'children': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'if':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'condition':
            parser.expandNode(node)  # noqa: B038
            data['condition'] = get_text(node)

        if node_tag == 'block':
            data['children'] = parse_block(parser)

    return data


def parse_elif(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'elif',
        'milestone': node.getAttribute('milestone') == 'true',
        'condition': None,
        'children': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'elif':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'condition':
            parser.expandNode(node)  # noqa: B038
            data['condition'] = get_text(node)

        if node_tag == 'block':
            data['children'] = parse_block(parser)

    return data


def parse_else(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'else',
        'milestone': node.getAttribute('milestone') == 'true',
        'children': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'else':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'block':
            data['children'] = parse_block(parser)

    return data


def parse_request(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'request',
        'milestone': node.getAttribute('milestone') == 'true',
        'dependencies': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'request':
            break

        if node_tag == 'dependencies':
            data['dependencies'] = parse_dependencies(node, parser)

    return data


def parse_call(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'call',
        'milestone': node.getAttribute('milestone') == 'true',
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'call':
            break

    return data


def parse_connection(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'connection',
        'milestone': node.getAttribute('milestone') == 'true',
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'connection':
            break

        if node_tag == 'name':
            parser.expandNode(node)  # noqa: B038
            data['name'] = get_text(node)

        if node_tag == 'description':
            parser.expandNode(node)  # noqa: B038
            data['description'] = get_text(node)

    return data


def parse_block(parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'block':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        parser_map = {
            'action': parse_action,
            'validation': parse_validation,
            'exit': parse_exit,
            'if': parse_if,
            'elif': parse_elif,
            'else': parse_else,
            'request': parse_request,
            'call': parse_call,
            'connection': parse_connection,
        }

        if node_tag in parser_map:
            items.append(
                parser_map[node_tag](node, parser),
            )

    return items


def parse_process(parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'process':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        parser_map = {
            'action': parse_action,
            'validation': parse_validation,
            'exit': parse_exit,
            'if': parse_if,
            'elif': parse_elif,
            'else': parse_else,
            'request': parse_request,
            'call': parse_call,
            'connection': parse_connection,
        }

        if node_tag in parser_map:
            items.append(
                parser_map[node_tag](node, parser),
            )

    return items


def parse_process_info(node, parser):
    data = {
        'author': None,
        'date': None,
        'name': None,
        'public': False,
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'process-info':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'author':
            parser.expandNode(node)  # noqa: B038
            data['author'] = get_text(node)

        if node_tag == 'date':
            parser.expandNode(node)  # noqa: B038
            data['date'] = get_text(node)

        if node_tag == 'public':
            parser.expandNode(node)  # noqa: B038
            data['public'] = get_text(node) == 'true'

        if node_tag == 'name':
            parser.expandNode(node)  # noqa: B038
            data['name'] = get_text(node)

        if node_tag == 'description':
            parser.expandNode(node)  # noqa: B038
            data['description'] = get_text(node)

    return data


def parse_xml(parser: xml.dom.pulldom.DOMEventStream):
    data = {
        'process-info': None,
        'process': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'process-spec':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'process-info':
            data['process-info'] = parse_process_info(node, parser)

        if node_tag == 'process':
            data['process'] = parse_process(parser)

    return data
