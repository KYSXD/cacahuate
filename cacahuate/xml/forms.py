import re

from cacahuate.errors import (
    InputError,
    ValidationErrors,
)
from cacahuate.jsontypes import SortedMap
from cacahuate.xml.inputs import make_input


class Form:
    def __init__(self, element, context=None):
        if not context:
            context = {}

        self.ref = element.getAttribute('id')
        self.multiple = self.calc_range(element.getAttribute('multiple'))

        # Load inputs
        self.inputs = []

        for input_el in element.getElementsByTagName('input'):
            self.inputs.append(make_input(input_el, context))

    def calc_range(self, attr):
        rng = (1, 1)

        if attr:
            nums = re.compile(r'\d+').findall(attr)
            nums = [int(x) for x in nums]
            if len(nums) == 1:
                rng = (nums[0], nums[0])
            elif len(nums) == 2:
                rng = (nums[0], nums[1])
            else:
                rng = (0, 1_000)

        return rng

    def validate(self, index, data):
        errors = []
        collected_inputs = []

        for inpt in self.inputs:

            try:
                value = inpt.validate(
                    data.get(inpt.name),
                    index,
                )
                input_description = inpt.to_json()
                input_description['value'] = value
                input_description['value_caption'] = inpt.make_caption(value)
                input_description['state'] = 'valid'

                collected_inputs.append(input_description)
            except InputError as e:
                errors.append(e)

        if errors:
            raise ValidationErrors(errors)

        return Form.state_json(self.ref, collected_inputs)

    @staticmethod
    def state_json(ref, inputs, state='valid'):
        return {
            '_type': 'form',
            'state': state,
            'ref': ref,
            'inputs': SortedMap(inputs, key='name').to_json(),
        }
